/*== This is home controller ==*/
app.controller('testCtrl', function($scope, NgMap, check){

	var marker;
	$scope.currentUser = {};
	$scope.currentPosition = '';
	$scope.address = '';

	var geocoder = new google.maps.Geocoder();

	$scope.doSth = function() {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({'location': this.getPosition()}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            console.log(results[0]);
          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });
    }

	check.getLocation(function(data){
		$scope.currentlocation = data;
		NgMap.getMap().then(function(map) {
			marker = new google.maps.Marker({
		          map: map,
		          position: {lat: $scope.currentlocation.lat, lng: $scope.currentlocation.lng},
		          draggable: true,
		          title: 'My Title'
		     });
		});
	});

	$scope.addressChange = function(){
		NgMap.getMap().then(function(map) {
			var address_comp = {};
			geocoder.geocode({'address': $scope.currentUser.pickUplocation}, function(results, status) {
				if(Object.keys(results).length > 0){
					map.setCenter(results[0].geometry.location);
					marker.setPosition(results[0].geometry.location);
				}
			});
		});
	}

	NgMap.getMap().then(function(map) {
		google.maps.event.addListener(marker, 'dragstart', function() {
			//updateMarkerAddress('Dragging...');
		});

		google.maps.event.addListener(marker, 'drag', function() {
			updateMarkerStatus('Dragging...');
			//updateMarkerPosition(marker.getPosition());
		});

		google.maps.event.addListener(marker, 'dragend', function() {
			updateMarkerStatus('Drag ended');
			geocodePosition(marker.getPosition());
			map.panTo(marker.getPosition()); 
			//console.log('dragend');
		});

		google.maps.event.addListener(map, 'click', function(e) {
			updateMarkerPosition(e.latLng);
			geocodePosition(marker.getPosition());
			marker.setPosition(e.latLng);
			map.panTo(marker.getPosition()); 
		});
	});

	function geocodePosition(pos) {
	  geocoder.geocode({
	    latLng: pos
	  }, function(responses) {
	    if (responses && responses.length > 0) {
			alert($scope.currentUser.pickUplocation);
			$scope.currentUser.pickUplocation = responses[0].formatted_address;
	    } else {
	      // updateMarkerAddress('Cannot determine address at this location.');
	    }
	  });
	}

	function updateMarkerStatus(str) {
	  	$scope.markerStatus = str;
	  	console.log($scope.markerStatus);	  	
	}

	function updateMarkerPosition(latLng) {
	   	$scope.currentPosition = [latLng.lat(), latLng.lng()].join(', ');
		console.log($scope.currentPosition);
	}

	function updateMarkerAddress(str) {
	  $scope.currentUser.pickUplocation  = str;
	  console.log($scope.currentUser.pickUplocation);
	}
});
