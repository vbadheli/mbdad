<?php 

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

/*== Requaire Config file ==*/
require_once(dirname(dirname(dirname(__FILE__))).DS.'config.php');
include_once(ROOT.DS.'vendor/autoload.php');

use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\PaymentMethod;
use Coinbase\Wallet\Resource\Sell;
use Coinbase\Wallet\Resource\Buy;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Exception;
use \GuzzleHttp\Exception\ClientException;

class trader {

    public $buyPrice;
    public $sellPrice;
    public $spotPrice;
    public $lastSellPrice;
    
    private $client;
    private $account;
    private $wallet;
    private $transactions;
    private $traderID;
    private $currencyWallet;

    private $redis;

    function __construct($noinit=false) {
        $configuration = Configuration::apiKey(COINBASE_KEY, COINBASE_SECRET);
        $this->client = Client::create($configuration);
    }


    function getTime(){
        return $this->client->getTime();
    }

    function getBuyingPrices() {

        $response = array();
        $response['BTC']['buyPrice'] =  floatval($this->client->getBuyPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
        $response['BTC']['sellPrice'] = floatval($this->client->getSellPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
        $response['BTC']['spotPrice'] = floatval($this->client->getSpotPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());        

        $response['ETH']['buyPrice'] =  floatval($this->client->getBuyPrice(CRYPTO_ETH.'-'.CURRENCY)->getAmount());
        $response['ETH']['sellPrice'] = floatval($this->client->getSellPrice(CRYPTO_ETH.'-'.CURRENCY)->getAmount());
        $response['ETH']['spotPrice'] = floatval($this->client->getSpotPrice(CRYPTO_ETH.'-'.CURRENCY)->getAmount());

        $response['LTC']['buyPrice'] =  floatval($this->client->getBuyPrice(CRYPTO_LTC.'-'.CURRENCY)->getAmount());
        $response['LTC']['sellPrice'] = floatval($this->client->getSellPrice(CRYPTO_LTC.'-'.CURRENCY)->getAmount());
        $response['LTC']['spotPrice'] = floatval($this->client->getSpotPrice(CRYPTO_LTC.'-'.CURRENCY)->getAmount());

        return $response;
    }

    function getPrimaryAccount(){
        $accounts = $this->client->getAccounts();
        foreach($accounts as $account) {          
            if($account->getName() == "BTC Wallet"){
                $this->client->getAccountTransactions($account);
                $this->account = $account;
                $this->wallet = $account;
                $this->currencyWallet = $account;
            }
        }

        $this->sellPrice = floatval($this->client->getSellPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
        $this->buyPrice = floatval($this->client->getBuyPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
    }

    function getTransactions() {
        $accounts = $this->client->getAccounts();
        foreach($accounts as $account) {          
            if($account->getName() == "BTC Wallet"){
                $transactions = $this->client->getAccountTransactions($account);
                return $transactions;
            }
        }

        $this->sellPrice = floatval($this->client->getSellPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
        $this->buyPrice = floatval($this->client->getBuyPrice(CRYPTO_BTC.'-'.CURRENCY)->getAmount());
    }

    function debug() {
        echo '<pre>';
        echo "############ DEBUG START ############\n\n";
        echo "[i] Listing accounts\n\n";
        $accounts = $this->client->getAccounts();
        foreach($accounts as $account) {
            echo "  [W] Wallet:\t '".$account->getName()."'\n";
            echo "    [Wi] ID: ".$account->getId()."\n";
            echo "    [Wi] currency: ".$account->getCurrency()."\n";
            echo "    [Wi] Amount: ".$account->getBalance()->getAmount()."\n\n";
        }

        echo "[i] Listing Payment methods\n\n";
        $paymentMethods = $this->client->getPaymentMethods();
        foreach($paymentMethods as $pm) {
            echo "  [PM] Wallet:\t '".$pm->getName()."'\n";
            echo "    [PMi] ID: ".$pm->getId()."\n";
            echo "    [PMi] currency: ".$pm->getCurrency()."\n\n";
        }

        echo "\n############ DEBUG END ############\n";
        echo '</pre>';
    }


    function getAccounts(){
        echo "############ DEBUG START ############\n\n";
        echo "[i] Listing accounts\n\n";
        $accounts = $this->client->getAccounts();
        foreach($accounts as $account) {
            echo "  [W] Wallet:\t '".$account->getName()."'\n";
            echo "    [Wi] ID: ".$account->getId()."\n";
            echo "    [Wi] currency: ".$account->getCurrency()."\n";
            echo "    [Wi] Amount: ".$account->getBalance()->getAmount()."\n\n";
        }

        echo "[i] Listing Payment methods\n\n";
        $paymentMethods = $this->client->getPaymentMethods();
        foreach($paymentMethods as $pm) {
            echo "  [PM] Wallet:\t '".$pm->getName()."'\n";
            echo "    [PMi] ID: ".$pm->getId()."\n";
            echo "    [PMi] currency: ".$pm->getCurrency()."\n\n";
        }

        echo "\n############ DEBUG END ############\n";
    }

    function checkBalanceOfAccount($account) {
        $data = $account->getBalance();
        $amount = $data->getAmount();
        $currency = $data->getCurrency();
        return $amount;
    }

    function loadTransactions() {
        //clearing transactions array so we don't have any legacy data
        $this->transactions = array();
        if(defined('REDIS_SERVER') && REDIS_SERVER != '')
        {
            if(file_exists(ROOT.DS.'transactions'.(CRYPTO!='BTC'?'-'.CRYPTO:'').'.json')) //migrate to redis
            {
                 echo "[C] Found transactions".(CRYPTO!='BTC'?'-'.CRYPTO:'').".json and Redis is configured. Converting json to Redis.. ";
                $transactions = json_decode(file_get_contents(ROOT.DS.'transactions'.(CRYPTO!='BTC'?'-'.CRYPTO:'').'.json'), true);
                foreach($transactions as $key=>$transaction)
                {
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':btc',$transaction['btc']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':eur',$transaction['eur']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':buyprice',$transaction['buyprice']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':sellat',$transaction['sellat']);
                }
                unlink(ROOT.DS.'transactions'.(CRYPTO!='BTC'?'-'.CRYPTO:'').'.json');
                 echo "done\n";
            }

             echo "[i] Loading data from Redis.. ";
            $data = $this->redis->keys('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':*');
            if(is_array($data))
                foreach($data as $d)
                {
                    $a = explode(':',$d);
                    $key = $a[1];
                    $var = $a[2];
                    $val = $this->redis->get("phptrader".(CRYPTO!='BTC'?'-'.CRYPTO:'').":$key:$var");
                    $this->transactions[$key][$var] = $val;
                }
            
             echo "done! Found ".count($this->transactions)." data points.\n";
        }
        else if(file_exists(ROOT.DS.'transactions'.(CRYPTO!='BTC'?'-'.CRYPTO:'').'.json'))
            $this->transactions = json_decode(file_get_contents(ROOT.DS.'transactions'.(CRYPTO!='BTC'?'-'.CRYPTO:'').'.json'), true);

    }

    function deleteTransaction($id) {
         echo "[i] Deleting transaction ID $id\n";
        if(defined('REDIS_SERVER') && REDIS_SERVER != '') {
            if(!$this->redis->exists("phptrader".(CRYPTO!='BTC'?'-'.CRYPTO:'').":$id:buyprice") && DEV===true)
                echo " [!ERR!] Key $id does not exist in Redis!\n";
            else {
                $keys = $this->redis->keys("phptrader".(CRYPTO!='BTC'?'-'.CRYPTO:'').":$id:*");
                foreach ($keys as $key) {
                    $this->redis->del($key);
                }
            }
        } else {
            unset($this->transactions[$id]);
        }

        $this->saveTransactions();
    }

    function saveTransactions()
    {
        if(defined('REDIS_SERVER') && REDIS_SERVER != '')
        {
            foreach($this->transactions as $key=>$transaction)
                {
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':btc',$transaction['btc']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':eur',$transaction['eur']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':buyprice',$transaction['buyprice']);
                    $this->redis->set('phptrader'.(CRYPTO!='BTC'?'-'.CRYPTO:'').':'.$key.':sellat',$transaction['sellat']);
                }
        }
        else
            file_put_contents(ROOT.DS."transactions".(CRYPTO!='BTC'?'-'.CRYPTO:'').".json",json_encode($this->transactions));
    }

    function updatePrices($silent=false) {
        
        $this->lastSellPrice = $this->sellPrice;
        $this->buyPrice =  floatval($this->client->getBuyPrice(CRYPTO.'-'.CURRENCY)->getAmount());
        $this->sellPrice = floatval($this->client->getSellPrice(CRYPTO.'-'.CURRENCY)->getAmount());
        $this->spotPrice = floatval($this->client->getSpotPrice(CRYPTO.'-'.CURRENCY)->getAmount());

        if(!$this->lastSellPrice)
            $this->lastSellPrice = $this->sellPrice;

        if($silent===false)
        {
            echo "[i] Buy price: $this->buyPrice ".CURRENCY."\n";
            echo "[i] Sell price: $this->sellPrice ".CURRENCY."\n";
            echo "[i] Spot price: $this->spotPrice ".CURRENCY."\n";
            echo "[i] Difference buy/sell: ".round(abs($this->buyPrice-$this->sellPrice),2)." ".CURRENCY."\n\n";
        }
    }

    function addBuyTransaction($eur,$buyat,$sellat) {
        $this->loadTransactions();
        echo "[i] Adding BUY order for $eur ".CURRENCY." in ".CRYPTO." when price is <= $buyat ".CURRENCY."\n";
        $id = @max(array_keys($this->transactions))+1;
        $this->transactions[$id] = array('eur'=>$eur,'buyprice'=>$buyat,'sellat'=>$sellat);
        $this->saveTransactions();
        if(ROCKETCHAT_REPORTING===true) sendToRocketchat("Added BUY order for *$eur ".CURRENCY."* when ".CRYPTO." price hits *$buyat ".CURRENCY."*. Currently it's at: *$this->sellPrice ".CURRENCY."*. Only *".($this->sellPrice-$buyat).' '.CURRENCY.'* to go',':raised_hands:');
    }

    function addSellTransaction($eur,$sellat) {
        $this->loadTransactions();
        echo "[i] Adding SELL order for $eur ".CURRENCY." in ".CRYPTO." when price is >= $sellat ".CURRENCY."\n";
        $id = @max(array_keys($this->transactions))+1;
        $this->transactions[$id] = array('eur'=>$eur,'sellat'=>$sellat);
        $this->saveTransactions();
        if(ROCKETCHAT_REPORTING===true) sendToRocketchat("Added SELL order for *$eur ".CURRENCY."* when ".CRYPTO." price hits *$sellat ".CURRENCY."*. Currently it's at: *$this->sellPrice ".CURRENCY."*. Only *".($sellat-$this->sellPrice).' '.CURRENCY.'* to go',':raised_hands:');
    }

    /*
    * Buys the configured crypto for real money
    * $money is $ or €, not some other crypto
    */
    function buyCryptoInMoney($money) {
        if(SIMULATE===false) {
            echo " [B] Buying $money ".CURRENCY.' of '.CRYPTO."\n";
            $buy = new Buy([
                'amount' => new Money($money, CURRENCY),
                'paymentMethodId' => $this->wallet->getId()
            ]);

            //check if account has enough currency
            if($this->checkBalanceOfAccount($this->currencyWallet)<$money)
            {
                echo " [ERR] You don't have enough ".CURRENCY." in your '".$this->currencyWallet->getName()."'. Cancelling buy\n";
                return;
            }
            else
                $this->client->createAccountBuy($this->account, $buy);

        } else {
            echo " [S] Simulating buy of $money ".CURRENCY.' in '.CRYPTO."\n";
        }
    }

    function buyBTC($amount, $sellat=false, $btc=false) {

        $this->getPrimaryAccount();
        $this->updatePrices(true);

        $eur = ($btc===true?($this->buyPrice*$amount):$amount);
        $btc = ($btc===true?$amount:($amount/$this->buyPrice));

        if($btc <= MIN_BTC){
            echo 'We don\'t support amounts smaller than '.MIN_BTC.' BTC';
            return false;
            $response['status'] = false;
            $response['message'] = 'We don\'t support amounts smaller than '.MIN_BTC.' BTC';
            return $response;
        }

        if(SIMULATE===false) {

            $buy = new Buy();
            $buy->setAmount(new Money($btc, CRYPTO));

            //check if account has enough currency
            $balance_of_account = $this->checkBalanceOfAccount($this->currencyWallet);
            if( $balance_of_account < $btc ) {
                $response['status'] = false;
                $response['message'] = "You don't have enough ".CRYPTO." in your '".$this->account->getName()."'. Cancelling sell";
                return $response;
            } else {
                $this->client->createAccountBuy($this->account, $buy);
                $response['status'] = true;
                $response['message'] = CRYPTO." buy succssfully";
                return $response;
            }
        }
    }

    function sellBTC($amount, $btc=false) {

        $response = array();

        $this->getPrimaryAccount();
        $eur = ($btc===true?($this->sellPrice*$amount):$amount);
        $btc = ($btc===true?$amount:($amount/$this->sellPrice));

        $sell = new Sell();
        $sell->setAmount(new Money($btc, CRYPTO));
        
        if(SIMULATE===false) {
            if($this->checkBalanceOfAccount($this->account) < $btc) {   
                $response['status'] = false;
                $response['message'] = "You don't have enough ".CRYPTO." in your '".$this->account->getName()."'. Cancelling sell";
                return $response;
            } else{
                $this->client->createAccountSell($this->account, $sell);
                $response['status'] = true;
                $response['message'] = CRYPTO." sell succssfully";
                return $response;
            }
        }else {
            $response['status'] = true;
            $response['message'] = CRYPTO." sell succssfully with simulate";
            return $response;
        }
    }

    function listTransactions()
    {
        $this->loadTransactions(); //update transactions since the data could have changed by now

        if(count($this->transactions)<1)
            $message = "No transactions at the moment\n";
        else
            foreach($this->transactions as $id=>$td)
            {
                $btc = isset($td['btc']) ? $td['btc'] : false;
                $eur = $td['eur'];
                $buyprice = $td['buyprice'];
                $sellat = $td['sellat']+$eur;
                $newprice = $btc*$this->sellPrice;
                $diff = round(($this->sellPrice-$buyprice)*$btc,2);

                $message = "ID: $id\t";
                //is this a SELL order?
                if(!$buyprice) 
                {
                    $message.="SELL order for $eur ".CURRENCY." when ".CRYPTO." will reach a price of ".$td['sellat']." ".CURRENCY."\n";
                }
                //is this a BUY order?
                else if(!$btc)
                {
                    $message.="BUY order for $eur in ".CRYPTO." when 1 ".CRYPTO." will be worth $buyprice ".CURRENCY." and sell when it's worth $sellat ".CURRENCY."\n";
                }
                else
                {
                    $message.="Holding $btc ".CRYPTO." (".($buyprice*$btc)." ".CURRENCY." at buy), will sell when it's worth $sellat ".CURRENCY."\n";
                }
                echo $message;
            }

        

    }

    function watchdog()
    {
        if(ROCKETCHAT_REPORTING===true) sendToRocketchat("Starting watchdog",':wave:');
        /*while(1) {*/
            $this->mainCheck();
            //sleep((defined('SLEEPTIME')?SLEEPTIME:10));
            echo "------\n";
        /*}*/
    }

    function mainCheck() {

        $this->loadTransactions(); //update transactions since the data could have changed by now
        echo "[i] Currently watching ".count($this->transactions)." transactions\n";
        //only update prices if we have active transactions to watch
        if(count($this->transactions)>0)
            $this->updatePrices();
        if($this->lastSellPrice!=$this->sellPrice && round(abs($this->sellPrice-$this->lastSellPrice),2) > 0)
            echo "[".CRYPTO."] Price went ".($this->sellPrice>$this->lastSellPrice?'up':'down')." by ".round($this->sellPrice-$this->lastSellPrice,2)." ".CURRENCY."\n";

        foreach($this->transactions as $id=>$td) {

            $btc = isset($td['btc']) ? $td['btc'] : false;
            $eur = $td['eur'];
            $buyprice = $td['buyprice'];
            $sellat = $td['sellat']+$eur;
            $newprice = $btc*$this->sellPrice;
            
            $diff = round(($this->sellPrice-$buyprice)*$btc,2);

                //is this a SELL order?
            if(!$buyprice) {
                if($this->sellPrice >= $td['sellat'])  {
                    $btc = (1/$this->sellPrice) * $eur;
                    $this->deleteTransaction($id);
                    $this->sellBTC($btc,true);
                    if(ROCKETCHAT_REPORTING===true) sendToRocketchat("Selling *".$btc." ".CRYPTO."* for *".$eur." ".CURRENCY."*. Forefilling and deleting this sell order.",':money_with_wings:');
                } else {
                    echo " [#$id] Watching SELL order for \t$eur ".CURRENCY.". Will sell when ".CRYPTO." price reaches ".$td['sellat']." ".CRYPTO.".\n";
                }
            } else if(!$btc) {
                if($this->buyPrice <= $buyprice) {
                    $this->deleteTransaction($id);
                    $this->buyBTC($eur, ($sellat-$eur) );
                } else {
                    echo " [#$id] Watching BUY order for \t$eur ".CURRENCY.". Will buy when ".CRYPTO." price reaches $buyprice.\n";
                }
            } else {
                $message = " [#$id] Holding \t$eur ".CURRENCY." at buy. Now worth:\t ".round($newprice,2)." ".CURRENCY.". Change: ".($diff)." ".CURRENCY.". Will sell at \t$sellat ".CURRENCY."\n";
                echo $message;

                if( ($this->sellPrice*$btc) >= $sellat ) {
                    echo "  [#$id] AWWYEAH time to sell $btc ".CRYPTO." since it hit ".($this->sellPrice*$btc)." ".CURRENCY.". Bought at $eur ".CURRENCY."\n";
                    $this->sellBTCID($id);
                }
            }
        }
    }

    function report()
    {
        ob_start();
        $this->mainCheck();
        $out = ob_get_contents();
        ob_end_clean();

        sendToRocketchat($out,':information_source:');
    }

    function autotrade($stake=10,$sellpercent=115)
    {
        if(!$stake || !is_numeric($stake) || $stake < 1) $stake = 10;
        if(!$sellpercent || !is_numeric($sellpercent) || $sellpercent < 1) $sellpercent = 115;
        
        if(file_exists('autotrader.txt')) {
            $data = trim(file_get_contents('autotrader.txt'));
            $a = explode(';',$data);
            $boughtat = $a[0];
            $coins = $a[1];
            $stake = $a[2];
            echo "[A] Loading existing autotrader with stake of $stake ".CURRENCY.". Holding ".$coins.' '.CRYPTO." at $boughtat ".CURRENCY." per ".CRYPTO."\n";
        } else {
            $boughtat = $this->buyPrice;
            $coins = $stake/$boughtat;
            $this->buyCryptoInMoney($stake);
            file_put_contents('autotrader.txt',"$boughtat;$coins;$stake");
            echo "[A] Starting autotrader with stake of $stake ".CURRENCY.".".($nobuy===true?' NOT':'')." Buying ".$coins.' '.CRYPTO." at $boughtat ".CURRENCY." per ".CRYPTO."\n";
        }

        $targetprice = round(($stake/100)*$sellpercent);
        while(1) {

            $diff = ($this->sellPrice*$coins)-($boughtat*$coins);
            $percentdiff = round((($this->sellPrice*$coins)/($boughtat*$coins))*100,2);
            if($percentdiff>=115) {
                echo "\n  [!] Price is now $percentdiff % of buy price. Selling $stake ".CURRENCY."\n";
                $this->sellBTC($stake);
                //aaand here we go again
                $boughtat = $this->buyPrice;
                $coins = $stake/$boughtat;
                $this->buyCryptoInMoney($stake);
                file_put_contents('autotrader.txt',"$boughtat;$coins;$stake");
                echo "\n[A] Re-buying with stake of $stake ".CURRENCY.". Buying ".$coins.' '.CRYPTO." at $boughtat ".CURRENCY." per ".CRYPTO."\n";
            } else
                echo "\r [".date("d.m H:i")."] Current price: ".round($this->sellPrice*$coins)." ".CURRENCY.". $percentdiff% of target. Will sell at ".$sellpercent."% for $targetprice ".CURRENCY."         ";

            sleep(SLEEPTIME);
            $this->updatePrices(true);
        }
    }

}


//rocketchat
function sendToRocketchat($message,$icon=':ghost:'){
    
    $username = CURRENCY.' - '.CRYPTO.' trader'.(SIMULATE===true?' (simulation)':'');
    $data = array("icon_emoji"=>$icon,
                "username"=>$username,
		        "text"=>$message);
    makeRequest(ROCKETCHAT_WEBHOOK,array('payload'=>json_encode($data)));
}

function makeRequest($url,$data,$headers=false,$post=true) {

    $headers[] = 'Content-type: application/x-www-form-urlencoded';
    $options = array(
        'http' => array(
            'header'  => $headers,
            'method'  => $post?'POST':'GET',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }
    return json_decode($result,true);
}