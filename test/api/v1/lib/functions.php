<?php

/**
 * Get database
 */
function getDB() {

    $dbhost=DB_HOST;
    $dbuser=DB_USERNAME;
    $dbpass=DB_PASSWORD;
    $dbname=DB_NAME;

    $dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbConnection->exec("set names utf8");
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}

/**
 * Calculate percentage hike 
 * 
 */
function calculate_percentage($given_number , $percent = 10, $decrease = true){

    if ( ! is_numeric($given_number) ){
        return false;
    }
    
    $numberToAdd = ($given_number / 100) * $percent;
    if($decrease){
        return $given_number-$numberToAdd;
    }else {
        return $given_number+$numberToAdd;
    }
}

/**
* Request Headers
**/
function request_headers() {

    $arh = array();
    $rx_http = '/\AHTTP_/';
    foreach($_SERVER as $key => $val) {
        if( preg_match($rx_http, $key) ) {
            $arh_key = preg_replace($rx_http, '', $key);
            $rx_matches = array();
            $rx_matches = explode('_', $arh_key);
            if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
                    foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
                    $arh_key = implode('-', $rx_matches);
            }
            $arh[$arh_key] = $val;
        }
    }

    return($arh);
}

/**
 * Check is email exist
 */
function is_email_exist($email){

    $db = getDB();
    $userData = '';
    $sql = "SELECT id FROM ".table_prifix."_users WHERE email=:email";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("email", $email,PDO::PARAM_STR);
    $stmt->execute();
    $mainCount = $stmt->rowCount();

    if( $mainCount > 0 ) {
    	return true;
    }

    return false;
}

/**
 * Is json valid  
 */
function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

/**
 * Check is Token Valid
 */
function is_token_valid($token){
    $db = getDB();
    $sql = "SELECT id FROM ".table_prifix."_users WHERE token=:token";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("token", $token, PDO::PARAM_STR);
    $stmt->execute();
    $userData = $stmt->fetch(PDO::FETCH_OBJ);
    return isset($userData->id) ? $userData->id : false;
}

/**
 * Update Token  
 */
function update_token($token, $user_id, $tokenExpiration) {
    $db = getDB();
    $sql = "UPDATE ".table_prifix."_users SET token=:token,token_expire=:tokenExpiration WHERE id=:user_id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
    $stmt->bindParam("token", $token, PDO::PARAM_STR);
    $stmt->bindParam("tokenExpiration", $tokenExpiration, PDO::PARAM_INT);
    $stmt->execute();
}

/**
 * Get user meta
 */
function get_user_data($requested_fields, $field = 'email', $filed_value) {

    if( is_array($requested_fields) && sizeof($requested_fields) > 0 ) {
        $requested_fields = explode(",", $requested_fields);
    }    

    $db = getDB();
    $sql = "SELECT $requested_fields FROM ".table_prifix."_users WHERE $field=:filed_value";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("filed_value", $filed_value);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);

    if( !empty($result) && sizeof($result) > 0 ) {
        return $result;
    }

    return false;
}

/**
 * Get user meta
 */
function get_user_meta($user_id, $meta_key) {

    $db = getDB();
    $sql = "SELECT `meta_value` FROM ".table_prifix."_usermeta WHERE user_id=:user_id AND meta_key=:meta_key";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
    $stmt->bindParam("meta_key", $meta_key, PDO::PARAM_STR);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);

    if( isset($result->meta_value) && isJSON($result->meta_value) ) {
        return json_decode($result->meta_value);
    }

    if( isset($result->meta_value) && !isJSON($result->meta_value) ) {
        return $result->meta_value;
    }

    return false;
}

/**
 * Get user meta
 */
function update_user_meta( $user_id, $meta_key, $meta_value ) {
    
    if( empty($user_id) || empty($meta_key) || empty($meta_value) ) {
        return false;
    }

    if( is_array($meta_value) ) {
        $meta_value = json_encode($meta_value, true);
    } 

    $meta_data = get_user_meta($user_id, $meta_key);
    if( !empty($meta_data) ) {
        $db = getDB();
        $sql = "UPDATE ".table_prifix."_usermeta SET meta_key=:meta_key,meta_value=:meta_value WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam("meta_key", $meta_key, PDO::PARAM_STR);
        $stmt->bindParam("meta_value", $meta_value, PDO::PARAM_STR);

        if($stmt->execute()) {
            return true;
        }
    }else {

        $db = getDB();
        $sql = "INSERT INTO ".table_prifix."_usermeta (meta_key,meta_value,user_id) VALUES (:meta_key,:meta_value,:user_id)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam("meta_key", $meta_key, PDO::PARAM_STR);
        $stmt->bindParam("meta_value", $meta_value, PDO::PARAM_STR);

        if($stmt->execute()) {
            return true;
        }
    }

    return false;   
}

/**
 * Get Buying Orders 
 */
function get_buying_orders($coin = 'BTC'){
    
    $db = getDB();
    $status = 1;
    $frequently_buy = 0;

    $sql = "SELECT * FROM ".table_prifix."_buy_orders WHERE status=:status AND coin=:coin AND frequently_buy=:frequently_buy";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status,PDO::PARAM_STR);
    $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
    $stmt->bindParam("frequently_buy", $frequently_buy,PDO::PARAM_INT);
    $stmt->execute();
    $buying_orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $buying_orders;
}

/**
 * Get Buying Orders 
 */
function get_frequently_buying_orders($coin = 'BTC'){
    
    $db = getDB();

    $status = 1;
    $frequently_buy = 1;
    $sql = "SELECT * FROM ".table_prifix."_buy_orders WHERE status=:status AND coin=:coin AND frequently_buy=:frequently_buy";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status,PDO::PARAM_STR);
    $stmt->bindParam("frequently_buy", $frequently_buy,PDO::PARAM_INT);
    $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
    $stmt->execute();
    $buying_orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $buying_orders;
}

/**
 * Get Buying Orders 
 */
function get_frequently_selling_orders($coin = 'BTC'){
    
    $db = getDB();

    $status = 1;
    $frequently_sell = 1;
    $sql = "SELECT * FROM ".table_prifix."_sell_orders WHERE status=:status AND coin=:coin AND frequently_sell=:frequently_sell";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status,PDO::PARAM_STR);
    $stmt->bindParam("frequently_sell", $frequently_sell,PDO::PARAM_INT);
    $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
    $stmt->execute();
    $buying_orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $buying_orders;
}

/**
 * Get orders that was put on sell after buying  
 */
function get_buy_put_on_sell($coin = 'BTC'){

    $db = getDB();
    $status = 2;
    $sell_check = 1;
    $sql = "SELECT * FROM ".table_prifix."_buy_orders WHERE status=:status AND coin=:coin AND sell_check=:sell_check";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status,PDO::PARAM_STR);
    $stmt->bindParam("sell_check", $sell_check,PDO::PARAM_STR);
    $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
    $stmt->execute();
    $selling_orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $selling_orders;
}

/**
 * Get orders that was put on sell after buying  
 */
function get_sells_orders($coin = 'BTC'){

    $db = getDB();
    
    $status = 1;
    $frequently_sell = 0;

    $sql = "SELECT * FROM ".table_prifix."_sell_orders WHERE status=:status AND coin=:coin AND frequently_sell=:frequently_sell";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status,PDO::PARAM_STR);
    $stmt->bindParam("frequently_sell", $frequently_sell ,PDO::PARAM_STR);
    $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
    $stmt->execute();
    $selling_orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $selling_orders;
}

/**
 * Update order status
 */
function update_status($id, $status = 1, $table='_buy_orders' ){
    $db = getDB();
    $sql = "UPDATE ".table_prifix.$table." SET status=:status WHERE ID=:id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("status", $status, PDO::PARAM_INT);
    $stmt->bindParam("id", $id, PDO::PARAM_INT);
    $stmt->execute();
}

/**
 * Update order status
 */
function update_column($id, $column, $column_value, $table ){

    if( $column !== '' && $column_value !== '' ) {

        $db = getDB();
        $sql = "UPDATE ".table_prifix.$table." SET ".$column."=:column_value WHERE ID=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("column_value", $column_value, PDO::PARAM_STR);
        $stmt->bindParam("id", $id, PDO::PARAM_INT);
        $stmt->execute();
    }else {
        return false;
    }
}

/*== Completed cron jobs ==*/

/**
 * Active Buying Cron   
 */
function active_buying_cron(){
    
    /*== Make Trader Object ==*/
    $trader = new trader();

    $buying_btc_orders = get_buying_orders();
    if( !empty($buying_btc_orders) && is_array($buying_btc_orders) && sizeof($buying_btc_orders) > 0 ) {
        $buying_price = $trader->getBuyingPrices();
        foreach ($buying_btc_orders as $key => $value) {
            if( $value['amount'] > 0 
                && $value['position'] > 0 
                && isset($buying_price['BTC']['buyPrice']) 
                && $buying_price['BTC']['buyPrice'] > 0 
                && $buying_price['BTC']['buyPrice'] <= $value['position'] ) {
                $result = $trader->buyBTC($value['amount']);
                if( $result['status'] && $value['sell_check'] > 0 ){
                    update_status($value['ID'], 2);
                    update_column($value['ID'], 'buying_position', $buying_price['BTC']['buyPrice'], '_buy_orders');
                }else {
                    update_status($value['ID'], 0);
                    update_column($value['ID'], 'buying_position', $buying_price['BTC']['buyPrice'], '_buy_orders');
                }
            }
        }
    }
}

/**
 * Active put on sell cron 
 */
function active_put_on_sell(){

    /*== Make Trader Object ==*/
    $trader = new trader();

    $order_on_sell = get_buy_put_on_sell();
    
    if( !empty($order_on_sell) && is_array($order_on_sell) && sizeof($order_on_sell) > 0 ) {
        $selling_price = $trader->getBuyingPrices();
        foreach ($order_on_sell as $key => $value) {
            if( $value['buying_position'] > 0 
                && isset($buying_price['BTC']['buyPrice']) 
                && $buying_price['BTC']['sellPrice'] > 0 
                && $buying_price['BTC']['sellPrice'] >= ( $value['desired_earning'] + $value['buying_position']) ) {
                $result = $trader->sellBTC($value['amount'], false);
                if( $result['status']  ){
                    update_status($value['ID'], 0);
                }
            }
        }
    }
}

/**
 * Active selling cron orders 
 */
function active_sell_orders(){

    $trader = new trader();
    $order_on_sell = get_sells_orders();

    if( !empty($order_on_sell) && is_array($order_on_sell) && sizeof($order_on_sell) > 0 ) {
        $selling_price = $trader->getBuyingPrices();
        foreach ($order_on_sell as $key => $value) {

            echo $selling_price['BTC']['sellPrice'].'<br/>';
            echo $value['position'].'<br/>';
            echo $value['stop_loss_limit'].'<br/>';

            if( $value['position'] > 0 
                && isset($selling_price['BTC']['sellPrice']) 
                && $selling_price['BTC']['sellPrice'] > 0 
                && ( $selling_price['BTC']['sellPrice'] >= $value['position'] || $selling_price['BTC']['sellPrice'] <= $value['stop_loss_limit'] ) ) {
                $result = $trader->sellBTC($value['amount'], false);
                if(true){
                    update_status($value['ID'], 0, '_sell_orders');
                    update_column($value['ID'], 'selling_position', $selling_price['BTC']['sellPrice'], '_sell_orders');
                }
            }
        }
    }
}

/**
 * Frequently buying cron
 */
function frequently_buying_cron(){
    /*== Make Trader Object ==*/
    $trader = new trader();
    $buying_btc_orders = get_frequently_buying_orders();
    if( !empty($buying_btc_orders) && is_array($buying_btc_orders) && sizeof($buying_btc_orders) > 0 ) {
        $buying_price = $trader->getBuyingPrices();
        foreach ($buying_btc_orders as $key => $value) {
            if( $value['amount'] > 0 
                && $value['position'] > 0 
                && isset($buying_price['BTC']['buyPrice']) 
                && $buying_price['BTC']['buyPrice'] > 0 
                && $buying_price['BTC']['buyPrice'] <= $value['position'] ) {
                if( $value['buying_position'] && $value['buying_position'] > 0 ){
                    if( isset($value['frequently_buy_percentage']) && $value['frequently_buy_percentage'] > 0 ) {                   
                        if( $buying_price['BTC']['buyPrice'] <= calculate_percentage( $value['buying_position'], $value['frequently_buy_percentage']) ){
                            $result = $trader->buyBTC($value['amount']);
                            if( $result['status'] ){
                                update_column($value['ID'], 'buying_position', $buying_price['BTC']['buyPrice'], '_buy_orders');
                            }
                        }
                    }
                }else {
                    $result = $trader->buyBTC($value['amount']);
                    if( $result['status'] ){
                        update_column($value['ID'], 'buying_position', $buying_price['BTC']['buyPrice'], '_buy_orders');
                    }
                }
            }
        }
    }
}

/*== Active Frequantly Selling Order ==*/
function frequently_selling_cron(){

    /*== Make Trader Object ==*/
    $trader = new trader();
    $selling_btc_orders = get_frequently_selling_orders();

    if( !empty($selling_btc_orders) && is_array($selling_btc_orders) && sizeof($selling_btc_orders) > 0 ) {
        $selling_price = $trader->getBuyingPrices();
        foreach ($selling_btc_orders as $key => $value) {
            if( $value['amount'] > 0 
                && $value['position'] > 0 
                && isset($buying_price['BTC']['sellPrice']) 
                && $selling_price['BTC']['sellPrice'] > 0 
                && $selling_price['BTC']['sellPrice'] >= $value['position'] ) {

                if( $selling_price['BTC']['sellPrice'] <= $value['stop_loss_limit'] ) {
                    $result = $trader->sellBTC($value['amount'], false);
                    if( $result['status'] ){
                        update_column($value['ID'], 'frequently_sell', 0, '_sell_orders');
                        return false;
                    }
                }

                if( $value['selling_position'] && $value['selling_position'] > 0 ){
                    if( isset($value['frequently_sell_percentage']) && $value['frequently_sell_percentage'] > 0 ) {                   
                        if( $selling_price['BTC']['sellPrice'] <= calculate_percentage( $value['selling_position'], $value['calculate_percentage'], false) ){
                            $result = $trader->sellBTC($value['amount'], false);
                            if( $result['status'] ){
                                update_column($value['ID'], 'selling_position', $selling_price['BTC']['sellPrice'], '_sell_orders');
                            }
                        }
                    }
                }else {
                    $result = $trader->sellBTC($value['amount'], false);
                    if( $result['status'] ){
                        update_column($value['ID'], 'selling_position', $selling_price['BTC']['sellPrice'], '_sell_orders');
                    }
                }
            }
        }
    }
}