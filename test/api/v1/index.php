<?php

/*== Requaire slim library and custom functions file ==*/
require_once('../Slim/Slim.php');
include_once('lib/trader.php');
include_once('lib/functions.php');

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

// Get request headers as associative array
$headers = $app->request->headers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
session_start();


$app->get('/testing', function() {
    //meta_key_exist(123);
/*    $response = array();
    $trader = new trader();
    $trader->getAccounts();*/

    $db = getDB();
    $user_data = 4;

    $sql = "SELECT * FROM ".table_prifix."_buy_orders WHERE user_id=:user_id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $user_data, PDO::PARAM_INT);
    $stmt->execute();
    $buying_ordres = $stmt->fetchAll(PDO::FETCH_OBJ);

    print_r($buying_ordres );
});

/**
 * Check authorization before every request 
 */
$app->hook('slim.before.dispatch', function () use ($app){
    
    $response = array();
    $app = \Slim\Slim::getInstance();
    $headers = request_headers();

    /*if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        if ( $headers['AUTHRIZATION'] == 'QWxhZGRpbjpvcGVuIHNlc2FtZQ==' ){
            //Do nothing
        }else {
            $token = $headers['AUTHRIZATION'];
            if( !is_token_valid($token) || !isset($_SESSION['token']) ) {
                $app->halt('403', 'Invalid User Authentication!');        
            }
        }
    }else {
        $app->halt('403', 'Invalid User Authentication!');
    }*/
});

/**
 * User Sign Up
 */
$app->post('/signup', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;

    try {

        $email_check = isset($data->email) ? preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $data->email) : 0;
        $password_check = isset($data->password) ? preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $data->password) : 0;

        if ( isset($data->email) && !filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            $response['status'] = 'error';
            $response['message'] = 'The email address you entered is not valid';
        }


        if ( strlen( trim($data->password) ) > 0 && strlen( trim($data->email) ) > 0 && $email_check > 0 && $password_check > 0 ) {
            
            $created = time();
            $password = md5($data->password);

            $status = 0;

            if(!is_email_exist($data->email)) {

                /*Inserting user values*/
                $date = new DateTime();
                $timedata = $date->getTimestamp();
                
                $sql1 = "INSERT INTO ".table_prifix."_users (first_name,last_name,email,password,phone,registration_date,status) VALUES (:fname,:lname,:email,:password,:phone,:registration_date,:status)";

                $db = getDB();
                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("fname", $data->first_name, PDO::PARAM_STR);
                $stmt1->bindParam("lname", $data->last_name, PDO::PARAM_STR);
                $stmt1->bindParam("email", $data->email, PDO::PARAM_STR);
                $stmt1->bindParam("password", $password, PDO::PARAM_STR);
                $stmt1->bindParam("phone", $data->phone, PDO::PARAM_INT);
                $stmt1->bindParam("status", $status, PDO::PARAM_STR);
                $stmt1->bindParam("registration_date", $timedata, PDO::PARAM_INT);
                $db = null;

                if($stmt1->execute()){
                    $response['status'] = 'success';
                    $response['message'] = 'User successfully Registered';
                    $subject = 'Thanks for your registration';
                    $custom_message = 'Hi '.$data->first_name.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$data->email.', We will inform you once your account is active.';
                    //$result = delivery_mail_template($email, $subject, '', $custom_message);
                    echo json_encode($response);
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'Error in getting userdata';
                    echo json_encode($response);
                }

            }else {
                $response['status'] = 'error';
                $response['message'] = 'User already registered, Please try again with diffrent email id';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * User Sign Up
 */
$app->post('/logOut', function() {
    
    $response = array();
    if( isset($_SESSION['token']) && $_SESSION['token'] !== '' ){
        $token = $_SESSION['token'];
        $new_token = '';
        $tokenExpiration = '';
        
        /*== Remove Token from database ==*/
        $db = getDB();
        $sql = "UPDATE ".table_prifix."_users SET token=:new_token,token_expire=:tokenExpiration WHERE token=:token";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("token", $token, PDO::PARAM_STR);
        $stmt->bindParam("new_token", $new_token, PDO::PARAM_STR);
        $stmt->bindParam("tokenExpiration", $tokenExpiration, PDO::PARAM_STR);
        $stmt->execute();

        /*== Empty sesstion variable  ==*/
        $_SESSION = array();
        session_destroy();

        $response['status'] = 'success';
        $response['message'] = "Logout successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "user not found!";
    }

    echo json_encode($response); 
});

/**
* User Login
*/
$app->post('/login', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;

    try {

        if( isset($data->email) && !filter_var($data->email, FILTER_VALIDATE_EMAIL) ){
            $response['status'] = 'error';
            $response['message'] = 'The email address you entered is not valid';
            echo json_encode($response);
        }

        $db = getDB();
        $userDataArr ='';
        $sql = "SELECT * FROM ".table_prifix."_users WHERE email=:email and password=:password";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $data->email, PDO::PARAM_STR);
        $password=md5($data->password);
        $stmt->bindParam("password", $password, PDO::PARAM_STR);
        $stmt->execute();
        $userData = $stmt->fetch();

        $db = null;

        if(!empty($userData)) {
            $temp = array();
            $temp['name'] = $userData['first_name'].' '.$userData['last_name'];
            $temp['email'] = $userData['email'];
            $temp['token'] = bin2hex(openssl_random_pseudo_bytes(15)); //enerate a random token
            $response['userData'] = $temp;
            $response['status'] = 'success';
            $response['message'] = 'User login successfully';
            $tokenExpiration = strtotime('+1 hour');
            update_token($temp['token'],  $userData['ID'], $tokenExpiration);
            $_SESSION['token'] = $temp['token'];
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Username or password is invalid!';            
        }

        echo json_encode($response);
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get cryto prices 
 */
$app->post('/crypto-prices', function() {
    
    $response = array();
    $trader = new trader();
    $buying_price = $trader->getBuyingPrices();

    if( !empty($buying_price) ){
        $response['cryptoPrices'] = $buying_price;
        $response['status'] = 'success';
        $response['message'] = "Logout successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting data!";
    }

    echo json_encode($response);
});

/**
 * Get cryto prices 
 */
$app->post('/update-api-keys', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $data = $request_data->apiData;

    $api_status = isset($data->api_status) ? $data->api_status : '';
    $api_key = isset($data->api_key) ? $data->api_key : '';
    $api_secret = isset($data->api_secret) ? $data->api_secret : '';

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        /*== Update api datas==*/
        //update_user_meta($user_data->ID, 'api_status',  $api_status);
        update_user_meta($user_data->ID, 'api_key',  $api_key);
        update_user_meta($user_data->ID, 'api_secret',  $api_secret);

        /*== get api datas ==*/
       //$apiData['api_status'] = get_user_meta($user_data->ID, 'api_status');
        $apiData['api_key'] = get_user_meta($user_data->ID, 'api_key');
        $apiData['api_secret'] = get_user_meta($user_data->ID, 'api_secret');

        $response['apiData'] = $apiData;
        $response['status'] = 'success';
        $response['message'] = "Api keys save successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting user!";
    }

    echo json_encode($response);
});

/**
 * Get cryto prices 
 */
$app->post('/add-buying-order', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody(), true);

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID) ) {


        if( isset($request_data['buyCrypto']) 
            && sizeof($request_data['buyCrypto']) > 0 
            && isset($request_data['currentCryptoData']) 
            && sizeof($request_data['currentCryptoData']) > 0 ) {

            
            $status = 1;
            $date = new DateTime();
            $timedata = $date->getTimestamp();
            
            $coin = isset($request_data['buyCrypto']['coin']) ? $request_data['buyCrypto']['coin'] : ''; 
            $amount = isset($request_data['buyCrypto']['amount']) ? floatval($request_data['buyCrypto']['amount']) : ''; 
            $position = isset($request_data['buyCrypto']['position']) ? floatval($request_data['buyCrypto']['position']) : ''; 
            $sell_check = isset($request_data['buyCrypto']['addSellDataCheck']) ? $request_data['buyCrypto']['addSellDataCheck'] : ''; 
            $desired_earning_percentage = isset($request_data['buyCrypto']['desired_earning_percentage']) ? $request_data['buyCrypto']['desired_earning_percentage'] : ''; 
            $desired_earning = isset($request_data['buyCrypto']['desired_earning']) ? floatval($request_data['buyCrypto']['desired_earning']) : ''; 
            $current_coin_price = isset($request_data['currentCryptoData'][$coin]['buyPrice']) ? $request_data['currentCryptoData'][$coin]['buyPrice'] : ''; 

            $frequently_check = isset($request_data['buyCrypto']['frequentlyBuyCheck']) ? $request_data['buyCrypto']['frequentlyBuyCheck'] : ''; 
            $desired_percentage_increase = isset($request_data['buyCrypto']['desired_percentage_increase']) ? $request_data['buyCrypto']['desired_percentage_increase'] : ''; 
            
            $sql = "INSERT INTO ".table_prifix."_buy_orders (user_id,coin,amount,position,sell_check,desired_earning_percentage,desired_earning,timestamp,current_coin_price,status,frequently_buy,frequently_buy_percentage) VALUES (:user_id,:coin,:amount,:position,:sell_check,:desired_earning_percentage,:desired_earning,:timestamp,:current_coin_price,:status,:frequently_buy,:frequently_buy_percentage)";

            $db = getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindParam("user_id", $user_data->ID, PDO::PARAM_INT);
            $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
            $stmt->bindParam("amount", $amount, PDO::PARAM_STR);
            $stmt->bindParam("position", $position, PDO::PARAM_STR);
            $stmt->bindParam("sell_check", $sell_check, PDO::PARAM_INT);
            $stmt->bindParam("desired_earning_percentage", $desired_earning_percentage, PDO::PARAM_INT);
            $stmt->bindParam("desired_earning", $desired_earning, PDO::PARAM_STR);
            $stmt->bindParam("timestamp", $timedata, PDO::PARAM_STR);
            $stmt->bindParam("current_coin_price", $current_coin_price, PDO::PARAM_STR);
            $stmt->bindParam("status", $status, PDO::PARAM_STR);
            $stmt->bindParam("frequently_buy", $frequently_check, PDO::PARAM_INT);
            $stmt->bindParam("frequently_buy_percentage", $desired_percentage_increase, PDO::PARAM_STR);

            $db = null;

            if($stmt->execute()){
                $response['status'] = 'success';
                $response['message'] = 'Your Buying order create successfully, We frequently send status on registered email id!';
                //$subject = 'Thanks for your registration';
                //$custom_message = 'Hi '.$data->first_name.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$data->email.', We will inform you once your account is active.';
                //$result = delivery_mail_template($email, $subject, '', $custom_message);
               // echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in getting userdata';
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = "Error in getting data!";
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting user!";
    }

    echo json_encode($response);
});

/**
 * Add selling order 
 */
$app->post('/add-selling-order', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody(), true);

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID) ) {


        if( isset($request_data['sellCrypto']) 
            && sizeof($request_data['sellCrypto']) > 0 
            && isset($request_data['currentCryptoData']) 
            && sizeof($request_data['currentCryptoData']) > 0 ) {

            
            $status = 1;
            $date = new DateTime();
            $timedata = $date->getTimestamp();
            
            $coin = isset($request_data['sellCrypto']['coin']) ? $request_data['sellCrypto']['coin'] : ''; 
            $amount = isset($request_data['sellCrypto']['amount']) ? floatval($request_data['sellCrypto']['amount']) : ''; 
            $position = isset($request_data['sellCrypto']['position']) ? floatval($request_data['sellCrypto']['position']) : ''; 

            $stop_loss_limit = isset($request_data['sellCrypto']['stop_loss_limit']) ? floatval($request_data['sellCrypto']['stop_loss_limit']) : ''; 
            $current_selling_price = isset($request_data['currentCryptoData'][$coin]['buyPrice']) ? $request_data['currentCryptoData'][$coin]['buyPrice'] : ''; 

            $frequently_sell = isset($request_data['sellCrypto']['frequentlySellCheck']) ? $request_data['sellCrypto']['frequentlySellCheck'] : ''; 
            $frequently_sell_percentage = isset($request_data['sellCrypto']['desired_percentage_increase']) ? $request_data['sellCrypto']['desired_percentage_increase'] : ''; 
            
            $sql = "INSERT INTO ".table_prifix."_sell_orders (user_id,coin,amount,position,timestamp,current_selling_price,stop_loss_limit,status,frequently_sell,frequently_sell_percentage) VALUES (:user_id,:coin,:amount,:position,:timestamp,:current_selling_price,:stop_loss_limit,:status,:frequently_sell,:frequently_sell_percentage)";

            $db = getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindParam("user_id", $user_data->ID, PDO::PARAM_INT);
            $stmt->bindParam("coin", $coin, PDO::PARAM_STR);
            $stmt->bindParam("amount", $amount, PDO::PARAM_STR);
            $stmt->bindParam("position", $position, PDO::PARAM_STR);
            $stmt->bindParam("stop_loss_limit", $stop_loss_limit, PDO::PARAM_STR);
            $stmt->bindParam("timestamp", $timedata, PDO::PARAM_STR);
            $stmt->bindParam("current_selling_price", $current_selling_price, PDO::PARAM_STR);
            $stmt->bindParam("status", $status, PDO::PARAM_INT);
            $stmt->bindParam("frequently_sell", $frequently_sell, PDO::PARAM_INT);
            $stmt->bindParam("frequently_sell_percentage", $frequently_sell_percentage, PDO::PARAM_STR);
            $db = null;

            if($stmt->execute()){
                $response['status'] = 'success';
                $response['message'] = 'Your Selling order create successfully, We frequently send status on registered email id!';
                //$subject = 'Thanks for your registration';
                //$custom_message = 'Hi '.$data->first_name.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$data->email.', We will inform you once your account is active.';
                //$result = delivery_mail_template($email, $subject, '', $custom_message);
               // echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in getting userdata';
                //echo json_encode($response);
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = "Error in getting data!";
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting user!";
    }

    echo json_encode($response);
});

/**
 * Get cryto prices 
 */
$app->post('/get-api-keys', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);

    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        $apiData = array();
        
        /*== get api datas ==*/
        $apiData['api_status'] = 1;
        $apiData['api_key'] = get_user_meta($user_data->ID, 'api_key');
        $apiData['api_secret'] = get_user_meta($user_data->ID, 'api_secret');

        $response['apiData'] = $apiData;
        $response['status'] = 'success';
        $response['message'] = "Api keys get successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting user!";
    }

    echo json_encode($response);
});

/**
 * Get All Buying Orders
 */
$app->post('/get-buying-order', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        $db = getDB();
        $sql = "SELECT * FROM ".table_prifix."_buy_orders WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_data->ID, PDO::PARAM_INT);
        $stmt->execute();
        $buying_ordres = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ( !empty($buying_ordres) && sizeof($buying_ordres) > 0 ){
            foreach ($buying_ordres as $key => $value) {
                $buying_ordres[$key]->timestamp = date("F j, Y, g:i a", $value->timestamp);
            }
        }

        $response['buyingOrders'] = $buying_ordres;
        $response['status'] = 'success';
        $response['message'] = "Buying orders get successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting orders!";
    }

    echo json_encode($response);
});

/**
 * Delete buying by id 
 */
$app->post('/cancel-buying-order', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }
    
    if( isset($data->buyingId) && $data->buyingId > 0 ){
        $buying_id = $data->buyingId;
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting buying order !";
        return $response;
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        /*== Remove Token from database ==*/
        $db = getDB();
        $new_status = 3;
        $sql = "UPDATE ".table_prifix."_buy_orders SET status=:new_status WHERE ID=:buying_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("buying_id", $buying_id, PDO::PARAM_INT);
        $stmt->bindParam("new_status", $new_status, PDO::PARAM_INT);
        $stmt->execute();

        $response['status'] = 'success';
        $response['message'] = "Order cancel successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting data!";
    }

    echo json_encode($response);
});

/**
 * Delete buying by id 
 */
$app->post('/cancel-selling-order', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }
    
    if( isset($data->sellingId) && $data->sellingId > 0 ){
        $selling_id = $data->sellingId;
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting buying order !";
        return $response;
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        /*== Remove Token from database ==*/
        $db = getDB();
        $new_status = 3;
        $sql = "UPDATE ".table_prifix."_sell_orders SET status=:new_status WHERE ID=:selling_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("selling_id", $selling_id, PDO::PARAM_INT);
        $stmt->bindParam("new_status", $new_status, PDO::PARAM_INT);
        $stmt->execute();

        $response['status'] = 'success';
        $response['message'] = "Order cancel successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting data!";
    }

    echo json_encode($response);
});

/**
 * Get All Sellings Orders
 */
$app->post('/get-selling-orders', function() {

    $response = array();
        
    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $headers = request_headers();
    if( isset($headers['AUTHRIZATION']) && $headers['AUTHRIZATION'] !== '' ){
        $token = $headers['AUTHRIZATION'];
    }else {
        $app->halt('403', 'Invalid User Authentication!');   
    }

    $user_data = get_user_data('ID', 'token', $token);
    if( !empty($user_data) && sizeof($user_data) > 0 && isset($user_data->ID)) {

        $db = getDB();
        $sql = "SELECT * FROM ".table_prifix."_sell_orders WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_data->ID, PDO::PARAM_INT);
        $stmt->execute();
        $selling_ordres = $stmt->fetchAll(PDO::FETCH_OBJ);

        if ( !empty($selling_ordres) && sizeof($selling_ordres) > 0 ){
            foreach ($selling_ordres as $key => $value) {
                $selling_ordres[$key]->timestamp = date("F j, Y, g:i a", $value->timestamp);
            }
        }

        $response['sellingOrders'] = $selling_ordres;
        $response['status'] = 'success';
        $response['message'] = "Buying orders get successfully!";
    }else {
        $response['status'] = 'error';
        $response['message'] = "Error in getting orders!";
    }

    echo json_encode($response);
});


$app->run();
