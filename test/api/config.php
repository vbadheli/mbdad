<?php
/**
 * Database configuration
 */
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'coinbase_bot');
define('table_prifix', 'bot');

/**
 * Setup site url home url
 */
define('SITE_URL', 'http://localhost/coinbase');
define('HOME_URL', 'http://localhost/coinbase');

/**
 * Setup All coinname  
 */
define('CRYPTO_BTC','BTC');
define('CRYPTO_ETH','ETH');
define('CRYPTO_LTC','LTC');

define('PAYMENT_METHOD_NAME','USD Wallet');
define('CRYPTO','BTC');
define('TIMEZONE','Europe/London'); 

define('ROCKETCHAT_REPORTING', false); 
define('ROCKETCHAT_WEBHOOK', ''); 

// Simulate or really commit buy/sells?
//if false, transactions are really paid, with true it's just a simulation
define('SIMULATE',false); 

// The currency you're going to pay with when buying new coins
// This can also be a crypto currency you have on Coinbase
// EUR or USD or even ETH or BTC
define('CURRENCY','USD');

//Set minimum coin value
define('MIN_BTC','0.00009200');

/**
 * Setup coinbase API Keys
 */
/*== Sandbox details ==*/
/*define('COINBASE_KEY','0dmX6kGrr1Qi0IDQ');
define('COINBASE_SECRET','Dz3tIkK1dpqeh9PLPejRuFHUhXpVv974');*/

/*== Vikas Account keys ==*/
// define('COINBASE_KEY','31JYC0Ihdn3A1gsc');
// define('COINBASE_SECRET','ZsOJ452CgScFN3MNGZ58wFZBsxAdnytK');

/*== Client  Account details ==*/
define('COINBASE_KEY','No5rR6xQImwtTiWz');
define('COINBASE_SECRET','SU3FzsUdrszP6mfaR1uz1IhfPTBvJMNW');