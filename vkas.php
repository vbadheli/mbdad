<?php

function complete_mark_order_by_api($url = '', $order_id, $key){

    $url = 'https://yourfoodorder.online/wp-content/yfo-dialadelivery/yfo_dd_otp.php';
    
    $fields_string = '';

    $fields = array(
        'yfo_order_id' => 123456,
    );

    $fields_string = json_encode($fields);

    //open connection
    $ch = curl_init();

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Content-Length: ' . strlen($fields_string);
    $headers[] = 'Key: '.$key;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);
}
