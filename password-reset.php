<?php 
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html ng-app="myApp" ng-controller="mainController"> 
<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="assets/css/angular-material.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/custom-style.css">    
    <!-- <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,900" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Delivering</title>
</head>
<body>  
<div layout="column" layout-fill class="header-logo-wrap">
    <div class="header-logo">
       <img src="assets/images/logo.png" alt="Logo">
    </div>
</div><!-- /.header-wrap -->
<div layout="column" layout-fill class="login-signup-wrap">
    <div class="login-signup-tabs">
        <md-tabs flex layout="column" md-dynamic-height md-stretch-tabs="always">
            <md-tab flex layout="column"> <span class="vender-style">Reset Password</span></md-tab>
            <!-- <md-tab flex layout="column" ui-sref="login"><span class="vender-style">Log In</span></md-tab> -->
        </md-tabs>
        <md-content class="dv-padding">
            <div class="newadd-form login-signup-form">
                <form name="Form" id="target" class="newadd-stores-form">
                    <md-input-container class="md-icon-float md-block">
                        <label>Password</label>
                        <md-icon><img src="assets/images/form-password.png"></md-icon>
                        <input type="hidden" name="u_id" id="u_id" value="<?= $id;?>" md-no-asterisk/>
                        <input type="password" name="password" ng-model="user.password" id="password" required="" pattern="^[A-Za-z0-9!@#$%^&*()_]{6,20}$" md-minlength="6" md-maxlength="20" md-no-asterisk/>
                        <div ng-messages="Form.password.$error" role="alert"  class="form-errors first-name-error">
                            <div ng-message="required">Password is required.</div>
                            <div ng-message="pattern">Password is not correct.</div>
                            <div ng-message="minlength">Password is too small</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-icon-float md-block">
                        <label>Confirm Password</label>
                        <md-icon><img src="assets/images/form-password.png"></md-icon>
                        <input type="password" name="rePassword" ng-model="user.rePassword" id="rePassword" required="" compare-to="user.password" md-minlength="6" md-maxlength="20" pattern="^[A-Za-z0-9!@#$%^&*()_]{6,20}$" md-no-asterisk/>
                        <div ng-messages="Form.rePassword.$error" role="alert" class="form-errors first-name-error">
                            <div ng-message="required">Password is required.</div>
                            <div ng-message="pattern">Password is not correct.</div>
                            <div ng-message="compareTo">Must not match</div>
                            <div ng-message="minlength">Password is too small</div>
                        </div>
                    </md-input-container>
                    <div class="login-btn">
                        <md-button type="submit" ng-disabled="!Form.$valid" id="reset" class="btn">Submit</md-button>
                    </div>
                </form>
            </div>
        </md-content>
    </div>
</div>
    <script type="text/javascript">
         jQuery( document ).ready(function($) {
            $( "#target" ).submit(function( event ) {

                  var id = $('#u_id').val();
                  var password = $('#password').val();
                  var rePassword = $('#rePassword').val();

                  $.ajax({
                      type: "POST",
                      url: 'http://earth.ourgoogle.in/clients/dipen-patel/delivering/api/v1/reset-password',
                      data: { 
                          'id' : id,
                          'password' : password,
                          'rePassword' : rePassword
                      },
                      cache: false,
                      success: function(response){
                        console.log(response);                        
                        if(response.status == 'success'){
                            window.location = 'http://earth.ourgoogle.in/clients/dipen-patel/delivering/';
                        }
                      }
                    });
              event.preventDefault();
            });
        });
    </script>
    <!-- <script type="text/javascript" src="assets/js/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- <script type="text/javascript" src="assets/js/jquery/materialize.min.js"></script> -->
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCaHbsH1oYOtOiQ3RBTaWoaWW_rlibWtcM&libraries=places"></script>
    <!-- #Incude Anguler libraries -->
    <script src="assets/js/angular/angular.min.js"></script>
    <script src="assets/js/angular/angular-animate.min.js"></script>
    <script src="assets/js/angular/angular-aria.min.js"></script>
    <script src="assets/js/angular/angular-cookies.min.js"></script>
    <script src="assets/js/angular/angular-loader.min.js"></script>
    <script src="assets/js/angular/angular-messages.min.js"></script>
    <script src="assets/js/angular/angular-route.min.js"></script>
    <script src="assets/js/angular/angular-sanitize.min.js"></script>
    <script src="assets/js/angular/angular-material.min.js"></script>
    <script src="assets/js/angular/angular-ui-router.min.js"></script>
    <script src="assets/js/angular/ng-map.min.js"></script>
    <script src="assets/js/angular/ngAutocomplete.js"></script>
    
    <!-- #Incude Anguler Custom Code -->
    <script type="text/javascript" src="app/app.js"></script>
    <script type="text/javascript" src="app/filters.js"></script>
    <script type="text/javascript" src="app/factory.js"></script>
    <script type="text/javascript" src="app/services.js"></script>
    <script type="text/javascript" src="app/config.js"></script>
    <script type="text/javascript" src="app/run.js"></script>
    <script type="text/javascript" src="app/controller.js"></script>
    <script type="text/javascript" src="app/directive.js"></script>
</body>
</html>