<?php

date_default_timezone_set('Australia/Victoria');
ini_set('session.gc_maxlifetime', 10800);
session_start();

/*== Requaire Config file ==*/
include('../config.php');
include('../ncc_api/src/NationalCrimeCheck.php');
include_once('url-shorter.php');

// Require the bundled autoload file - the path may need to change
require '../twilio-php-master/Twilio/autoload.php';

//Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
 
 /**
 * Encode string/ Int 
 */
function encode($string) {

    $key = sha1(GOGLE_API_KEY);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}

 /**
 * Decode string/ Int 
 */
function decode($string) {

    $key = sha1(GOGLE_API_KEY);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

/**
 * Short Url
 */
function short_url($url){
    // Create instance with key
    $key = GOGLE_API_KEY;
    $googer = new GoogleURLAPI($key);
    // Test: Shorten a URL
    return $googer->shorten($url);
}

/**
 * Get database
 */
function getDB() {

    $dbhost=DB_HOST;
    $dbuser=DB_USERNAME;
    $dbpass=DB_PASSWORD;
    $dbname=DB_NAME;

    $dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbConnection->exec("set names utf8");
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}

/**
 * Send message
 */
function send_message($to, $message_body){

    // Your Account SID and Auth Token from twilio.com/console
    $sid = TWILLO_SID;
    $token = TWILLO_TOKEN;

    $client = new Client($sid, $token);

    //Use the client to do fun stuff like send text messages!
    return $client->messages->create(
        $to,
        array(
            'from' => '+61437766049',
            'body' => $message_body
        )
    );
}

/**
* API key encryption
*/
function apiToken($uid) {
    return hash('sha256', $uid);
}

/**
 * Get Internal User Details
 */
function internalUserDetails($input) {

    try {

        $db = getDB();
        $sql = "SELECT id, first_name,last_name, email FROM ".table_prifix."_user WHERE email=:input";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("input", $input,PDO::PARAM_STR);
        $stmt->execute();
        $usernameDetails = $stmt->fetch(PDO::FETCH_OBJ);
        $usernameDetails->token = apiToken($usernameDetails->id);
        $db = null;
        return $usernameDetails;

    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Get Last Insert Id of any table
 */
function getLastInsertId($table_name){

    try {

        $db = getDB();
        $sql = "SELECT id FROM ".table_prifix."_$table_name ORDER BY id DESC LIMIT 1";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        if( !empty($result) && sizeof($result) > 0 && isset($result->id) ){
            return $result->id;
        }else{
            return false;
        }
    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Get Store Details by user ID
 */
function getUserStoreData($user_id) {

    try {

        $db = getDB();
        $sql = "SELECT * FROM ".table_prifix."_stores WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id,PDO::PARAM_INT);
        $stmt->execute();
        $userStoreData = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        return $userStoreData;

    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

/**
 * Get Store Details by user ID
 */
function getAllUserData($user_id) {

    try {

        $db = getDB();

        $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role,user.activataion_key,user.status,user.user_agent,user.dnd_mode,user.regisration_date, usermeta.* FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        
        $final_arr = (array)$stmt->fetch(PDO::FETCH_OBJ);

        /*== Change int status to boolen ==*/
        if( isset($final_arr['dnd_mode']) && $final_arr['dnd_mode'] > 0 ){
            $final_arr['dndMode'] = true;
        }else{
            $final_arr['dndMode'] = false;
        }

        if( isset($final_arr['availability_slots'])){
            $final_arr['availability_slots'] = json_decode($final_arr['availability_slots']);
        }
        if( isset($final_arr['suburbs'])){
            $final_arr['suburbs'] = json_decode($final_arr['suburbs']);
        }
        // echo json_encode($final_arr);exit;
        
        $db = null;
        if( !empty($final_arr) && sizeof($final_arr) > 0 ) {
            return $final_arr;
        }



        return false;

    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Get Store Details by user ID
 */
function getVendorUserDataByDeliveryId($user_id) {

    try {

        $db = getDB();

        $sql1 = "SELECT user.id, usermeta.inhouse_vendor FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.id=:user_id";
        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt1->execute();

        $vendor_arr = (array)$stmt1->fetch(PDO::FETCH_OBJ);
        // echo "<pre>"; print_r($vendor_arr);exit;
        if($vendor_arr)
        {
            $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role,user.activataion_key,user.status,user.user_agent,user.dnd_mode,user.regisration_date, usermeta.* FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.id=:user_id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $vendor_arr['inhouse_vendor'], PDO::PARAM_INT);
            $stmt->execute();

            $final_arr = (array)$stmt->fetch(PDO::FETCH_OBJ);
            
            /*== Change int status to boolen ==*/
            if( isset($final_arr['dnd_mode']) && $final_arr['dnd_mode'] > 0 ){
                $final_arr['dndMode'] = true;
            }else{
                $final_arr['dndMode'] = false;
            }

            if( isset($final_arr['availability_slots'])){
                $final_arr['availability_slots'] = json_decode($final_arr['availability_slots']);
            }
            if( isset($final_arr['suburbs'])){
                $final_arr['suburbs'] = json_decode($final_arr['suburbs']);
            }
            
            $db = null;
            if( !empty($final_arr) && sizeof($final_arr) > 0 ) {
                return $final_arr;
            }


        }
        return false;


    } catch(PDOException $e) {
        return false;
    }
}


/**
 * Genrate Rondom String
 */
function randomString() {

    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

/**
 * Delivery Email Template
 */
function delivery_mail_template($to, $subject, $from, $custom_message){

    if($to !== '' && $subject !== '' ){
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        if($from !== ''){
            // More headers
            $headers .= 'From: '.$from. "\r\n";
            $headers .= 'Cc: '.$from."\r\n";
        }else {
            // More headers
            $headers .= 'From: Dialadelivery <service@yourfoodorder.online>' . "\r\n";
            $headers .= 'Cc: <service@yourfoodorder.online>' . "\r\n";
        }

        $header = '<style type="text/css">
            a{color:rgb(8, 32, 230); text-decoration: none;}
            a:hover{color:rgb(23, 18, 19);text-decoration:underline;}
            </style>
            <div style="background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #62cbe4 0%, #c85d79 100%) repeat scroll 0 0;
                                color: #fff;font-size: 13px;font-weight: 400;height: auto;width: 100%;padding:0px 0 4px;max-width:550px;">
                <table style="width: 100%; padding-right:15px; padding-left:10px;padding-top:7px;" border="0" cellspacing="0" cellpadding="2">
                    <tbody>
                        <tr>
                            <td style="text-align: center; vertical-align: middle; width:8%;">
                                <a href="#"><img style="width:25px; height:25px;" src="'.SITE_URL.'/assets/images/Facebook.png" alt="Facebook" />
                                </a>
                            </td>
                            <td style="text-align: center; vertical-align: middle; width: 8%;">
                                <a href="#"><img style="width:25px; height:25px;" src="'.SITE_URL.'/assets/images/Twitter.png" alt="Twitter" />
                                </a>
                            </td>
                            <td style="text-align: center; vertical-align: middle; width: 8%;">
                                <a href="#"><img style="width:25px; height:25px;" src="'.SITE_URL.'/assets/images/pinterest.png" alt="Pinterest" />
                                </a>
                            </td>
                            <td style="text-align: center; vertical-align: middle; width: 37%;"><img src="'.SITE_URL.'/assets/images/logo.png" alt="icon" style="width: 120px;" />
                            </td>
                            
                            <td style="text-align: right; vertical-align: middle; width: 8%;">
                                <a href="#"><img style="width:27px; height:28px;" src="'.SITE_URL.'/assets/images/mail.png" alt="email" />
                                </a>
                            </td>
                            <td style="text-align: right; vertical-align: middle; width: 8%;">
                                <a href="#"><img style="width:25px; height:25px;" src="'.SITE_URL.'/assets/images/Call.png" alt="Call Us" />
                                </a>
                            </td>
                            <td style="text-align: right; vertical-align: middle; width: 8%;">
                                <a href="#"><img style="width:25px; height:25px;" src="'.SITE_URL.'/assets/images/live-chat.png" alt="Live Chat" />
                                </a>
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
                            
                <div style="border-bottom: 1px solid rgba(255, 255, 255, 0.38);margin-top: 11px;"></div>';

        $footer = '<div style="border-bottom: 1px solid rgba(255, 255, 255, 0.38);"></div>
                    <p style="text-align:center">
                    <em>
                        <font face="Verdana">
                            <span style="color: #fff;">&copy; 2017 Dialadelivery.com</span>
                        </font>
                    </em>
                </p>
            </div>';

        $message = '';
        $message.= $header;
        $message.= '<div style="margin:10px 20px;">
        <div style="text-align:justify;min-height:150px;padding:10px 25px;">'.$custom_message.'</div>';
        $message.= $footer;

        // return mail($to, $subject, $message, $headers);
        return true;
    }

    return false;
}

/**
 * Get address From Latitude Longitude
 */
function get_address_from_lat_lng($latitude, $longitude){

    $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false');
    $output = json_decode($geocodeFromLatLong);
    $address = ( $output->status=="OK" )? $output->results[0]->formatted_address : false;
    return $address;
}

/**
 * Get delivery boy information by order id
 */
function getDeliveryBoyPersonalData($order_id){

    try {

        $db = getDB();

        $sql = "SELECT delivery_agent_id FROM ".table_prifix."_order WHERE id=:order_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
        $stmt->execute();
        $delivery_boy_data = (array)$stmt->fetch(PDO::FETCH_OBJ);

        if( !empty($delivery_boy_data) && sizeof($delivery_boy_data) ){
            return getAllUserData($delivery_boy_data['delivery_agent_id']);
        }

        return false;

    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

/**
 * Get delivery user by User Role
 * @ deliveryman
 * @ vendor
 */
function getUserByUserRole($user_role='deliveryman'){

    try {

        $userData = array();

        $db = getDB();
        $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role,user.activataion_key,user.status,user.user_agent,user.dnd_mode,user.regisration_date, usermeta.* FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.user_role=:user_role";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_role", $user_role, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetchAll(PDO::FETCH_OBJ);


        if( !empty($userData) && sizeof($userData) > 0 ){
            foreach ($userData as $key => $value) {
                if( $value->status > 0 ){
                    $value->angularStatus = true;
                }else{
                    $value->angularStatus = false;
                }
            }
        }

        return $userData;

    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Get delivery user by Delivery Mode
 * @ deliveryman
 * @ vendor
 */
function getDeliverymanByDeliveryMode($deliverymode, $user_id){

    try {

        $userData = array();
        $db = getDB();

        if( $deliverymode == 'inhouse' ){
            $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role,user.activataion_key,user.status,user.user_agent,user.dnd_mode,user.regisration_date, usermeta.* FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.user_role='deliveryman' AND usermeta.delivery_mode=:deliverymode AND usermeta.inhouse_vendor=:user_id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        }else {
           $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role,user.activataion_key,user.status,user.user_agent,user.dnd_mode,user.regisration_date, usermeta.* FROM ".table_prifix."_user as user Left Join ".table_prifix."_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.user_role='deliveryman' AND usermeta.delivery_mode=:deliverymode";
            $stmt = $db->prepare($sql);
        }

        $stmt->bindParam("deliverymode", $deliverymode, PDO::PARAM_STR);
        $stmt->execute();
        $userData = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
        return $userData;

    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Order Status Array
 * [0]=> 'awaiting-pickup'
 * [1]=> 'on-hold'
 * [2]=> 'picked-up'
 * [3]=> 'processing'
 * [4]=> 'delivery-ready'
 * [5]=> 'out-of-delivery'
 * [6]=> 'delivered-return'
 * [7]=> 'complete'
 */
function orderStatus($index = 0){

    $order_status = array(
            'awaiting-pickup',
            'on-hold',
            'processing',
            'picked-up',
            'delivery-ready',
            'out-of-delivery',
            'delivered-return',
            'complete',
        );

    return $order_status[$index];
}

/**
 * Check table id is valid or not
 */
function checktableIdValid($id, $table = 'order'){

    try {

        $userData = array();
        $table_name = table_prifix.'_'.$table;

        $db = getDB();
        $sql = "SELECT * FROM $table_name WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id, PDO::PARAM_INT);
        $stmt->execute();

        $result = (array)$stmt->fetch(PDO::FETCH_OBJ);
        $result = current($result);

        if(!empty($result) && sizeof($result) > 0 ){
            return true;
        }

        return false;

    } catch(PDOException $e) {
        return false;
    }
}

/**
 * Get state name and code
 */
function getAllStates(){
    
    $town_data = array();

    $db = getDB();
    $sql = "SELECT distinct(`state`) as `state`,`state_code` FROM ".table_prifix."_au_towns order by state asc";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /*if(!empty($result) && sizeof($result) > 0 ){
        foreach ($result as $key => $value) {
           $town_data[] = $value['state'];
        }
        return $town_data;
    }*/

    return $result;
}


/**
 * Get Town Name by state code
 */
function getCityNameByStateCode($state_code){
    
    $town_data = array();

    $db = getDB();
    $sql = "SELECT distinct(`urban_area`) FROM ".table_prifix."_au_towns WHERE state_code=:state_code";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("state_code", $state_code, PDO::PARAM_INT);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


    if(!empty($result) && sizeof($result) > 0 ){
        foreach ($result as $key => $value) {
            // if($value['urban_area'] !="" && $value['urban_area'] != null)
                $town_data[] = $value['urban_area'];
        }
        return $town_data;
    }

    return $town_data;
}

function getSuburbsNameByStateCode($state_code){
    
    $town_data = array();

    $db = getDB();
    $sql = "SELECT distinct(`name`),id FROM ".table_prifix."_au_towns WHERE state_code=:state_code";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("state_code", $state_code, PDO::PARAM_INT);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


    if(!empty($result) && sizeof($result) > 0 ){
        foreach ($result as $value) {
            if($value['name'] !="" && $value['name'] != null)
            {

                $item = [];
                $item['name'] = $value['name'];
                $item['id'] = $value['id'];
                $town_data[] = $item;
            }
        }
        return $town_data;
    }

    return $town_data;
}


/**
 * Get Town Name by state code
 */
function getTownNameByStateCode($state_code){
    
    $town_data = array();

    $db = getDB();
    $sql = "SELECT `name` FROM ".table_prifix."_au_towns WHERE state_code=:state_code";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("state_code", $state_code, PDO::PARAM_INT);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);


    if(!empty($result) && sizeof($result) > 0 ){
        foreach ($result as $key => $value) {
           $town_data[] = $value['name'];
        }
        return $town_data;
    }

    return $town_data;
}

/*== Get distance B/W given two location ==*/
function get_distance_and_duration($from, $to ){
    
    $response = array();

    if($to !== '' && $from !== '' ){

        $from = urlencode($from);
        $to = urlencode($to);
        $data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&sensor=false");
        $data = json_decode($data);
        $time = 0;
        $distance = 0;

        if(isset($data->rows[0]->elements[0]->status) && $data->rows[0]->elements[0]->status == 'OK'){
            $distanceText = $data->rows[0]->elements[0]->distance->text;
            $distance = filter_var($distanceText, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $response['distance'] = $distance;
            $response['duration'] =  $data->rows[0]->elements[0]->duration->text;
            $response['distanceText'] = $data->rows[0]->elements[0]->distance->text;
        }

        return  $response;
    }

    return $response;
}

/*== Check api key is valid or not ==*/
function isValidApi($key){
    
    if($key !== '' ){

        $db = getDB();
        $sql = "SELECT * FROM ".table_prifix."_usermeta WHERE api_key=:api_key";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("api_key", $key, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if( !empty($result) && sizeof($result) > 0 ){
            return true;
        }
    }

    return false;
}

/*== Check userID by api key ==*/
function getUserIDByApiKey($key){
    
    if($key !== '' ){

        $db = getDB();
        $sql = "SELECT `user_id` FROM ".table_prifix."_usermeta WHERE api_key=:api_key";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("api_key", $key, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if( !empty($result) && isset($result['user_id'])){
            return $result['user_id'];
        }
    }

    return false;
}

/*== Check userID by api key ==*/
function getUserCommissionAndChargeByApiKey($key){
    
    if($key !== '' ){

        $db = getDB();
        $sql = "SELECT `user_id`,`commission`,`deliveryman_per_km_charge`, `vendor_per_km_charge` FROM ".table_prifix."_usermeta WHERE api_key=:api_key";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("api_key", $key, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if( !empty($result) ){
            return $result;
        }
    }

    return false;
}

/*== Get Order Quote ==*/
function get_order_quote($vendor_commision, $per_km_charge, $distance){

    $per_km_charge = floatval($per_km_charge);
    $distance = floatval($distance);

    if(/*$vendor_commision > 0 &&*/ $per_km_charge > 0 && $distance > 0 ){
        $distance = round( $distance, 1, PHP_ROUND_HALF_UP);
        if($distance < 5){
            return $per_km_charge*5;
        }else {
            return $per_km_charge*$distance;
        }
    }

    return false;
}

/**
 * Get lat/long from given address 
 */
function get_lat_long_form_address($address){

    $response = array();

    if($address !== ''){
        // Get JSON results from this request
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
        $geo = json_decode($geo, true); // Convert the JSON to an array

        if ($geo['status'] == 'OK') {
            return $geo['results'][0]['geometry']['location'];
        }
    }

    return $response;
}

/**
 * GET all vendor order 
 */
function getVendorOrders($user_id){

    if( $user_id > 0 ){

        $sql = "SELECT * FROM ".table_prifix."_order LEFT JOIN ".table_prifix."_order_items ON delevering_order.id = delevering_order_items.order_id WHERE vendor_id=:vendor_id";
        
        $db = getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);

        if( $stmt->execute() ){
            $userOrders = array();
            $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
            $order_data = array();
            if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                foreach ($userOrders as $key => $value) {
                    $temp = $itemmeta = array();
                    if($value->id > 0 ){
                        if( array_key_exists($value->id, $order_data)){
                            $itemmeta['item_name'] = $value->item_name;
                            $itemmeta['item_image'] = $value->item_image;
                            $itemmeta['item_type'] = $value->item_type;
                            $itemmeta['item_quantity'] = $value->item_quantity;
                            $itemmeta['item_value'] = $value->item_value;
                            $itemmeta['item_height'] = $value->item_height;
                            $itemmeta['item_width'] = $value->item_width;
                            $itemmeta['item_weight'] = $value->item_weight;
                            $itemmeta['item_status'] = $value->item_status;
                            $order_data[$value->id]['orderItems'][] = $itemmeta;
                        }else {
                            $temp['order_id'] = $value->id;
                            $temp['vendor_id'] = $value->vendor_id;
                            $temp['order_title'] = $value->order_title;
                            $temp['pickup_address'] = $value->pickup_address;
                            $temp['deliver_address'] = $value->deliver_address;
                            $temp['pickup_time'] = $value->pickup_time;
                            $temp['deliver_time'] = $value->deliver_time;
                            $temp['order_date'] = date('d M Y H:i:s', $value->order_date);
                            $temp['status_modified'] = $value->status_modified;
                            $temp['delivery_agent_id'] = $value->delivery_agent_id;
                            $temp['order_status'] = $value->order_status;

                            $order_data[$value->id] = $temp;

                            $itemmeta['item_name'] = $value->item_name;
                            $itemmeta['item_image'] = $value->item_image;
                            $itemmeta['item_type'] = $value->item_type;
                            $itemmeta['item_quantity'] = $value->item_quantity;
                            $itemmeta['item_value'] = $value->item_value;
                            $itemmeta['item_height'] = $value->item_height;
                            $itemmeta['item_width'] = $value->item_width;
                            $itemmeta['item_weight'] = $value->item_weight;
                            $itemmeta['item_status'] = $value->item_status;
                            $order_data[$value->id]['orderItems'][] = $itemmeta;
                        }
                    }
                }

                return $order_data;
            }
        }

    }
    
    return false;
}

/**
 * Check meta exist 
 */
function checkUserMetaExist($user_id){

    if( $user_id > 0 ){
        $db = getDB();

        /*== Check meta exist ==*/
        $sqlSelect = "SELECT * FROM ".table_prifix."_usermeta WHERE user_id=:user_id";
        $stmtSelect = $db->prepare($sqlSelect);
        $stmtSelect->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmtSelect->execute();
        $userMetaData = (array)$stmtSelect->fetch(PDO::FETCH_OBJ);
        $user_meta = current($userMetaData);

        if( !empty($user_meta) && sizeof($user_meta) > 0 ){
            return true;
        }
    }

    return false;
}

/**
 * GET user role by id/Email id  
 */
function get_user_role( $user_id ){

    if($user_id > 0 ){

        $db = getDB();
        $sql = "SELECT user_role FROM ".table_prifix."_user WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($userData) && isset($userData['user_role']) ){
            return $userData['user_role'];
        }
    }

    return false;
}

/**
 * Check user is admin or not  
 */
function is_admin( $user_id ){

    if($user_id > 0 ){

        $db = getDB();
        $sql = "SELECT user_role FROM ".table_prifix."_user WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($userData) && isset($userData['user_role']) && $userData['user_role'] == 'administrator'){
            return true;
        }
    }

    return false;
}

/**
 * Check user is vendor or not  
 */
function is_vendor( $user_id ){

    if($user_id > 0 ){

        $db = getDB();
        $sql = "SELECT user_role FROM ".table_prifix."_user WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($userData) && isset($userData['user_role']) && $userData['user_role'] == 'vendor'){
            return true;
        }
    }

    return false;
}

/**
 * Check user is deliveryman or not  
 */
function is_deliveryman( $user_id ){

    if($user_id > 0 ){

        $db = getDB();
        $sql = "SELECT user_role FROM ".table_prifix."_user WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($userData) && isset($userData['user_role']) && $userData['user_role'] == 'deliveryman'){
            return true;
        }
    }

    return false;
}

/**
 * Get User total release amount and remaing release amount
 */
function get_released_amount($user_id){

    $response = array();

    if($user_id > 0 ){

        $db = getDB();
        $sql = "SELECT usermeta.release_remainig,usermeta.released_amount FROM ".table_prifix."_usermeta as usermeta WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $userData = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($userData) && sizeof($userData) > 0 ){
            return $userData;
        }
    }

    return $response;
}

/**
 * Get User total release amount and remaing release amount
 */
function update_remaining_released_amount($user_id, $amount){

    if($user_id > 0 ){

        $db = getDB();

        if( checkUserMetaExist($user_id) ){
            $sql = "UPDATE ".table_prifix."_usermeta SET release_remainig=:release_remainig WHERE user_id=:user_id";
        }else {
            $sql = "INSERT INTO ".table_prifix."_usermeta (release_remainig,user_id) VALUES (:release_remainig,:user_id)";
        }

        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam("release_remainig", $amount, PDO::PARAM_STR);
        
        if( $stmt->execute() ){
            return true;
        }
    }

    return false;
}

/**
 * get deliveryman charge from and order and update release amount  
 */
function get_order_charge_update_release_amt($order_id){

    if($order_id > 0 ){

        $db = getDB();

        $sql = "SELECT order.deliveryman_amount,order.delivery_agent_id FROM ".table_prifix."_order as `order` WHERE order.id=:order_id AND order.order_status='complete'";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
        $stmt->execute();
        $order_data = (array)$stmt->fetch(PDO::FETCH_OBJ);

        if( !empty($order_data) 
            && isset($order_data['deliveryman_amount'])
            && $order_data['deliveryman_amount'] > 0
            && isset($order_data['delivery_agent_id'])
            && $order_data['delivery_agent_id'] > 0 ){
        
            $release_amount_data = get_released_amount($order_data['delivery_agent_id']);
            if( isset($release_amount_data['release_remainig']) ){
                $release_amount = floatval($release_amount_data['release_remainig']) + floatval($order_data['deliveryman_amount']) ;
                update_remaining_released_amount($order_data['delivery_agent_id'], $release_amount);
            }
        }
    }

    return false;
}

/**
 * Check user status is active or not
 */
function is_user_active($user_id){

    if( $user_id > 0 ){
        
        $db = getDB();
        $sql = "SELECT `status` FROM ".table_prifix."_user WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_STR);
        $stmt->execute();
        $user_data = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( isset($user_data['status']) && $user_data['status'] > 0 ){
            return true;
        }

    }

    return false;
}


/**
 * api order exist or not
 */
function is_order_exist($header_key, $order_id){

    if( $header_key !== '' &&  $order_id > 0 ){

        $user_id = getUserIDByApiKey($header_key);
        if( $user_id > 0 ){
            $db = getDB();
            $sql = "SELECT * FROM ".table_prifix."_order WHERE vendor_api_order_id=:order_id AND vendor_id=:user_id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->execute();
            $order_data = (array)$stmt->fetch(PDO::FETCH_OBJ);
            $order_data = current($order_data);
            if( !empty($order_data) && sizeof($order_data) > 0 ){
                return true;    
            }
        }
    }

    return false;
}

/**
 * Complete order by api
 */
function complete_mark_order_by_api($url = '', $order_id, $key){

    $url = 'https://junoon.yourfoodorder.online/wp-content/yfo-dialadelivery/yfo_dd_otp.php';
    
    $fields_string = '';

    $fields = array(
        'yfo_order_id' => $order_id,
    );

    $fields_string = json_encode($fields);

    //open connection
    $ch = curl_init();

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Content-Length: ' . strlen($fields_string);
    $headers[] = 'Key: '.$key;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);
}

/**
 * Send deliveryman data to vendor once deliveryman accept order 
 */
function send_deliveryman_data_to_vendor($url = '', $orderData, $key){

    $url = 'https://junoon.yourfoodorder.online/wp-content/yfo-dialadelivery/yfo_dd_driver_inf.php';
    
    $fields_string = '';
    $fields_string = json_encode($orderData);

    //open connection
    $ch = curl_init();

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Content-Length: ' . strlen($fields_string);
    $headers[] = 'Key: '.$key;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($orderData));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);

    return $result;
}

/**
 * Is order type api 
 */
function is_order_type_api($order_id){

    if( $order_id > 0 ){
        
        $db = getDB();
        $sql = "SELECT `vendor_api_order_id` FROM ".table_prifix."_order WHERE id=:order_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
        $stmt->execute();
        $order_data = (array)$stmt->fetch(PDO::FETCH_OBJ);
        $order_data = current($order_data);
        if( !empty($order_data) && sizeof($order_data) > 0 ){
            return true;    
        }
    }

    return false;
}

/***
 * check is vendor 
 */
function checkIsValidInhouseOrder($deliveryman_id ,$vendor_id ) {

    if( $vendor_id > 0 && $deliveryman_id > 0 && checkUserMetaExist($deliveryman_id) ){
        
        $db = getDB();
        $sql = "SELECT * FROM ".table_prifix."_usermeta WHERE `inhouse_vendor`=:inhouse_vendor AND `user_id`=:deliveryman_id AND delivery_mode='inhouse'";
        $stmt = $db->prepare($sql);

        $stmt->bindParam("inhouse_vendor", $vendor_id, PDO::PARAM_INT);
        $stmt->bindParam("deliveryman_id", $deliveryman_id, PDO::PARAM_INT);
        $stmt->execute();

        $user_data = (array)$stmt->fetch(PDO::FETCH_OBJ);
        $user_data = current($user_data);
        if( !empty($user_data) && sizeof($user_data) > 0 ){
            return true;    
        }
    }

    return false;
}

/***
 * check is Deliveryman Active 
 */
function is_deliveryman_active($deliveryman_id) {

    if( $deliveryman_id > 0 && is_deliveryman($deliveryman_id) ){
        
        $db = getDB();
        $sql = "SELECT `dnd_mode` FROM ".table_prifix."_user WHERE `id`=:deliveryman_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("deliveryman_id", $deliveryman_id, PDO::PARAM_INT);
        $stmt->execute();

        /*$sql1 = "SELECT time_avalibility_from, time_avalibility_from FROM ".table_prifix."_usermeta WHERE `user_id`=:deliveryman_id";
        $stmt1 = $db->prepare($sql1);
        $stmt->bindParam("deliveryman_id", $deliveryman_id, PDO::PARAM_INT);
        $stmt->execute();*/

        $user_data = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if( !empty($user_data['dnd_mode']) && isset($user_data['dnd_mode']) ){
            return false;
        }
    }

    return true;
}


/**
 * Call api when deliverman pickup order
 */
function api_pickup_order_by_deliveryman($url = '', $orderData, $key){

    $url = 'https://junoon.yourfoodorder.online/wp-content/yfo-dialadelivery/yfo_dd_dispatch.php';
    
    $fields_string = '';
    $fields_string = json_encode($orderData);

    //open connection
    $ch = curl_init();

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Content-Length: ' . strlen($fields_string);
    $headers[] = 'Key: '.$key;

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($orderData));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);

    return $result;
}

/**
 * Check deliveryman is on order or not 
 */
function deliveryman_on_order($deliveryman_id){

    if( $deliveryman_id > 0 && is_deliveryman($deliveryman_id) ){
        
        $db = getDB();
        $sql = "SELECT * FROM `delevering_order` WHERE delivery_agent_id=:deliveryman_id AND (order_status='awaiting-pickup' OR order_status='picked-up')";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("deliveryman_id", $deliveryman_id, PDO::PARAM_INT);
        $stmt->execute();

        $order_data = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
        if( !empty($order_data) && isset($order_data) ){
            return true;
        }
    }

    return false;
}

/**
 * On-hold order status 
 */
function new_order_notification($user_id){

    if( $user_id > 0 && is_deliveryman_active($user_id) ){

        $db = getDB();
        $sql = "SELECT * FROM `delevering_order` WHERE delivery_agent_id=:deliveryman_id AND order_status='on-hold'";

        $stmt = $db->prepare($sql);
        $delivery_agent_id = 0;

        $stmt->bindParam("deliveryman_id", $delivery_agent_id, PDO::PARAM_INT);
        $stmt->execute();
        $order_data = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
        if( !empty($order_data) && isset($order_data) ){
            return true;
        }
    }

    return false;
}


/**
 * Get TM data by status
 */
function getTransportationModesByStatus($status) {

    try {

        $db = getDB();

        $sql = "SELECT * FROM ".table_prifix."_transportation_modes_master where status=:status";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("status", $status, PDO::PARAM_INT);
        $stmt->execute();

        $final_arr = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
    
        /*$db = null;
        if( !empty($final_arr) && sizeof($final_arr) > 0 ) {
            return $final_arr;
        }*/

        return $final_arr;

    } catch(PDOException $e) {
        return false;
    }
}


/**
 * Get All TModes by status
 */
function getAllTransportationModes() {

    try {

        $db = getDB();

        $sql = "SELECT * FROM ".table_prifix."_transportation_modes_master";

        $stmt = $db->prepare($sql);
        $stmt->execute();

        $final_arr = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
    
        return $final_arr;

    } catch(PDOException $e) {
        return false;
    }
}

function getPickupCount($userid)
{
    try {
        $db = getDB();

        $sql = "SELECT count(*) as totalpickpus FROM ".table_prifix."_order where delivery_agent_id = :userid and (order_status='awaiting-pickup' or order_status='picked-up')";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam("userid", $userid, PDO::PARAM_INT);
        $stmt->execute();

        $final_arr = (array)$stmt->fetch(PDO::FETCH_OBJ);
    
        return $final_arr['totalpickpus'];

    } catch(PDOException $e) {
        return false;
    }
}

function getFeedback($userid)
{
	try{
		$db = getDB();
		$sql = "SELECT * FROM ".table_prifix."_tbl_reviews where from_user_id = :userID";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("userID",$userid, PDO::PARAM_INT);
		$stmt->execute();
		$final_arr = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
		$feed_details = [];
		foreach ($final_arr as $feeddt) {
            $sub= [];
            $sub['id'] = $feeddt->id;
            $sub['from_user_id'] = $feeddt->from_user_id;
            $sub['to_user_id'] = $feeddt->to_user_id;
			$sub['order_id'] = $feeddt->order_id;
			$sub['description'] = $feeddt->description;
			$sub['rating'] = $feeddt->rating;
            $feed_details[] = $sub;
        }
		return $feed_details;
	}catch(PDOException $e){
		return false;
	}
}

function getDeliverySuburbs($userid)
{
    try {
        $db = getDB();

        $sql = "SELECT * FROM ".table_prifix."_usermeta where user_id = :userid";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam("userid", $userid, PDO::PARAM_INT);
        $stmt->execute();

        $final_arr = (array)$stmt->fetch(PDO::FETCH_OBJ);
        $subArr = json_decode($final_arr['suburbs']);
        $subIds = [];
		//print_r($subArr);exit;
        if($subArr)
        {
            foreach ($subArr as $sub) {
                $subIds[] = $sub->id;
            }

        }
		//print_r($subIds);exit;

        $sql = "SELECT * FROM ".table_prifix."_au_towns where id in (".implode($subIds,",").")";
        $stmt1 = $db->prepare($sql);
        // $stmt1->bindParam("subid", implode($subIds,","), PDO::PARAM_STR);
        $stmt1->execute();

        $final_arr_sub = (array)$stmt1->fetchAll(PDO::FETCH_OBJ);
        $delLocations = [];
        foreach ($final_arr_sub as $subloc) {
            $sub= [];
            $sub['latitude'] = $subloc->latitude;
            $sub['longitude'] = $subloc->longitude;
            $sub['elevation'] = $subloc->elevation;
            $delLocations[] = $sub;
        }
        // echo "<pre>"; print_r($delLocations);exit;
    
        return $delLocations;

    } catch(PDOException $e) {
        return false;
    }
}

function nccCreateCheck ($data){
    $api_key = NCC_API_KEY;
    $api_secret = NCC_API_SECRET;
    try
    {
        $inst = new NationalCrimeCheck(NationalCrimeCheck::HOST_SANDBOX, $api_key, $api_secret);
        $result = $inst->createCheck($data);
    }
    catch(NationalCrimeCheckException $e)
    {
        // echo "<pre>";
        // print_r($e->getMessage());exit;
        // throw new NationalCrimeCheckException($e->getMessage(), 1);
        return $e->getMessage();
    }
        return $result;
}