<?php

/*== Requaire slim library and custom functions file ==*/
require_once('../Slim/Slim.php');
include_once('lib/functions.php');

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
// Get request headers as associative array
$headers = $app->request->headers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

/**
 * Testing Api
 */
$app->get('/checkCurlApi/:fdn', function($fdn) {
   
    $vendorData = getUserCommissionAndChargeByApiKey('64c85efd-5115-48cc-83ef-4e7d51cc5eae');
    $distance_arr = get_distance_and_duration('1183 High Street Armadale VIC 3143', '3, 2nd Ave, Box Hill South, VIC, AU, 3150');

    if( !empty($distance_arr) && sizeof($distance_arr) > 0 ){
        $distance = $distance_arr['distance'];
        if( !empty($vendorData) && sizeof($vendorData) > 0 ){
            $vendor_commision = $vendorData['commission'];
            $per_km_charge = $vendorData['vendor_per_km_charge'];
            
            if(/*$vendor_commision > 0 &&*/ $per_km_charge > 0 ){
                $order_quote = get_order_quote($vendor_commision, $per_km_charge, $distance);
                if( $order_quote > 0 ){
                    $response['quotePrice'] = $order_quote;
                    $response['estimateDistance'] = $distance_arr['distanceText'];
                    $response['estimateDuration'] = $distance_arr['duration'];
                    /*$response['pickupLocation'] = $request_data->pickupLocation;
                    $response['dropLocation'] = $request_data->dropLocation;*/
                    $response['estimateDuration'] = $distance_arr['duration'];
                    $response['status'] = 'success';
                    $response['message'] = 'Order quote get successfully';
                    echo json_encode($response);
                    die();    
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'error in geting order quote';
                    echo json_encode($response);
                    die();    
                }
            }else {
                $response['status'] = 'error';
                $response['message'] = 'error in geting commission and KM/Charge';
                echo json_encode($response);
                die(); 
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in geting vendor';
            echo json_encode($response); 
            die();
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'error in validating addressess';
        echo json_encode($response);
        die();
    }
});

/**
 * User Sign Up
 */
$app->post('/upload', function() {
    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $tempfilename = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
    $tempfileExt = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

    $newfilename = $tempfilename."_".time()."_pro_pic.".$tempfileExt;

    $uploadPath = dirname( __FILE__ )."/../.." . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $newfilename;
    move_uploaded_file( $tempPath, $uploadPath );
    // $answer = array( 'res' => 'File transfer completed' );
    $response['status'] = 'success';
    $response['message'] = 'Image uploaded successfully';
    $response['filename'] = $newfilename;
    echo json_encode( $response );
});

/**
 * User Sign Up
 */
$app->post('/signup', function() {
    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    // echo "<pre>"; print_r($user_data);exit;
    $data = $user_data->userData;
    // date_default_timezone_set("Asia/Kolkata");
    // $data = $_POST['userData'];

    $fname = $data->fname;
    $lname = $data->lname;
    $email = $data->email;
    $phone_no = $data->phone;
    $dialingNumber = isset($data->dialingNumber) ? $data->dialingNumber : null;
    $gender = "M";//isset($data->gender) ? $data->gender : "M";
    $password = $data->password;
    $rePassword = $data->rePassword;
    $requestFor = $data->requestFor;

    $dateofbirth = !empty($data->dateofbirth) ? $data->dateofbirth : '';
    $dobforapi = date("Y-m-d",strtotime($dateofbirth));
    // echo "<pre>"; print_r(date("Y-m-d",strtotime($dateofbirth)));exit;
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $suburbs = !empty($data->suburbs) ? json_encode($data->suburbs) : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';

    $company = '';//!empty($data->company) ? $data->company : '';
    $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';
    $availability_slots = isset($data->availability_slots) ? json_encode($data->availability_slots) : '';
    $min_release_amt = isset($data->min_release_amt) ? $data->min_release_amt : '';
    $vendor_per_km_charge = isset($data->vendor_per_km_charge) ? $data->vendor_per_km_charge : '';
    $deliveryman_per_km_charge = isset($data->deliveryman_per_km_charge) ? $data->deliveryman_per_km_charge : '';
    $commission = isset($data->commission) ? $data->commission : '';
    $time_avalibility_from = isset($data->time_avalibility_from) ? $data->time_avalibility_from : '';
    $time_avalibility_to = isset($data->time_avalibility_to) ? $data->time_avalibility_to : '';

    $display_name = $data->fname.' '.$data->lname;
    $newfilename = isset($data->filename)? $data->filename: null;

    try {

        $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
        $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);

        if ( strlen( trim($password) ) > 0 && strlen( trim($email) ) > 0 && $email_check > 0 && $password_check > 0 && $password == $rePassword ) {

            $db = getDB();
            $userData = '';
            $sql = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email,PDO::PARAM_STR);
            $stmt->execute();
            $mainCount=$stmt->rowCount();

            $created=time();
            $password = md5($password);
            $status = 0;
            $phone = $dialingNumber.$phone_no;

            if($mainCount==0) {
                /*Inserting user values*/
                $date = new DateTime();
                $timedata = $date->getTimestamp();
                
                $sql1 = "INSERT INTO ".table_prifix."_user (first_name,last_name,email,contact_no,password,display_name,user_role,status,regisration_date,image_url) VALUES(:fname,:lname,:email,:contact_no,:password,:display_name,:requestFor,:status,:current_timestamp,:image_url)";

                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("status", $status, PDO::PARAM_STR);
                $stmt1->bindParam("fname", $fname, PDO::PARAM_STR);
                $stmt1->bindParam("lname", $lname, PDO::PARAM_STR);
                $stmt1->bindParam("email", $email, PDO::PARAM_STR);
                $stmt1->bindParam("password", $password, PDO::PARAM_STR);
                $stmt1->bindParam("display_name", $display_name, PDO::PARAM_STR);
                $stmt1->bindParam("requestFor", $requestFor, PDO::PARAM_STR);
                $stmt1->bindParam("contact_no", $phone, PDO::PARAM_INT);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
                $stmt1->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                if($requestFor != "deliveryman")
                    $stmt1->execute();


                if($requestFor == "deliveryman")
                {

                    $data = [
                        "client_ref" => "abcde",
                        "cost_centre" => "Marketing",
                        "first_name" => $fname,
                        "middle_name" => "",
                        "last_name" => $lname,
                        "single_name" => false,
                        "dob" =>  $dobforapi,
                        "birth_place" => $city,
                        "birth_state" => $state,
                        "birth_country" => 'AUS',
                        "sex" => $gender,
                        "email" => $email,
                        "resid" => [
                            "street" => $address_1.' '.$address_2,
                            "suburb" => $suburbs,
                            "state" => $state,
                            "postcode" => $zip,
                            "years" => 3,
                            "months" => 2
                        ],
                        "type" => "EMPLOYMENT",
                        "services" => ["Vevo", "Bankruptcy"],
                        "reason" => "Driver registration",
                        "result_webhook" => "http://example.com/result"
                    ];

                    /*if(in_array($state,["ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA"]))
                    {
                        $data['birth_state'] = $state;
                        $data['resid']['state'] = $state;
                    }*/
                        $dataresult = nccCreateCheck($data);
                        // echo "<pre>";
                        // print_r($dataresult);exit;
                    if(is_array($dataresult))
                    {
                        if(isset($dataresult['id']))
                        {
                            $stmt1->execute();
                            $user_id = $db->lastInsertId(); 

                            $sql2 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,suburbs,city,state,zip,country,commission,deliveryman_per_km_charge,vendor_per_km_charge,min_release_amt,delivery_mode,transport_mode,inhouse_vendor,state_avalibility, dateofbirth,time_avalibility_from,time_avalibility_to,image_url, availability_slots, ncc_id, continue_url) VALUES (:user_id,:company,:address_one,:address_two,:suburbs,:city,:state,:zip,:country,:commission,:deliveryman_per_km_charge,:vendor_per_km_charge,:min_release_amt,:delivery_mode,:transport_mode,:inhouse_vendor,:state_avalibility,:dateofbirth,:time_avalibility_from,:time_avalibility_to,:image_url,:availability_slots,:nccid, :continue_url)";

                        
                            $stmt2 = $db->prepare($sql2);

                            $stmt2->bindParam("user_id", $user_id, PDO::PARAM_INT);
                            $stmt2->bindParam("company", $company, PDO::PARAM_STR);
                            $stmt2->bindParam("dateofbirth", $dateofbirth, PDO::PARAM_STR);
                            $stmt2->bindParam("address_one", $address_1, PDO::PARAM_STR);
                            $stmt2->bindParam("address_two", $address_2, PDO::PARAM_STR);
                            $stmt2->bindParam("suburbs", $suburbs, PDO::PARAM_STR);
                            $stmt2->bindParam("city", $city, PDO::PARAM_STR);
                            $stmt2->bindParam("state", $state, PDO::PARAM_STR);
                            $stmt2->bindParam("zip", $zip, PDO::PARAM_INT);
                            $stmt2->bindParam("country", $country, PDO::PARAM_STR);
                            $stmt2->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
                            $stmt2->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);
                            $stmt2->bindParam("availability_slots", $availability_slots, PDO::PARAM_STR);
                            $stmt2->bindParam("commission", $commission, PDO::PARAM_STR);
                            $stmt2->bindParam("deliveryman_per_km_charge", $deliveryman_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("vendor_per_km_charge", $vendor_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("min_release_amt", $min_release_amt, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_from", $time_avalibility_from, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_to", $time_avalibility_to, PDO::PARAM_STR);
                            $stmt2->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                            $stmt2->bindParam("nccid", $dataresult['id'], PDO::PARAM_STR);
                            $stmt2->bindParam("continue_url", $dataresult['continue_url'], PDO::PARAM_STR);
                            $stmt1_result = $stmt2->execute();
                            

                            // $apires = json_decode($dataresult);
                            // $sqlupdate = "UPDATE ".table_prifix."_usermeta SET ncc_id=:nccid, continue_url=:continue_url WHERE user_id=:user_id";
                            // $stmt1update = $db->prepare($sqlupdate);
                            // $stmt1update->bindParam("user_id", $user_id, PDO::PARAM_STR);
                            // $res = $stmt1update->execute();
                        }
                        
                        $userData = internalUserDetails($email);
                        $db = null;

                        if($userData){
                            $response['status'] = 'success';
                            $response['message'] = 'User successfully Registered';
                            $response['userData'] = $userData;
                            $response['verificationData'] = $dataresult;

                            $subject = 'Thanks for your registration';
                            $custom_message = 'Hi '.$fname.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$email.', We will inform you once your account is active.';
                            //$result = delivery_mail_template($email, $subject, '', $custom_message);
                            echo json_encode($response);
                        } else {
                            $response['status'] = 'error';
                            $response['message'] = 'Error in getting userdata';
                            echo json_encode($response);
                        }
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['message'] = 'Verification Error';
                        $response['tech'] = $dataresult;
                        echo json_encode($response);
                        
                    }
                    
                    
                    /*if($phone > 0 ){
                        $message = "hi you OTP is 1234";
                        send_message($phone, $message);
                    }*/
                    

                }
                else
                {
                    $response['status'] = 'success';
                    $response['message'] = 'User successfully Registered';
                    $response['userData'] = $userData;
                    echo json_encode($response);
                }
            }else {
                $response['status'] = 'error';
                $response['message'] = 'User already registered, Please try again with diffrent email id';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

$app->get('/smstest', function() {
    $phone = "+918390115965";
    $message = "Welcome to twilio msg api";
    echo "<pre>";print_r(send_message($phone, $message));
});

$app->get('/ncccreatecheck', function() {

    $data = [
    "client_ref" => "abcde",
    "cost_centre" => "Marketing",
    "first_name" => "John",
    "middle_name" => "Nathan",
    "last_name" => "Smith",
    "single_name" => false,
    "dob" => "1976-05-04",
    "birth_place" => "Adelaide",
    "birth_state" => "SA",
    "birth_country" => "AUS",
    "sex" => "M",
    "email" => "john.smith@example.com",
    "resid" => [
        "street" => "1 Something street",
        "suburb" => "Adelaide",
        "state" => "SA",
        "postcode" => "5000",
        "years" => 3,
        "months" => 2
    ],
    "previous" => [
        [
            "street" => "1 Another street",
            "suburb" => "Adelaide",
            "state" => "SA",
            "postcode" => "5000",
            "country" => "AUS",
            "years" => 1,
            "months" => 1
        ],
        [
            "street" => "1 Old avenue",
            "suburb" => "Adelaide",
            "state" => "SA",
            "postcode" => "5000",
            "country" => "AUS",
            "years" => 3,
            "months" => 1
        ]
    ],
    "type" => "EMPLOYMENT",
    "services" => ["Vevo", "Bankruptcy"],
    "reason" => "Computer programmer",
    "result_webhook" => "http://example.com/result"
];

$data = [
    "client_ref" => "abcde",
    "cost_centre" => "Marketing",
    "first_name" => "John",
    "middle_name" => "",
    "last_name" => "Smith",
    "single_name" => false,
    "dob" => "1976-05-04",
    "birth_place" => "",
    "birth_state" => "",
    "birth_country" => "AUS",
    "sex" => "M",
    "email" => "john.smith@example.com",
    "resid" => [
        "street" => "1 Something street",
        "suburb" => "Adelaide",
        "state" => "SA",
        "postcode" => "5000",
        "years" => 3,
        "months" => 2
    ],
    "type" => "EMPLOYMENT",
    "services" => ["Vevo", "Bankruptcy"],
    "reason" => "Driver registration",
    "result_webhook" => "http://example.com/result"
];
    $dataresult = nccCreateCheck($data);
    echo "<pre>";
    print_r($dataresult);
});


/**
* User Login
*/
$app->post('/login', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;

    try {

        $db = getDB();
        $userDataArr ='';
        $sql = "SELECT * FROM ".table_prifix."_user WHERE email=:email and password=:password";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $data->email, PDO::PARAM_STR);
        $password=md5($data->password);
        $stmt->bindParam("password", $password, PDO::PARAM_STR);
        $stmt->execute();
        $userData = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        if(!empty($userData)) {
            $user_id = $userData->id;
            if( is_user_active($user_id) ){
                $userData->token = apiToken($user_id);
                $_SESSION["userID"] = apiToken($user_id);
                $response['status'] = 'success';
                $response['message'] = 'User login successfully';
                if(is_vendor($user_id))
                {
                    $response['userData'] = $userData;
                }
                else{
                    $response['userData'] = getAllUserData($user_id);
                }
                echo json_encode($response);
            }else {
                $response['status'] = 'error';
                $response['message'] = 'Unable to login, Please contact to admin';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'login credential are incorrect, Please try again.';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
* Forget Password
*/
$app->post('/forgotPass', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userEmail;

    try {

        $db = getDB();
        $userDataArr ='';
        $sql = "SELECT * FROM ".table_prifix."_user WHERE email=:email";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $data->email, PDO::PARAM_STR);
        $stmt->execute();
        $userData = $stmt->fetch(PDO::FETCH_OBJ);

        $db = null;

        if(!empty($userData)) {

            $user_id = $userData->id;
            $user_email = $userData->email;
            $user_password = randomString();

            $db = getDB();
            $password = md5($user_password);

            $sql1 = "UPDATE ".table_prifix."_user SET password=:password WHERE id=:user_id";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("password", $password, PDO::PARAM_STR);
            $stmt1->bindParam("user_id", $userData->id, PDO::PARAM_STR);
            $stmt1->execute();

            $from = '';
            $subject = 'Password reset request - Delevering App';
            $custom_message = 'You recently requested to reset your password. We have auto-genrated new password. Please use: <b>'.$user_password.'</b> as your new password';
			$result = true;
            //$result = delivery_mail_template($user_email, $subject, $from, $custom_message);

            if($result) {
                $response['status'] = 'success';
                $response['message'] = 'Password Sent on your registered email';
                echo json_encode($response);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Error in sending password, Please try again';
                echo json_encode($response);
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'User not Found!';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
* Reset Password
*/
$app->post('/reset-password/', function() {
    $db = getDB();
    $response = array();

    $u_id = $_REQUEST['id'];
    $password = $_REQUEST['password'];
    $password = md5($password);
    $sql = "UPDATE ".table_prifix."_user SET password = $password WHERE email=$u_id";

    if($db->prepare($sql)){
       $response['status'] = 'success';
       $response['message'] = 'Password update successfully';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Unable to process request';
    }

    echo json_encode($response);
});

/**
* User Log Out
*/
$app->get('/logOut/:user_id', function($user_id) {
    $response = array();

    if( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ){
       $_SESSION["userID"] = null;
       unset($_SESSION["userID"]);
       session_destroy();
       $response['status'] = 'success';
       $response['message'] = 'User seccessfully logged Out!';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Unable to process request';
    }

    echo json_encode($response);
});

/**
* User Login
*/
$app->get('/checkLogin/:user_id', function($user_id) {

    $response = array();

    if( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ){
        $response['status'] = 'success';
        $response['message'] = 'User logged In!';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'User Not logged In!';
    }

    echo json_encode($response);
});

/**
* Check user logged in and is Admin or not
*/
$app->get('/isAdmin/:user_id', function($user_id) {

    $response = array();

    if( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_admin($user_id) ){
        $response['status'] = 'success';
        $response['message'] = 'User is admin!';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'User Not admin!';
    }

    echo json_encode($response);
});

/**
* Check user logged in and is Deliveryman or not
*/
$app->get('/isDeliveryMan/:user_id', function($user_id) {

    $response = array();

    if( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_deliveryman($user_id) ){
        $response['status'] = 'success';
        $response['message'] = 'User is deliveryman!';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'User Not Deliveryman';
    }

    echo json_encode($response);
});

/**
* Check user logged in and is vendor or not
*/
$app->get('/isVendor/:user_id', function($user_id) {

    $response = array();

    if( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_vendor($user_id) ){
        $response['status'] = 'success';
        $response['message'] = 'User is vendor';
    }else {
        $response['status'] = 'error';
        $response['message'] = 'User not vendor';
    }

    echo json_encode($response);
});

/**
 * Get User Meta
 */
$app->get('/getUserData/:user_id', function($user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $userData = getAllUserData($user_id);
            if(!empty($userData) ){
                $response['status'] = 'success';
                $response['message'] = 'User meta get successfully';
                $response['userData'] = $userData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No meta found along with this user';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get User Meta
 */
$app->get('/getVendorUserData/:user_id', function($user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $userData = getVendorUserDataByDeliveryId($user_id);
            if(!empty($userData) ){
                $response['status'] = 'success';
                $response['message'] = 'User meta get successfully';
                $response['userData'] = $userData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No meta found along with this user';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get User By Admin
 */
$app->get('/getUserDataByAdmin/:admin_id/:user_id', function($admin_id,$user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($admin_id) ) {

            $userData = getAllUserData($user_id);
            
            if(!empty($userData) ){
                $response['status'] = 'success';
                $response['message'] = 'User meta get successfully';
                $response['userData'] = $userData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No meta found along with this user';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get User By Admin
 */
$app->get('/getVendorUserDataByAdmin/:admin_id/:user_id', function($admin_id,$user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($admin_id) ) {

            $userData = getVendorUserDataByDeliveryId($user_id);

            if(!empty($userData) ){
                $response['status'] = 'success';
                $response['message'] = 'User meta get successfully';
                $response['userData'] = $userData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No meta found along with this user';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Update User data
 */
$app->post('/updateUser/', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;


    $user_id = $user_data->userID;

    $first_name = !empty($data->first_name) ? $data->first_name : '';
    $last_name = !empty($data->last_name) ? $data->last_name : '';
    $display_name = !empty($data->display_name) ? $data->display_name : '';
    $company = !empty($data->company) ? $data->company : '';
    $contact_no = !empty($data->contact_no) ? $data->contact_no : '';
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';
    $email = !empty($data->email) ? $data->email : '';
    $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $time_avalibility_from = isset($data->time_avalibility_from) ? $data->time_avalibility_from : '';
    $time_avalibility_to = isset($data->time_avalibility_to) ? $data->time_avalibility_to : '';
	$deliveryman_per_km_charge = isset($data->deliveryman_per_km_charge) ? $data->deliveryman_per_km_charge : 0;
    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';

    try{

        if ( strlen($data->id) > 0 && $_SESSION["userID"] == apiToken($user_id) && $email !== '' ) {

            $db = getDB();

            $sq = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $st = $db->prepare($sq);
            $st->bindParam("email", $email, PDO::PARAM_STR);
            $st->execute();
            $mainCount = $st->rowCount();
            $user_profile_data = $st->fetch(PDO::FETCH_OBJ);
            if($data->id !== $user_profile_data->id && $mainCount > 0){
                $response['status'] = 'error';
                $response['message'] = 'Email Already exist!!';
                echo json_encode($response);
                die();
            }

            $sqlSelect = "SELECT * FROM ".table_prifix."_usermeta WHERE user_id=:user_id";
            $stmtSelect  = $db->prepare($sqlSelect);
            $stmtSelect ->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmtSelect ->execute();
            $userMetaData = (array)$stmtSelect->fetch(PDO::FETCH_OBJ);

            $sql = "UPDATE ".table_prifix."_user SET first_name=:first_name,last_name=:last_name,contact_no=:contact_no,display_name=:display_name WHERE id=:user_id";
            $user_meta = current($userMetaData);

            if( !empty($user_meta) ) {
                $sql1 = "UPDATE ".table_prifix."_usermeta SET company=:company,address_1=:address_one,address_2=:address_two,city=:city,state=:state,zip=:zip,country=:country,delivery_mode=:delivery_mode,deliveryman_per_km_charge=:deliveryman_per_km_charge,transport_mode=:transport_mode,inhouse_vendor=:inhouse_vendor,time_avalibility_from=:time_avalibility_from,time_avalibility_to=:time_avalibility_to,state_avalibility=:state_avalibility WHERE user_id=:user_id";
            }else{
               $sql1 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,city,state,zip,country,delivery_mode,deliveryman_per_km_charge,transport_mode,inhouse_vendor,time_avalibility_from,time_avalibility_to,state_avalibility) VALUES (:user_id,:company,:address_one,:address_two,:city,:state,:zip,:country,:delivery_mode,:transport_mode,:deliveryman_per_km_charge,:inhouse_vendor,:time_avalibility_from,:time_avalibility_to,:state_avalibility)";
            }

            $stmt = $db->prepare($sql);
            $stmt1 = $db->prepare($sql1);

            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->bindParam("first_name", $first_name, PDO::PARAM_STR);
            $stmt->bindParam("last_name", $last_name, PDO::PARAM_STR);
            $stmt->bindParam("display_name", $display_name, PDO::PARAM_STR);
            $stmt->bindParam("contact_no", $contact_no, PDO::PARAM_INT);

            $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt1->bindParam("company", $company, PDO::PARAM_STR);
            $stmt1->bindParam("address_one", $address_1, PDO::PARAM_STR);
            $stmt1->bindParam("address_two", $address_2, PDO::PARAM_STR);
            $stmt1->bindParam("city", $city, PDO::PARAM_STR);
            $stmt1->bindParam("state", $state, PDO::PARAM_STR);
            $stmt1->bindParam("zip", $zip, PDO::PARAM_INT);
            $stmt1->bindParam("country", $country, PDO::PARAM_STR);
            $stmt1->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
            $stmt1->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
            $stmt1->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
			$stmt1->bindParam("deliveryman_per_km_charge",$deliveryman_per_km_charge,PDO::PARAM_STR);
            $stmt1->bindParam("time_avalibility_from", $time_avalibility_from, PDO::PARAM_STR);
            $stmt1->bindParam("time_avalibility_to", $time_avalibility_to, PDO::PARAM_STR);
            $stmt1->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);

            $stmt_result = $stmt->execute();
            $stmt1_result = $stmt1->execute();

            if( $stmt_result && $stmt1_result ){
                $response['status'] = 'success';
                $response['message'] = 'User Data Update successfully';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating user data';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Update User availability data -- vishal
 */
$app->post('/updateUserAvailability/', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;


    $user_id = $user_data->userID;

    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    // $country = !empty($data->country) ? $data->country : '';
    // $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';
    $availability_slots = isset($data->availability_slots) ? json_encode($data->availability_slots) : '';
    $suburbs = isset($data->suburbs) ? json_encode($data->suburbs) : '';

    try{

        if ( strlen($data->id) > 0 && $_SESSION["userID"] == apiToken($user_id)) {

            $db = getDB();

            /*$sq = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $st = $db->prepare($sq);
            $st->bindParam("email", $email, PDO::PARAM_STR);
            $st->execute();
            $mainCount = $st->rowCount();
            $user_profile_data = $st->fetch(PDO::FETCH_OBJ);
            if($data->id !== $user_profile_data->id && $mainCount > 0){
                $response['status'] = 'error';
                $response['message'] = 'Email Already exist!!';
                echo json_encode($response);
                die();
            }*/

            /*$sqlSelect = "SELECT * FROM ".table_prifix."_usermeta WHERE user_id=:user_id";
            $stmtSelect  = $db->prepare($sqlSelect);
            $stmtSelect ->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmtSelect ->execute();
            $userMetaData = (array)$stmtSelect->fetch(PDO::FETCH_OBJ);

            $sql = "UPDATE ".table_prifix."_user SET first_name=:first_name,last_name=:last_name,contact_no=:contact_no,display_name=:display_name WHERE id=:user_id";
            $user_meta = current($userMetaData);*/

            $sql = "UPDATE ".table_prifix."_usermeta SET city=:city,state=:state,availability_slots=:availability_slots,suburbs=:suburbs WHERE user_id=:user_id";
            
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->bindParam("city", $city, PDO::PARAM_STR);
            $stmt->bindParam("state", $state, PDO::PARAM_STR);
            $stmt->bindParam("availability_slots", $availability_slots, PDO::PARAM_STR);
            $stmt->bindParam("suburbs", $suburbs, PDO::PARAM_STR);

            $stmt_result = $stmt->execute();

            if( $stmt_result){
                $response['status'] = 'success';
                $response['message'] = 'User Data Update successfully';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating user data';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Add User data By Admin
 */

$app->post('/addUserByAdmin/', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());

    $data = $user_data->userData;
    $admin_id = $user_data->adminID;

    $fname = !empty($data->first_name) ? $data->first_name : '';
    $lname = !empty($data->last_name) ? $data->last_name : '';
    $email = !empty($data->email) ? $data->email : '';
    $display_name = $fname.' '.$lname;
    $company = !empty($data->company) ? $data->company : '';
    $contact_no = !empty($data->contact_no) ? $data->contact_no : '';
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';
    $password = $data->password;
    $rePassword = $data->rePassword;
    $requestFor = $data->user_role;

    $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';
    $min_release_amt = isset($data->min_release_amt) ? $data->min_release_amt : '';
    $vendor_per_km_charge = isset($data->vendor_per_km_charge) ? $data->vendor_per_km_charge : '';
    $deliveryman_per_km_charge = isset($data->deliveryman_per_km_charge) ? $data->deliveryman_per_km_charge : '';
    $commission = isset($data->commission) ? $data->commission : '';




    try {

        $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
        $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);

        if ( strlen( trim($password) ) > 0 && strlen( trim($email) ) > 0 && $email_check > 0 && $password_check > 0 && $password == $rePassword ) {

            $db = getDB();
            $userData = '';
            $sql = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email,PDO::PARAM_STR);
            $stmt->execute();
            $mainCount=$stmt->rowCount();
            $created=time()."<br>";
            $password = md5($password);
            $status = 1;
            $phone = $contact_no;
            
            if($mainCount==0) {
                /*Inserting user values*/
                $date = new DateTime();
                $timedata = $date->getTimestamp();
                
                $sql1 = "INSERT INTO ".table_prifix."_user (first_name,last_name,email,contact_no,password,display_name,user_role,status,regisration_date) VALUES(:fname,:lname,:email,:contact_no,:password,:display_name,:requestFor,:status,:current_timestamp)";

                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("status", $status, PDO::PARAM_STR);
                $stmt1->bindParam("fname", $fname, PDO::PARAM_STR);
                $stmt1->bindParam("lname", $lname, PDO::PARAM_STR);
                $stmt1->bindParam("email", $email, PDO::PARAM_STR);
                $stmt1->bindParam("password", $password, PDO::PARAM_STR);
                $stmt1->bindParam("display_name", $display_name, PDO::PARAM_STR);
                $stmt1->bindParam("requestFor", $requestFor, PDO::PARAM_STR);
                $stmt1->bindParam("contact_no", $phone, PDO::PARAM_INT);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);

                $stmt1->execute();
                $userData = internalUserDetails($email);

                $user_id = $db->lastInsertId(); 

                $sql2 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,city,state,zip,country,commission,deliveryman_per_km_charge,vendor_per_km_charge,min_release_amt,delivery_mode,transport_mode,inhouse_vendor,state_avalibility) VALUES (:user_id,:company,:address_one,:address_two,:city,:state,:zip,:country,:commission,:deliveryman_per_km_charge,:vendor_per_km_charge,:min_release_amt,:delivery_mode,:transport_mode,:inhouse_vendor,:state_avalibility)";

            
                $stmt2 = $db->prepare($sql2);

                $stmt2->bindParam("user_id", $user_id, PDO::PARAM_INT);
                $stmt2->bindParam("company", $company, PDO::PARAM_STR);
                $stmt2->bindParam("address_one", $address_1, PDO::PARAM_STR);
                $stmt2->bindParam("address_two", $address_2, PDO::PARAM_STR);
                $stmt2->bindParam("city", $city, PDO::PARAM_STR);
                $stmt2->bindParam("state", $state, PDO::PARAM_STR);
                $stmt2->bindParam("zip", $zip, PDO::PARAM_INT);
                $stmt2->bindParam("country", $country, PDO::PARAM_STR);
                $stmt2->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
                $stmt2->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
                $stmt2->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
                $stmt2->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);
                $stmt2->bindParam("commission", $commission, PDO::PARAM_STR);
                $stmt2->bindParam("deliveryman_per_km_charge", $deliveryman_per_km_charge, PDO::PARAM_STR);
                $stmt2->bindParam("vendor_per_km_charge", $vendor_per_km_charge, PDO::PARAM_STR);
                $stmt2->bindParam("min_release_amt", $min_release_amt, PDO::PARAM_STR);

                $stmt1_result = $stmt2->execute();

                if($userData){
                    $response['status'] = 'success';
                    $response['message'] = 'User successfully Registered';
                    $response['userData'] = $userData;

                    $subject = 'Thanks for your registration';
                    $custom_message = 'Hi '.$fname.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$email.', We will inform you once your account is active.';
                    //$result = delivery_mail_template($email, $subject, '', $custom_message);
                    echo json_encode($response);
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'Error in getting userdata';
                    echo json_encode($response);
                }

            }else {
                $response['status'] = 'error';
                $response['message'] = 'User already registered, Please try again with diffrent email id';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Update User data By Admin
 */
$app->post('/updateUserByAdmin/', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $data = $user_data->userData;
    $user_id = $user_data->userID;
    $admin_id = $user_data->adminID;

    $first_name = !empty($data->first_name) ? $data->first_name : '';
    $last_name = !empty($data->last_name) ? $data->last_name : '';
    $display_name = !empty($data->display_name) ? $data->display_name : '';
    $company = !empty($data->company) ? $data->company : '';
    $contact_no = !empty($data->contact_no) ? $data->contact_no : '';
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';
    $email = !empty($data->email) ? $data->email : '';

    $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $time_avalibility_from = isset($data->time_avalibility_from) ? $data->time_avalibility_from : '';
    $time_avalibility_to = isset($data->time_avalibility_to) ? $data->time_avalibility_to : '';

    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';

    try{

        if ( strlen($data->id) > 0 && $_SESSION["userID"] == apiToken($admin_id) && $email !== '' ) {

            $db = getDB();

            $sq = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $st = $db->prepare($sq);
            $st->bindParam("email", $email, PDO::PARAM_STR);
            $st->execute();
            $mainCount = $st->rowCount();
            $user_profile_data = $st->fetch(PDO::FETCH_OBJ);
            if($data->id !== $user_profile_data->id && $mainCount > 0){
                $response['status'] = 'error';
                $response['message'] = 'Email Already exist!!';
                echo json_encode($response);
                die();
            }

            $sqlSelect = "SELECT * FROM ".table_prifix."_usermeta WHERE user_id=:user_id";
            $stmtSelect  = $db->prepare($sqlSelect);
            $stmtSelect ->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmtSelect ->execute();
            $userMetaData = (array)$stmtSelect->fetch(PDO::FETCH_OBJ);

            $sql = "UPDATE ".table_prifix."_user SET first_name=:first_name,last_name=:last_name,contact_no=:contact_no,display_name=:display_name WHERE id=:user_id";
            $user_meta = current($userMetaData);

            if( !empty($user_meta) ) {
                $sql1 = "UPDATE ".table_prifix."_usermeta SET company=:company,address_1=:address_one,address_2=:address_two,city=:city,state=:state,zip=:zip,country=:country,delivery_mode=:delivery_mode,transport_mode=:transport_mode,inhouse_vendor=:inhouse_vendor,time_avalibility_from=:time_avalibility_from,time_avalibility_to=:time_avalibility_to,state_avalibility=:state_avalibility WHERE user_id=:user_id";
            }else{
               $sql1 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,city,state,zip,country,delivery_mode,transport_mode,inhouse_vendor,time_avalibility_from,time_avalibility_to,state_avalibility) VALUES (:user_id,:company,:address_one,:address_two,:city,:state,:zip,:country,:delivery_mode,:transport_mode,:inhouse_vendor,:time_avalibility_from,:time_avalibility_to,:state_avalibility)";
            }

            $stmt = $db->prepare($sql);
            $stmt1 = $db->prepare($sql1);

            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->bindParam("first_name", $first_name, PDO::PARAM_STR);
            $stmt->bindParam("last_name", $last_name, PDO::PARAM_STR);
            $stmt->bindParam("display_name", $display_name, PDO::PARAM_STR);
            $stmt->bindParam("contact_no", $contact_no, PDO::PARAM_INT);

            $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt1->bindParam("company", $company, PDO::PARAM_STR);
            $stmt1->bindParam("address_one", $address_1, PDO::PARAM_STR);
            $stmt1->bindParam("address_two", $address_2, PDO::PARAM_STR);
            $stmt1->bindParam("city", $city, PDO::PARAM_STR);
            $stmt1->bindParam("state", $state, PDO::PARAM_STR);
            $stmt1->bindParam("zip", $zip, PDO::PARAM_INT);
            $stmt1->bindParam("country", $country, PDO::PARAM_STR);
            $stmt1->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
            $stmt1->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
            $stmt1->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
            $stmt1->bindParam("time_avalibility_from", $time_avalibility_from, PDO::PARAM_STR);
            $stmt1->bindParam("time_avalibility_to", $time_avalibility_to, PDO::PARAM_STR);
            $stmt1->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);

            $stmt_result = $stmt->execute();
            $stmt1_result = $stmt1->execute();

            if( $stmt_result && $stmt1_result ){
                $response['status'] = 'success';
                $response['message'] = 'User Data Update successfully';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating user data';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get User Stores
 */
$app->get('/getStores/:user_id', function($user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            if( getUserStoreData($user_id) ){
                $response['status'] = 'success';
                $response['message'] = 'Store get successfully';
                $response['storeData'] = getUserStoreData($user_id);
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No store found along with this user';
                echo json_encode($response);
            }

        }else {

            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Insert User Stores
 */
$app->post('/addStoreData', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $store_data = json_decode($request->getBody());

    $user_id = $store_data->userID;
    $store_data = $store_data->storeData;

    $store_id = isset($store_data->id) ? $store_data->id : null;
    $store_name = isset($store_data->store_name) ? $store_data->store_name : '';
    $store_address1 = isset($store_data->address_1) ? $store_data->address_1 : '';
    $store_address2 = isset($store_data->address_2) ? $store_data->address_2 : '';
    $store_city = isset($store_data->city) ? $store_data->city : '';
    $store_state = isset($store_data->state) ? $store_data->state : '';
    $store_zip = isset($store_data->zip) ? $store_data->zip : '';
    $store_country = isset($store_data->country) ? $store_data->country : '';
    $store_phone = isset($store_data->store_contact) ? $store_data->store_contact : '';
    $status = 1;

    try{

        if ( strlen($store_name) > 0 && ( strlen($store_address1) > 0 || strlen($store_address2) > 0 )  && $store_phone > 0 && $user_id > 0 && is_vendor($user_id) ) {

            $created = time();

            if(isset($store_id) && $store_id > 0 ){
                /*Updating store values*/
                $sql1 = 'UPDATE '.table_prifix.'_stores
                            SET `store_name` = :store_name,
                                `store_contact` = :store_phone,
                                `address_1` = :store_address1,
                                `address_2` = :store_address2,
                                `city` = :store_city,
                                `state` = :store_state,
                                `country` = :store_country,
                                `zip` = :store_zip,
                                `last_modified` = :created
                            WHERE `id` = :store_id';

                $db = getDB();
                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("store_id", $store_id, PDO::PARAM_INT);
                $stmt1->bindParam("store_name", $store_name, PDO::PARAM_STR);
                $stmt1->bindParam("store_phone", $store_phone, PDO::PARAM_INT);
                $stmt1->bindParam("store_address1", $store_address1, PDO::PARAM_STR);
                $stmt1->bindParam("store_address2", $store_address2, PDO::PARAM_STR);
                $stmt1->bindParam("store_city", $store_city, PDO::PARAM_STR);
                $stmt1->bindParam("store_state", $store_state, PDO::PARAM_STR);
                $stmt1->bindParam("store_country", $store_country, PDO::PARAM_STR);
                $stmt1->bindParam("store_zip", $store_zip, PDO::PARAM_INT);
                $stmt1->bindParam("created", $created, PDO::PARAM_STR);

                if( $stmt1->execute() ){
                    $response['status'] = 'success';
                    $response['message'] = 'Store Update successfully';
                    $response['storeData'] = getUserStoreData($user_id);
                    echo json_encode($response);
                } else {
                    $response['status'] = 'error';
                    $response['message'] = $stmt1->error;
                    $response['storeData'] = getUserStoreData($user_id);
                    echo json_encode($response);
                }

                $db = null;
            }else{

                $status = 1;

                /*Inserting store values*/
                $sql1 = "INSERT INTO ".table_prifix."_stores (user_id,store_name,store_contact,address_1,address_2,city,state,country,status) VALUES(:user_id,:store_name,:store_phone,:store_address1,:store_address2,:store_city,:store_state,:store_country,:status)";

                $db = getDB();
                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
                $stmt1->bindParam("store_name", $store_name, PDO::PARAM_STR);
                $stmt1->bindParam("store_phone", $store_phone, PDO::PARAM_INT);
                $stmt1->bindParam("store_address1", $store_address1, PDO::PARAM_STR);
                $stmt1->bindParam("store_address2", $store_address2, PDO::PARAM_STR);
                $stmt1->bindParam("store_city", $store_city, PDO::PARAM_STR);
                $stmt1->bindParam("store_state", $store_state, PDO::PARAM_STR);
                $stmt1->bindParam("store_country", $store_country, PDO::PARAM_STR);
                /*$stmt->bindParam("created", $created, PDO::PARAM_STR);*/
                $stmt1->bindParam("status", $status, PDO::PARAM_INT);

                // $stmt1->execute();

                if( $stmt1->execute() ){
                    $response['status'] = 'success';
                    $response['message'] = 'Store added successfully';
                    $response['storeData'] = getUserStoreData($user_id);
                    echo json_encode($response);
                } else {
                    $response['status'] = 'error';
                    $response['message'] = $stmt1->error;
                    $response['storeData'] = getUserStoreData($user_id);
                    echo json_encode($response);
                }
                $db = null;
            }
        }else {

            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Delete User Stores
 */
$app->delete('/deleteStore/:store_id', function ($store_id) {


    try{

        if ( strlen($store_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_vendor($user_id) ) {

            $sql = "DELETE FROM ".table_prifix."_stores
                        WHERE `id` = :store_id";

            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("store_id", $store_id, PDO::PARAM_INT);
            $stmt_result = $stmt->execute();

             if($stmt_result){
                $response['status'] = 'success';
                $response['message'] = 'Store Delete successfully';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = $stmt1->error;
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Create Order
 */
$app->post('/getNearestDeliveryMan', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    $user_details = $user_data->userData;
    $user_id = $user_data->userID;

    $response = array();

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $pickUplocation = isset($user_details->pickUplocation) ? $user_details->droplocation : '';
            $droplocation = isset($user_details->droplocation) ? $user_details->droplocation : '';
            $pickLatlng = !empty($user_details->pickLatlng) ? $user_details->pickLatlng : null;
            $dropLatlng = !empty($user_details->dropLatlng) ? $user_details->dropLatlng : null;
            $shortNote = isset($user_details->item->shortNote) ? $user_details->item->shortNote : '';
            $itemDetail = isset($user_details->itemDetail) ? $user_details->itemDetail : array();

            $db = getDB();

            /*$sql1 = "SELECT delevering_user_markers.address, delevering_user_markers.lat, delevering_user_markers.lng, delevering_user.*, ( 6371 * acos( cos( radians(:pick_lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:pick_long) ) + sin( radians(:pick_lat) ) * sin( radians( lat ) ) ) ) AS distance FROM delevering_user_markers LEFT JOIN delevering_user ON delevering_user_markers.user_id = delevering_user.id
             WHERE delevering_user_markers.user_id IN (SELECT id  FROM delevering_user WHERE work_status = 0 ) HAVING distance < 15 ORDER BY distance LIMIT 0 , 20";*/

             $sql1 = "SELECT delevering_user_markers.address, delevering_user_markers.lat, delevering_user_markers.lng, delevering_user.* FROM delevering_user_markers LEFT JOIN delevering_user ON delevering_user_markers.user_id = delevering_user.id
             WHERE delevering_user_markers.user_id IN (SELECT id  FROM delevering_user WHERE dnd_mode = 0 ) LIMIT 20";

            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("pick_lat", $pickLatlng->lat, PDO::PARAM_STR);
            $stmt1->bindParam("pick_long", $pickLatlng->lng, PDO::PARAM_STR);

            $stmt1->execute();
            $user_location_data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            if( !empty($user_location_data) && sizeof($user_location_data) > 0 ) {
                $response['status'] = 'success';
                $response['message'] = 'We found '.sizeof($user_location_data).' Delivery boy in your area';
                $response['deliveryBoyData'] = $user_location_data;
                echo json_encode($response);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'No Delivery boy found in your area, Please Try again after some time';
                echo json_encode($response);
            }

        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Create Order
 */
$app->post('/createOrder', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $order_datails = json_decode($request->getBody());

    $user_id = $order_datails->userID;
    $order_data = $order_datails->userData;

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_vendor($user_id) ) {

            /*== Get vendor and delivery boy ==*/
            $deliveryboy_data = getUserByUserRole();
            $verndor_data = getAllUserData($user_id);

            $date = new DateTime();
            $timedata = $date->getTimestamp();

            $status = orderStatus(1);
            $db = getDB();

            $last_insert_id = ( getLastInsertId('order') > 0 ) ? getLastInsertId('order') : 0;
            $order_title = 'Order#'.($last_insert_id+1);

            $pickUplocation = $order_data->pickUplocation;
            $droplocation = $order_data->droplocation;

            $order_note = isset($order_data->item->shortNote) ? $order_data->item->shortNote : '';
            $pickup_lat = !empty($order_data->pickLatlng->lat) ? $order_data->pickLatlng->lat : '';
            $pickup_lng = !empty($order_data->pickLatlng->lng) ? $order_data->pickLatlng->lng : '';
            $drop_lat = !empty($order_data->dropLatlng->lat) ? $order_data->dropLatlng->lat : '';
            $drop_lng = !empty($order_data->dropLatlng->lng) ? $order_data->dropLatlng->lng : '';

            $enduser_name = isset($order_data->endUser->name) ? $order_data->endUser->name : '';
            $enduser_contact_no = isset($order_data->endUser->contactNo) ? $order_data->endUser->contactNo : '';

            $distance_arr = get_distance_and_duration($pickUplocation, $droplocation);
            $distance = isset($distance_arr['distance']) ? $distance_arr['distance'] : 0;
            
            if($distance < 5){
                $amount_with_Commission = ( isset($verndor_data['vendor_per_km_charge']) && $verndor_data['vendor_per_km_charge'] > 0 ) ? 5*$verndor_data['vendor_per_km_charge'] : 0;

                $deliveryman_amount = ( isset($verndor_data['deliveryman_per_km_charge']) && $verndor_data['deliveryman_per_km_charge'] > 0 ) ? 5*$verndor_data['deliveryman_per_km_charge'] : 0;
            }else {
                $amount_with_Commission = ( isset($verndor_data['vendor_per_km_charge']) && $verndor_data['vendor_per_km_charge'] > 0 ) ? $distance*$verndor_data['vendor_per_km_charge'] : 0;

                $deliveryman_amount = ( isset($verndor_data['deliveryman_per_km_charge']) && $verndor_data['deliveryman_per_km_charge'] > 0 ) ? $distance*$verndor_data['deliveryman_per_km_charge'] : 0;
            }


            $pickup_unique_code = sprintf("%06d", mt_rand(1, 999999));

            /*Inserting order values*/
            $sql = "INSERT INTO ".table_prifix."_order (`order_date`,`order_title` ,`status_modified`,`pickup_address`,`deliver_address`,`vendor_id`,`pickup_lat`,`pickup_long`,`drop_lat`,`drop_long`,`order_status`,`order_note`,`end_user_name`,`end_user_contact_no`,`pickup_unique_code`,`amount_with_Commission`,`deliveryman_amount`,`total_distance` ) VALUES(:current_timestamp,:order_title,:current_timestamp,:pickup_location,:drop_location,:vendor_id,:pickup_lat,:pickup_long,:drop_lat,:drop_lng,:order_status,:order_note,:enduser_name,:enduser_contact_no,:pickup_unique_code,:amount_with_Commission,:deliveryman_amount,:distance )";

            $stmt = $db->prepare($sql);

            $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);
            $stmt->bindParam("order_title", $order_title, PDO::PARAM_STR);
            $stmt->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
            $stmt->bindParam("pickup_location", $pickUplocation, PDO::PARAM_STR);
            $stmt->bindParam("drop_location", $droplocation, PDO::PARAM_STR);
            $stmt->bindParam("pickup_lat", $pickup_lat, PDO::PARAM_INT);
            $stmt->bindParam("pickup_long", $pickup_lng, PDO::PARAM_INT);
            $stmt->bindParam("drop_lat", $drop_lat, PDO::PARAM_INT);
            $stmt->bindParam("drop_lng", $drop_lng, PDO::PARAM_INT);
            $stmt->bindParam("order_status", $status, PDO::PARAM_STR);
            $stmt->bindParam("order_note", $order_note, PDO::PARAM_STR);
            $stmt->bindParam("pickup_unique_code", $pickup_unique_code, PDO::PARAM_INT);
            $stmt->bindParam("enduser_name", $enduser_name, PDO::PARAM_STR);
            $stmt->bindParam("enduser_contact_no", $enduser_contact_no, PDO::PARAM_STR);
            $stmt->bindParam("amount_with_Commission", $amount_with_Commission, PDO::PARAM_STR);
            $stmt->bindParam("deliveryman_amount", $deliveryman_amount, PDO::PARAM_STR);
            $stmt->bindParam("distance", $distance, PDO::PARAM_STR);

            if( $stmt->execute() ){

                $orderID = $db->lastInsertId();
                if( !empty($order_data->itemDetail) && sizeof($order_data->itemDetail) > 0 ){

                    $sql2 = "INSERT INTO `delevering_order_items` (`order_id`, `item_name`, `item_image`, `item_type`, `item_quantity`, `item_value`, `item_height`, `item_width`, `item_weight`, `item_status`) VALUES (:order_id,:item_name,:item_image,:item_type,:item_quantity,:item_value,:item_height,:item_width,:item_weight,:item_status)";

                    $stmt2 = $db->prepare($sql2);

                    foreach ($order_data->itemDetail as $item_key => $itemvalue) {

                        $stmt2->bindValue('order_id', $orderID);
                        $stmt2->bindValue('item_name', $itemvalue->item_name);
                        $stmt2->bindValue('item_image', null);
                        $stmt2->bindValue('item_type', $itemvalue->item_type);
                        $stmt2->bindValue('item_quantity', $itemvalue->item_qty);
                        $stmt2->bindValue('item_value', $itemvalue->item_value);
                        $stmt2->bindValue('item_width', $itemvalue->item_weight);
                        $stmt2->bindValue('item_height', null);
                        $stmt2->bindValue('item_weight', null);
                        $stmt2->bindValue('item_status', 1);
                        $stmt2->execute();
                    }
                }

                if( !empty($deliveryboy_data) && sizeof($deliveryboy_data) > 0 ){
                    
                    foreach ($deliveryboy_data as $key => $deliveryboy) {
                        $from = '';
                        $subject = 'New '.$order_title.' request - Dialadelivery';
                        $custom_message = '';
                        $custom_message.='We are happay to inform that we got a new order with following details:<br/><br/> Pickup Address - <b>'.$pickUplocation.'</b><br/>Delivery Address - <b>'.$droplocation.'</b></br></br>';
                        $custom_message.= '<br/><b>Note:-</b> Hurry up! If you are interested please accept by <a href="'.SITE_URL.'/#!/order-requests">Clicking here</a>.';
                        $user_email = $deliveryboy->email;
                        //$result = delivery_mail_template($user_email, $subject, $from, $custom_message);

                        if($deliveryboy->contact_no > 0 ){
                            $message = 'New '.$order_title.' request from delivering. Order pickup location is: '.$pickUplocation.' and drop location is: '.$droplocation;
                            //send_message($deliveryboy->contact_no, $message);
                        }
                    }
					$result=true;
                    if ($result) {

                        $vendor_email = $verndor_data['email'];
                        $vendor_contact_no = $verndor_data['contact_no'];
                        $subject = 'Your '.$order_title.' receipt - Dialadelivery';
                        $custom_message = '';
                        $custom_message = 'We have successfully recieved your '.$order_title.'. We will share Delivery Man details once someone accepted. and your pickup code is '.$pickup_unique_code;
                        //delivery_mail_template($vendor_email, $subject, '', $custom_message);

                        if($deliveryboy->contact_no > 0 ){
                            $custom_message = 'We have successfully recieved your '.$order_title.'. We will share Delivery Man details once someone accepted. and your pickup code is '.$pickup_unique_code;
                            //send_message($vendor_contact_no, $message);
                        }

                        $response['status'] = 'success';
                        $response['message'] = 'Your order is confirmed and our deliveryboy will contact you soon';
                        echo json_encode($response);
                    }else{
                        $response['status'] = 'error';
                        $response['message'] = 'Error in request sent to delivery boys.';
                        echo json_encode($response);
                    }

                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'Error in Creating order, Because no deliveryboy found';
                    echo json_encode($response);
                }

            }else {
                $response['status'] = 'error';
                $response['message'] = 'Error in Creating order';
                echo json_encode($response);
            }

        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Delete User Stores
 */
$app->Post('/acceptOrderByDeliveryBoy', function () {

    $response = array();
    $request = \Slim\Slim::getInstance()->request();
    $ajax_data = json_decode($request->getBody());

    $user_id = $ajax_data->userID;
    $order_id = $ajax_data->orderId;

    try{

        if ( isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_deliveryman($user_id) ) {

            if ( $order_id > 0 ) {

                $db = getDB();
                $status = orderStatus(1);

                $sql1 = "SELECT * FROM ".table_prifix."_order WHERE id=:order_id AND order_status=:order_status AND delivery_agent_id <= 0";
                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("order_id", $order_id, PDO::PARAM_STR);
                $stmt1->bindParam("order_status", $status, PDO::PARAM_INT);
                $stmt1->execute();
                $order_data = $stmt1->fetchAll(PDO::FETCH_OBJ);


                if ( !empty($order_data[0]) && sizeof($order_data[0]) > 0 ) {

                    $vendor_data = getAllUserData($order_data[0]->vendor_id);
                    $deliveryman_data = getAllUserData($user_id);

                    $order_status = orderStatus();
                    $sql = "UPDATE ".table_prifix."_order SET delivery_agent_id=:delivery_agent,order_status=:order_status WHERE id=:order_id";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("delivery_agent", $user_id, PDO::PARAM_STR);
                    $stmt->bindParam("order_status", $order_status, PDO::PARAM_STR);
                    $stmt->bindParam("order_id", $order_id, PDO::PARAM_STR);

                    if ($stmt->execute()) {

                        /*== Send message to Vendor  ==*/
                        if( isset($vendor_data['contact_no']) 
                            && $deliveryman_data['contact_no'] > 0 
                            && isset($deliveryman_data['first_name']) 
                            && !is_order_type_api($order_id) ) {
                            $vendor_message_body = 'Your '.$order_data[0]->order_title.' has been ready to pickup our deliveryboy details- '.$deliveryman_data['first_name'].'/'.$deliveryman_data['contact_no'];
                            //send_message($vendor_data['contact_no'], $vendor_message_body);
                        }

                        /*== Send mail to Vendor  ==*/
                        if( isset($vendor_data['email']) 
                            && $deliveryman_data['email'] !== ''
                            && $deliveryman_data['first_name']
                            && $deliveryman_data['contact_no'] 
                            && !is_order_type_api($order_id) ) {

                            $subject = $order_data[0]->order_title.' status changed - Dialadelivery';

                            $vendor_email_body = 'We are happay to inform that Mr/Mrs '.$deliveryman_data['first_name'].' accepted your '.$order_data[0]->order_title.' and below are more details.<br/><br/>Full Name - '.$deliveryman_data['first_name'].' '.$deliveryman_data['last_name'].'<br/>Contact No. '.$deliveryman_data['contact_no'];
                            //delivery_mail_template($vendor_data['email'], $subject, '', $vendor_email_body);
                        }

                        /*== Send message to delivery boy ==*/
                        if( isset($deliveryman_data['contact_no']) 
                            && $deliveryman_data['contact_no'] > 0 
                            && !is_order_type_api($order_id) ) {

                            $deliveryman_message_body = $order_data[0]->order_title.' successfully accepted. Vendor details '.$vendor_data['first_name'].'/'.$vendor_data['contact_no'];
                            //send_message($deliveryman_data['contact_no'], $deliveryman_message_body);
                        }

                        if( isset($deliveryman_data['email']) 
                            && $deliveryman_data['email'] !== '' 
                            && isset($vendor_data['contact_no']) 
                            && isset($vendor_data['first_name']) 
                            && !is_order_type_api($order_id) ) {

                            $deliveryman_email_body = $order_data[0]->order_title.' successfully accepted.<br/><br/>Vendor Details:<br/>Name: <b>'.$vendor_data['first_name'].' '.$vendor_data['last_name'].'</b><br/>Contact No.: <b>'.$vendor_data['contact_no'].'</b>';
                            $subject = $order_data[0]->order_title.' successfully accepted';
                            //delivery_mail_template($deliveryman_data['email'], $subject, '', $deliveryman_email_body);
                        }

                        $response['status'] = 'success';
                        $response['message'] = 'You have successfully accept this order please check in your order section for more details.!';
                        echo json_encode($response);

                        $api_order_data = array(
                            'yfo_order_id' => $order_data[0]->vendor_api_order_id,
                            'yfo_driver_name' => $deliveryman_data['first_name'].' '.$deliveryman_data['last_name'],
                            'yfo_driver_ph' => $deliveryman_data['contact_no'],
                        );

                        $vendor_api_key = isset($vendor_data['api_key']) ? $vendor_data['api_key'] : '';
                        send_deliveryman_data_to_vendor($url = '', $api_order_data , $vendor_api_key);
                    }
                }else{
                    $response['status'] = 'error';
                    $response['message'] = 'Order already pick by other delivery boy, Please try with diffrent one!';
                    echo json_encode($response);
                }
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Error in get order details';
                echo json_encode($response);
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = 'unauthorized user';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all User Order
 */
$app->get('/getAllOrder/:user_id', function($user_id) {

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $sql = "SELECT * FROM ".table_prifix."_order LEFT JOIN ".table_prifix."_order_items ON delevering_order.id = delevering_order_items.order_id WHERE vendor_id=:vendor_id";

            //$sql = "SELECT * FROM ".table_prifix."_order WHERE vendor_id=:vendor_id";
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userOrders = array();
                $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
                $order_data = array();
                if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                    foreach ($userOrders as $key => $value) {
                        $temp = $itemmeta = array();
                        if($value->order_id > 0 ){
                            if( array_key_exists($value->order_id, $order_data)){
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data[$value->order_id]['orderItems'][] = $itemmeta;
                            }else {
                                $temp['order_id'] = $value->order_id;
                                $temp['vendor_id'] = $value->vendor_id;
                                $temp['order_title'] = $value->order_title;
                                $temp['pickup_address'] = $value->pickup_address;
                                $temp['deliver_address'] = $value->deliver_address;
                                $temp['pickup_time'] = $value->pickup_time;
                                $temp['deliver_time'] = $value->deliver_time;
                                $temp['order_date'] = date('d M Y H:i:s', $value->order_date);
								$temp['item_quantity'] = $value->item_quantity;
                                $temp['status_modified'] = $value->status_modified;
                                $temp['delivery_agent_id'] = $value->delivery_agent_id;
                                $temp['order_status'] = $value->order_status;
								
                                $order_data[$value->order_id] = $temp;

                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data[$value->order_id]['orderItems'][] = $itemmeta;
                            }
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = 'User orders get successfully';
                    $response['userOrders'] = $order_data;
                    echo json_encode($response);
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'No order found along with this user';
                    echo json_encode($response);
                }

            } else {
                $response['status'] = 'error';
                $response['message'] = $stmt1->error;
                echo json_encode($response);
            }

            $db = null;
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all User Order
 */
$app->get('/getAllDeliveryBoyOrders/:user_id', function($user_id) {

    try{

        if ( $user_id > 0 
            && isset($_SESSION["userID"]) 
            && $_SESSION["userID"] == apiToken($user_id) 
            && is_deliveryman($user_id) ) {

            $sql = "SELECT * FROM ".table_prifix."_order LEFT JOIN ".table_prifix."_order_items ON delevering_order.id = delevering_order_items.order_id WHERE delivery_agent_id=:vendor_id";

            //$sql = "SELECT * FROM ".table_prifix."_order WHERE vendor_id=:vendor_id";
            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userOrders = array();
                $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);

                $order_data = array();
                if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                    foreach ($userOrders as $key => $value) {
                        $temp = $itemmeta = array();
                        if($value->id > 0 ){
                            if( array_key_exists($value->id, $order_data)){
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data[$value->id]['orderItems'][] = $itemmeta;
                            }else {
                                $temp['order_id'] = $value->id;
                                $temp['vendor_id'] = $value->vendor_id;
                                $temp['order_title'] = $value->order_title;
                                $temp['pickup_address'] = $value->pickup_address;
                                $temp['deliver_address'] = $value->deliver_address;
                                $temp['pickup_time'] = $value->pickup_time;
                                $temp['deliver_time'] = $value->deliver_time;
                                $temp['order_date'] = date('m/d/Y H:i:s', $value->order_date);
                                $temp['status_modified'] = $value->status_modified;
                                $temp['delivery_agent_id'] = $value->delivery_agent_id;
                                $temp['order_status'] = $value->order_status;
                                $temp['item_count'] = $value->item_count;
                                
                                $order_data[$value->id] = $temp;

                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data[$value->id]['orderItems'][] = $itemmeta;
                            }
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = 'User orders get successfully';
                    $response['userOrders'] = $order_data;
                    echo json_encode($response);
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'No order found along with this user';
                    echo json_encode($response);
                }

            } else {
                $response['status'] = 'error';
                $response['message'] = $stmt1->error;
                echo json_encode($response);
            }

            $db = null;
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all User Feedbacks - Feedbacks he got
 */
$app->get('/getUserFeedbacks/:user_id', function($user_id) {

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $sql = "SELECT * FROM ".table_prifix."_tbl_reviews LEFT JOIN ".table_prifix."_user ON from_user_id = delevering_user.id WHERE to_user_id=:user_id";

            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userFeedbacks = array();
                $userFeedbacks = $stmt->fetchAll(PDO::FETCH_OBJ);
                $response['status'] = 'success';
                $response['message'] = 'User feedbacks get successfully';
                $response['userFeedbacks'] = $userFeedbacks;
                echo json_encode($response);
            }
            else
            {
                $response['status'] = 'error';
                $response['message'] = 'some error';
                echo json_encode($response);
            }
        }
    }
    catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get all User Given Feedbacks - Feedbacks he gave
 */
$app->get('/getUserGivenFeedbacks/:user_id', function($user_id) {

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $sql = "SELECT * FROM ".table_prifix."_tbl_reviews LEFT JOIN ".table_prifix."_user ON to_user_id = delevering_user.id WHERE from_user_id =:user_id";

            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userFeedbacks = array();
                $userFeedbacks = $stmt->fetchAll(PDO::FETCH_OBJ);
                $response['status'] = 'success';
                $response['message'] = 'User feedbacks get successfully';
                $response['userFeedbacks'] = $userFeedbacks;
                echo json_encode($response);
            }
            else
            {
                $response['status'] = 'error';
                $response['message'] = 'some error';
                echo json_encode($response);
            }
        }
    }
    catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all User Order
 */
$app->get('/getOrder/:order_id/:user_id', function($order_id, $user_id) {

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $sql = "SELECT * FROM ".table_prifix."_order LEFT JOIN ".table_prifix."_order_items ON delevering_order.id = delevering_order_items.order_id WHERE ( vendor_id=:user_id AND delevering_order.id=:order_id) OR ( delivery_agent_id=:user_id AND delevering_order.id=:order_id)";

            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userOrders = array();
                $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);


                $order_data = array();
                if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                    foreach ($userOrders as $key => $value) {
                        $temp = $itemmeta = array();
                        if($value->id > 0 ){
                            if( in_array($value->id, $order_data) ){
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data['orderItems'][] = $itemmeta;
                            }else {

                                $temp['order_id'] = $value->id;
                                $temp['vendor_id'] = $value->vendor_id;
                                $temp['order_title'] = $value->order_title;
                                $temp['order_note'] = $value->order_note;
                                $temp['pickup_address'] = $value->pickup_address;
                                $temp['deliver_address'] = $value->deliver_address;
                                $temp['pickup_time'] = !empty($value->pickup_time) ? date('d M Y H:i:s', $value->pickup_time) : '';
                                $temp['deliver_time'] = !empty($value->deliver_time) ? date('d M Y H:i:s', $value->deliver_time) : '';
                                $temp['order_date'] = isset($value->order_date) ? date('d M Y H:i:s', $value->order_date) : '';
                                $temp['status_modified'] = $value->status_modified;
                                $temp['delivery_agent_id'] = ($value->delivery_agent_id) > 0 ? $value->delivery_agent_id : '';                                
                                $temp['order_status'] = $value->order_status;
                                $temp['pickup_unique_code'] = $value->pickup_unique_code;
                                $temp['end_user_name'] = $value->end_user_name;
                                $temp['end_user_contact_no'] = $value->end_user_contact_no;
                                $temp['ship_total'] = $value->ship_total;
                                $temp['item_count'] = $value->item_count;

                                if( $value->delivery_agent_id > 0 ){
                                    $user_data =getAllUserData($value->delivery_agent_id);
                                    $temp['delivery_agent_name'] = $user_data['first_name'];
                                    $temp['delivery_agent_number'] = $user_data['contact_no'];
                                }else {
                                    $temp['delivery_agent_name'] = '';
                                    $temp['delivery_agent_number'] = '';
                                }

                                $order_data = $temp;
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data['orderItems'][] = $itemmeta;
                            }
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = 'User orders get successfully';
                    $response['userOrders'] = $order_data;
                    echo json_encode($response);
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'No order data found along with this order id';
                    echo json_encode($response);
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = $stmt1->error;
                echo json_encode($response);
            }

            $db = null;
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Send Push Notification
 */
$app->post('/sendPush', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $noti_data = json_decode($request->getBody());

    $pushdata = $noti_data->pushdata;
    $url = $noti_data->url;
    $key = $noti_data->key;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Key: '.$key)); //setting custom header
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $pushdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $api_output = curl_exec ($ch);
    curl_close ($ch);
    echo $api_output;
});

/**
 * Get all delivery boys
 */
$app->get('/getOneDeliveryBoy/:user_id', function($user_id) {

    $response = array();

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id)) {

            $db = getDB();

            $sql1 = "SELECT * FROM delevering_user_markers WHERE user_id=:user_id";

            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt1->execute();

            $delivery_boy_data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            if( !empty($delivery_boy_data) && sizeof($delivery_boy_data) > 0 ) {
                $response['status'] = 'success';
                $response['message'] = 'Delivery boy get successfully';
                $response['deliveryBoyData'] = $delivery_boy_data;
                $response['deliveryBoyStatus'] = deliveryman_on_order($user_id);
                $response['orderNotification'] = new_order_notification($user_id);
                echo json_encode($response);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'No Delivery boy found';
                $response['deliveryBoyData'] = array();
                echo json_encode($response);
            }

        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Check new ordre 
 */
$app->get('/checkNewOrder/:user_id', function($user_id) {

    $response = array();

    if ( $user_id > 0 
        && isset($_SESSION["userID"]) 
        && $_SESSION["userID"] == apiToken($user_id)
        && is_deliveryman($user_id) ) {

        if( new_order_notification($user_id) ) {
            $response['status'] = 'success';
            $response['message'] = 'order status get successfully';
            $response['orderNotification'] = new_order_notification($user_id) ;
            echo json_encode($response);
        }else{
            $response['status'] = 'error';
            $response['message'] = 'No order found!!';
            echo json_encode($response);
        }

    }else{
        $response['status'] = 'error';
        $response['message'] = 'Error in validating data';
        echo json_encode($response);
    }
});

/**
 * Get all Delivery boy
 */
$app->post('/getAllDeliveryBoy', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userId;

    try{

        if ( $user_id > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {

            $user_lat = $request_data->userLat;
            $user_lng = $request_data->userLng;
            $distance = isset($request_data->distance) ? $request_data->distance : 'all';
            $workStatus = isset($request_data->workStatus) ? $request_data->workStatus : 0;

            if( $workStatus == 2 ) {
              $both = " ";
            }else {
              $both = "WHERE dnd_mode=:workStatus";
            }

            $db = getDB();

            if( $distance == 'all' ){
                $distance = 1000000;
            }

            $sql1 = "SELECT delevering_user_markers.*,delevering_user.*, ( 6371 * acos( cos( radians(:user_lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:user_lng) ) + sin( radians(:user_lat) ) * sin( radians( lat ) ) ) ) AS distance FROM delevering_user_markers LEFT JOIN delevering_user ON delevering_user_markers.user_id = delevering_user.id
             WHERE delevering_user_markers.user_id IN (SELECT id FROM delevering_user $both) HAVING distance < $distance ORDER BY distance";

            $stmt1 = $db->prepare($sql1);

            $stmt1->bindParam("user_lat", $user_lat, PDO::PARAM_STR);
            $stmt1->bindParam("user_lng", $user_lng, PDO::PARAM_STR);
            $stmt1->bindParam("workStatus", $workStatus, PDO::PARAM_INT);

            $stmt1->execute();

            $delivery_boy_data = $stmt1->fetchAll(PDO::FETCH_OBJ);

            if( !empty($delivery_boy_data) && sizeof($delivery_boy_data) > 0 ) {
                $response['status'] = 'success';
                $response['message'] = 'Delivery boy get successfully';
                $response['deliveryBoyData'] = $delivery_boy_data;
                echo json_encode($response);
            }else{
                $response['status'] = 'error';
                $response['message'] = 'No Delivery boy found';
                $response['deliveryBoyData'] = array();
                echo json_encode($response);
            }

        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all Delivery boy
 */
$app->post('/insertUpdateCordinates', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userId;
    $logging_user_data = getAllUserData($user_id);

    try{

        if ( $user_id > 0 
            && isset($_SESSION["userID"])
            && !empty($logging_user_data) 
            && sizeof($logging_user_data) > 0 
            && isset($_SESSION["userID"]) 
            && is_deliveryman($user_id) ) {

            $user_lat = $request_data->userLat;
            $user_lng = $request_data->userLng;

            $db = getDB();
            $sql = "SELECT * FROM delevering_user_markers WHERE user_id=:user_id";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->execute();
            $delivery_boy_data = $stmt->fetchAll(PDO::FETCH_OBJ);

            $date = new DateTime();
            $timedata = $date->getTimestamp();


            if( !empty($delivery_boy_data) && sizeof($delivery_boy_data) > 0 ) {

                $address = get_address_from_lat_lng($user_lat, $user_lng);
                $user_name = $logging_user_data['first_name'].' '.$logging_user_data['last_name'];

                $sql1 = "UPDATE delevering_user_markers SET name=:user_name,address=:address,lat=:user_lat,lng=:user_lng,last_insert_time=:last_insert_time WHERE user_id=:user_id";
                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
                $stmt1->bindParam("user_name", $user_name, PDO::PARAM_STR);
                $stmt1->bindParam("address", $address, PDO::PARAM_STR);
                $stmt1->bindParam("user_lat", $user_lat, PDO::PARAM_STR);
                $stmt1->bindParam("user_lng", $user_lng, PDO::PARAM_STR);
                $stmt1->bindParam("last_insert_time", $timedata, PDO::PARAM_STR);
            }else{

                $address = get_address_from_lat_lng($user_lat, $user_lng);
                $user_name = $logging_user_data['first_name'].' '.$logging_user_data['last_name'];

                $sql1 = "INSERT INTO ".table_prifix."_user_markers (user_id,name,address,lat,lng,last_insert_time) VALUES (:user_id,:user_name,:address,:user_lat,:user_lng,:last_insert_time)";

                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
                $stmt1->bindParam("user_name", $user_name, PDO::PARAM_STR);
                $stmt1->bindParam("address", $address, PDO::PARAM_STR);
                $stmt1->bindParam("user_lat", $user_lat, PDO::PARAM_STR);
                $stmt1->bindParam("user_lng", $user_lng, PDO::PARAM_STR);
                $stmt1->bindParam("last_insert_time", $timedata, PDO::PARAM_STR);
            }

            if( $stmt1->execute() ){
                $db  = null;
                $response['status'] = 'success';
                $response['message'] = 'cordinate update successfully';
                echo json_encode($response);
            }else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating cordinate';
                echo json_encode($response);
            }

        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Check pickup code
 */
$app->post('/checkOrderPickupUniqueCode', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userID;
    $order_id = $request_data->orderId;
    $order_status = orderStatus(3);

    try{

        if ( $user_id > 0
            && isset($_SESSION["userID"])
            && $_SESSION["userID"] == apiToken($user_id) ) {

            $db = getDB();
            $sql = "SELECT * FROM ".table_prifix."_order WHERE id=:order_id";
            $stmt = $db->prepare($sql);

            $stmt->bindParam("order_id", $order_id, PDO::PARAM_STR);
            $stmt->execute();
            $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
            $userOrders = current($userOrders);


            if ( !empty($userOrders) && sizeof($userOrders) > 0 ) {

                $deliveryboy = getDeliveryBoyPersonalData($order_id);
                $vender_data = getAllUserData($userOrders->vendor_id);
    
                $num_str = sprintf("%06d", mt_rand(1, 999999));
                
                $date = new DateTime();
                $timedata = $date->getTimestamp();

                $sql1 = "UPDATE ".table_prifix."_order SET order_status=:order_status,pickup_time=:current_timestamp,delivery_agent_id=:delivery_agent_id,delivery_unique_code=:delivery_unique_code WHERE id=:order_id";

                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("order_status", $order_status, PDO::PARAM_STR);
                $stmt1->bindParam("order_id", $order_id, PDO::PARAM_INT);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
                $stmt1->bindParam("delivery_agent_id", $deliveryboy['id'], PDO::PARAM_INT);
                $stmt1->bindParam("delivery_unique_code", $num_str, PDO::PARAM_INT);
                $db = null;

                if( $stmt1->execute() ){

                    $response['status'] = 'success';
                    $response['message'] = "Order successfully marked as $order_status!";
                    echo json_encode($response);
                        
                    /*== Send mail to deliveryman ==*/
                    if( !empty($deliveryboy) 
                        && isset($deliveryboy['email']) 
                        && $deliveryboy['email'] !== ''
                        && !is_order_type_api($order_id) ) {

                        $subject = 'Order Status changed!';
                        $custom_message = "Hi, <br /><br /><b>".$userOrders->order_title."</b> status changed to $order_status, and order complete unique code has been sent to end user and vendor";
                        //delivery_mail_template($deliveryboy['email'], $subject, '', $custom_message);
                    }

                    if( $vender_data['contact_no'] > 0 
                        && !empty($deliveryboy) 
                        && sizeof($deliveryboy) > 0 
                        && !is_order_type_api($order_id) ){
                        
                        $vender_msg_subject = 'Order Status changed!';
                        $vender_msg = 'Your '.$userOrders->order_title.' has been picked-up by Delivery Boy '.$deliveryboy['first_name'].' and their contact number is '.$deliveryboy['contact_no'].' and unique code for complete order is '.$num_str;
                        /*== Send message to vendor ==*/
                        send_message($vender_data['contact_no'], $vender_msg);
                        //delivery_mail_template($vender_data['email'], $vender_msg_subject, '', $vender_msg);
                    }

                    if( isset($userOrders->end_user_contact_no) 
                        && $userOrders->end_user_contact_no > 0 
                        && !is_order_type_api($order_id) ){

                        $traking_url = SITE_URL.'/custom/track-order.php?order='.$order_id;
                        $short_traking_url = short_url($traking_url);

                        /*== Send message ==*/
                        // $message = 'Your '.$userOrders->order_title.' has been picked-up by Delivery Boy '.$deliveryboy['first_name'].' and their contact number is '.$deliveryboy['contact_no'].' and unique code for complete order is '.$num_str.', order tracking link -'.$short_traking_url;
                        $message = 'Your '.$userOrders->order_title.' has been picked-up by Delivery Boy '.$deliveryboy['first_name'].' and their contact number is '.$deliveryboy['contact_no'].' and unique code for complete order is '.$num_str;
                        send_message($userOrders->end_user_contact_no, $message);
                    }
    
                    $api_order_data = array(
                        'yfo_order_id' => isset($userOrders->vendor_api_order_id) ? $userOrders->vendor_api_order_id : 0,
                    );

                    $vendor_api_key = isset($vender_data['api_key']) ? $vender_data['api_key'] : '';
                    api_pickup_order_by_deliveryman($url = '', $api_order_data , $vendor_api_key);

                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'Error in order pickup, Please try again!';
                    echo json_encode($response);
                }

            }else{
                $response['status'] = 'error';
                $response['message'] = 'Error in order pickup, Please try again!';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all Delivery boy
 */
$app->post('/checkOrderDeliverCode', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userID;
    $order_id = $request_data->orderId;
    $orderUniqueCode = $request_data->orderUniqueCode;
    $order_status = orderStatus(7);

    try{

        if ( $user_id > 0 
            && isset($_SESSION["userID"])
            && $_SESSION["userID"] == apiToken($user_id) ) {

            $db = getDB();
            $sql = "SELECT * FROM ".table_prifix."_order WHERE id=:order_id";
            $stmt = $db->prepare($sql);

            $stmt->bindParam("order_id", $order_id, PDO::PARAM_STR);
            $stmt->execute();
            $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
            $userOrders = current($userOrders);


            if ( !empty($userOrders) && sizeof($userOrders) > 0 && $userOrders->delivery_unique_code == $orderUniqueCode) {

                $deliveryboy = getDeliveryBoyPersonalData($order_id);
                $vender_data = getAllUserData($userOrders->vendor_id);

                $date = new DateTime();
                $timedata = $date->getTimestamp();
                $sql1 = "UPDATE ".table_prifix."_order SET order_status=:order_status,deliver_time=:current_timestamp WHERE id=:order_id";
                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
                $stmt1->bindParam("order_status", $order_status, PDO::PARAM_STR);
                $stmt1->bindParam("order_id", $order_id, PDO::PARAM_STR);
                $db = null;                    

                if($stmt1->execute()){

                    if( !empty($deliveryboy) 
                        && isset($deliveryboy['email']) 
                        && $deliveryboy['email'] !== '' ) {
                        if( !is_order_type_api($order_id) ){
                            $subject = 'Order Status changed!';
                            $custom_message = "Hi, <br /><b>".$userOrders->order_title."</b><p>Order status changed to $order_status";
                            //delivery_mail_template($deliveryboy['email'], $subject, '', $custom_message);
                        }
                    }

                    if( $vender_data['contact_no'] > 0 
                        && !empty($deliveryboy) 
                        && sizeof($deliveryboy) > 0 
                        && !is_order_type_api($order_id) ){

                        $message = 'Your '.$userOrders->order_title.' has been successfully delivered to this customer - '.$userOrders->end_user_name.'/'.$userOrders->end_user_contact_no;
                        /*== Send message to vendor ==*/
                        //send_message($vender_data['contact_no'], $message);
                    }

                    if( isset($userOrders->end_user_contact_no) 
                        && $userOrders->end_user_contact_no > 0 
                        && !is_order_type_api($order_id) ){
                        /*== Send message ==*/

                        $message = 'Your '.$userOrders->order_title.' has been successfully Delivered by Delivery Boy '.$deliveryboy['first_name'].' and their contact number is '.$deliveryboy['contact_no'];
                        //send_message($userOrders->end_user_contact_no, $message);
                    }

                    $response_release = get_order_charge_update_release_amt($order_id);

                    if($response_release){
                        $response['status'] = 'success';
                        $response['message'] = "Order successfully marked as $order_status!";
                        echo json_encode($response);
                    }else {
                        $response['status'] = 'success';
                        $response['message'] = "Order successfully marked as $order_status!";
                        echo json_encode($response);
                    }

                    $vendor_api_order_id = isset($userOrders->vendor_api_order_id) ? $userOrders->vendor_api_order_id : 0;
                    $api_key = isset($vender_data['api_key']) ? $vender_data['api_key'] : '';

                    $result = complete_mark_order_by_api($url = '', $vendor_api_order_id, $api_key);
                }
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Invalid order Status change!';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all Delivery boy
 */
$app->post('/trackOrder', function() {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userID;
    $order_id = $request_data->orderId;

    try{

        if ( $user_id > 0 
            && isset($_SESSION["userID"])
            && $_SESSION["userID"] == apiToken($user_id) ) {

            $deliveryboy_data = getDeliveryBoyPersonalData($order_id);

            if( isset($deliveryboy_data['id']) && $deliveryboy_data['id'] > 0 ){

                $db = getDB();
                $sql = "SELECT * FROM delevering_user_markers WHERE user_id=:user_id";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("user_id", $deliveryboy_data['id'], PDO::PARAM_INT);
                $stmt->execute();
                $deliveryboy_cordinates = $stmt->fetch(PDO::FETCH_OBJ);


                if ( !empty($deliveryboy_cordinates) && sizeof($deliveryboy_cordinates) > 0 ) {
                    $response['tackOrderData'] = $deliveryboy_cordinates;
                    $response['status'] = 'success';
                    $response['message'] = 'Delivery boy data get successfully';
                    echo json_encode($response);
                }
            }else {
                $response['status'] = 'error';
                $response['message'] = 'Error in getting deliveryboy';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all Delivery boy
 */
$app->get('/getAllAwaitingOrder/:user_id', function($user_id) {

    date_default_timezone_set("Asia/Kolkata");

  try{
    $db = getDB();
    $sql1 = "SELECT * FROM delevering_usermeta WHERE user_id=:user_id";
    $stmt1 = $db->prepare($sql1);
    $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
    $stmt1->execute();

    $delivery_boy_data = $stmt1->fetch(PDO::FETCH_OBJ);

    $deliveryManIsAvailable = false;

    /*if( !empty($delivery_boy_data) ) {
        $currenttime = time();
        $deliverymanfromtime = strtotime($delivery_boy_data->time_avalibility_from);
        $deliverymantotime = strtotime($delivery_boy_data->time_avalibility_to);
        // echo $currenttime."<br>";
        // echo $deliverymanfromtime."<br>";
        // echo $deliverymantotime."<br>";
        $currenttimetime = date("Hi",$currenttime)."<br>";
        $deliverymanfromtimetime = date("Hi",$deliverymanfromtime)."<br>";
        $deliverymantotimetime = date("Hi",$deliverymantotime)."<br>";
        // exit;
        if($currenttimetime >= $deliverymanfromtimetime && $currenttimetime <=$deliverymantotimetime)
        {
            $deliveryManIsAvailable = true;
        }

    }*/

    if( !empty($delivery_boy_data) ) {
        $today_day = date("D",time());
        // $today_hour = date("H",time());
        // $today_mins = date("i",time());
        
        $slotsdata = json_decode($delivery_boy_data->availability_slots);
        $currenttime = date("Hi",time());
        if($slotsdata)
        {
            foreach ($slotsdata as $slot) {
                if($slot->day !="everyday")
                {
                    if(strtolower($today_day) == strtolower($slot->day))
                    {
                        $fromTime = date("Hi", STRTOTIME($slot->fromTime));
                        $toTime = date("Hi", STRTOTIME($slot->toTime));
                        if($currenttime >= $fromTime && $currenttime <= $toTime)
                        {
                            $deliveryManIsAvailable = true;
                            break;
                        }
                    }
                }
                else
                {
                    $fromTime = date("Hi", STRTOTIME($slot->fromTime));
                    $toTime = date("Hi", STRTOTIME($slot->toTime));
                    if($currenttime >= $fromTime && $currenttime <= $toTime)
                    {
                        $deliveryManIsAvailable = true;
                        break;
                    }
                }
            }
        }
        // echo "deliveryManIsAvailable: ".$deliveryManIsAvailable;
        // exit;
    }


      if ( $user_id > 0 
            && isset($_SESSION["userID"])
            && $_SESSION["userID"] == apiToken($user_id) 
            && is_deliveryman($user_id)) {

            if($deliveryManIsAvailable)
            {

            if( !is_deliveryman_active($user_id) ) {
                $response['status'] = 'error';
                $response['message'] = 'You have active DND mode!!';
                echo json_encode($response);
                die();
            }


            $status = orderStatus(1);
            $sql = "SELECT * FROM delevering_order LEFT JOIN delevering_order_items ON delevering_order.id = delevering_order_items.order_id WHERE delevering_order.order_status=:status AND delevering_order.delivery_agent_id <= 0";

          
            $stmt = $db->prepare($sql);
            $stmt->bindParam("status", $status, PDO::PARAM_STR);

          if( $stmt->execute() ){
              $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
              $order_data = array();


              $deliveryman_data = getAllUserData($user_id);
              if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                    /*== Remove inhouse order from backend ==*/
                    foreach ($userOrders as $key => $value) {
                        if( isset($value->delivery_type) && $value->delivery_type == 'inhouse' ){
                            if( checkIsValidInhouseOrder($user_id, $value->vendor_id)  ){
                                //Do nothing 
                            }else {
                                unset($userOrders[$key]);
                            }
                        }else if( isset($value->delivery_type) 
                                && isset($deliveryman_data['delivery_mode'])
                                && $deliveryman_data['delivery_mode'] == 'inhouse' 
                                && $value->delivery_type !== 'inhouse') {

                            unset($userOrders[$key]);
                        }
                    }
                    // echo json_encode($userOrders);
// exit;
                  foreach ($userOrders as $key => $value) {
                      $temp = $itemmeta = array();
                      if($value->id > 0 ){
                          if( array_key_exists($value->id, $order_data)){
                              $itemmeta['item_name'] = $value->item_name;
                              $itemmeta['item_image'] = $value->item_image;
                              $itemmeta['item_type'] = $value->item_type;
                              $itemmeta['item_quantity'] = $value->item_quantity;
                              $itemmeta['item_value'] = $value->item_value;
                              $itemmeta['item_height'] = $value->item_height;
                              $itemmeta['item_width'] = $value->item_width;
                              $itemmeta['item_weight'] = $value->item_weight;
                              $itemmeta['item_status'] = $value->item_status;
                              $order_data[$value->id]['orderItems'][] = $itemmeta;
                          }else {
                              $temp['order_id'] = $value->id;
                              $temp['vendor_id'] = $value->vendor_id;
                              $temp['order_title'] = $value->order_title;
                              $temp['pickup_address'] = $value->pickup_address;
                              $temp['deliver_address'] = $value->deliver_address;
                              $temp['pickup_time'] = $value->pickup_time;
                              $temp['deliver_time'] = $value->deliver_time;
                              $temp['order_date'] = date('d M Y H:i:s', $value->order_date);
                              $temp['status_modified'] = $value->status_modified;
                              $temp['delivery_agent_id'] = $value->delivery_agent_id;
                              $temp['order_status'] = $value->order_status;
                              $temp['item_count'] = $value->item_count;
                              $temp['pickup_lat'] = $value->pickup_lat;
                              $temp['pickup_long'] = $value->pickup_long;
                              $temp['drop_lat'] = $value->drop_lat;
                              $temp['drop_long'] = $value->drop_long;
                              $temp['item_quantity'] = $value->item_quantity;
							  $temp['delivery_type'] = $value->delivery_type;
                              $order_data[$value->id] = $temp;

                              $itemmeta['item_name'] = $value->item_name;
                              $itemmeta['item_image'] = $value->item_image;
                              $itemmeta['item_type'] = $value->item_type;
                              $itemmeta['item_quantity'] = $value->item_quantity;
                              $itemmeta['item_value'] = $value->item_value;
                              $itemmeta['item_height'] = $value->item_height;
                              $itemmeta['item_width'] = $value->item_width;
                              $itemmeta['item_weight'] = $value->item_weight;
                              $itemmeta['item_status'] = $value->item_status;
                              $order_data[$value->id]['orderItems'][] = $itemmeta;
                          }
                      }
                  }

                  if ( !empty($userOrders) && sizeof($userOrders) > 0 ){

                    //check if any order is accepted by the user/deliveryman

                    $userAcceptedOrders=[];
                    $sql = "SELECT * FROM delevering_order WHERE delevering_order.delivery_agent_id = :user_id and delevering_order.order_status!=:status1 and delevering_order.order_status!=:status2";

                    $db = getDB();
                    $stmt = $db->prepare($sql);
					$s1=orderStatus(1);
					$s2=orderStatus(7);
                    $stmt->bindParam("user_id", $user_id, PDO::PARAM_STR);
                    $stmt->bindParam("status1", $s1, PDO::PARAM_STR);
                    $stmt->bindParam("status2", $s2, PDO::PARAM_STR);
					 
                    if( $stmt->execute() ){
                        $userAcceptedOrders = $stmt->fetchAll(PDO::FETCH_OBJ);
                    }

                      $response['status'] = 'success';
                      $response['message'] = 'User orders get successfully';
                      $response['userOrders'] = $order_data;
                      $response['userAcceptedOrders'] = $userAcceptedOrders;
                      echo json_encode($response);
                  }else {
                    $response['status'] = 'error';
                    $response['message'] = 'No order found along with this user';
                    echo json_encode($response);
                  }

              }else {
                  $response['status'] = 'error';
                  $response['message'] = 'No order found along with this user';
                  echo json_encode($response);
              }

          } else {
              $response['status'] = 'error';
              $response['message'] = $stmt1->error;
              echo json_encode($response);
          }

          $db = null;
      }
      else
      {
            $response['status'] = 'error';
            $response['message'] = 'Awailability Time not matching';
            echo json_encode($response);
      }
  }
  else {
          $response['status'] = 'error';
          $response['message'] = 'Error in validating data';
          echo json_encode($response);
      }

  } catch(PDOException $e) {
      $response['status'] = 'error';
      $response['message'] = $e->getMessage();
      echo json_encode($response);
  }
});

/**
 * Track Delivery boy
 */
$app->get('/trackOrderByLink/:order_id', function($order_id) {

    $response = array();

    try{
     
        if ( isset($order_id) && !empty($order_id) && checktableIdValid($order_id) ) {
            $deliveryboy_data = getDeliveryBoyPersonalData($order_id);

            if( isset($deliveryboy_data['id']) && $deliveryboy_data['id'] > 0 ){

                $db = getDB();
                $sql = "SELECT * FROM delevering_user_markers WHERE user_id=:user_id";
                $stmt = $db->prepare($sql);
                $stmt->bindParam("user_id", $deliveryboy_data['id'], PDO::PARAM_INT);
                $stmt->execute();
                $deliveryboy_cordinates = $stmt->fetch(PDO::FETCH_OBJ);

                $order_sql = "SELECT * FROM delevering_order WHERE id=:id";
                $orDadd = $db->prepare($order_sql);
                $orDadd->bindParam("id", $order_id, PDO::PARAM_INT);
                $orDadd->execute();
                $order_cordinates = $orDadd->fetch(PDO::FETCH_OBJ);

                if ( !empty($deliveryboy_cordinates) && sizeof($deliveryboy_cordinates) > 0 ) {
                    $response['deliveryBoyData'] = $deliveryboy_cordinates;
                    $response['orderDetailData'] = $order_cordinates;
                    // Getting Order Pickup & Destination Address

                    $response['status'] = 'success';
                    $response['message'] = 'Delivery boy data get successfully';
                    echo json_encode($response);
                }
            }else {
                $response['status'] = 'error';
                $response['message'] = 'Your order hasnt been picked up yet!!!. You will get a SMS as soon as your order is picked up';
                echo json_encode($response);
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Error in getting data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Admin dashboard API
 */
/*== Get all type user ==*/
$app->get('/adminGetAllUsers/:user_id', function($user_id){

    $response = array();

    try {
 
        if ( $user_id > 0 && $_SESSION["userID"] == apiToken($user_id) && is_admin($user_id) ) {

            $deliveryman_data = getUserByUserRole();
            $vendors_data = getUserByUserRole('vendor');
            
            if( !empty($vendors_data) || !empty($deliveryman_data) ) {
                $response['deliveryBoys'] = $deliveryman_data;
                $response['vendors'] = $vendors_data;
                $response['status'] = 'success';
                $response['message'] = 'data get successfully';
                echo json_encode($response);
            }else {
                $response['status'] = 'error';
                $response['message'] = 'no data found';
                echo json_encode($response);
            }
        }

    }catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/*== Get order Quote ==*/
$app->post('/insertUpdateApiKey', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->userID;
    $api_key = $request_data->apiKey;

    if( $user_id > 0 && $api_key !== '' ){
        
        $db = getDB();

        $sql = "UPDATE ".table_prifix."_usermeta SET api_key=:api_key WHERE user_id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("api_key", $api_key, PDO::PARAM_STR);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

        if($stmt->execute()){
            $response['status'] = 'success';
            $response['message'] = 'Api key save successfully';
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in saveing Api key';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== Send Api key to email ==*/
$app->post('/sendApiKeyToEmail', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $api_key = $request_data->apiKey;
    $user_email = $request_data->userEmail;

    if( $api_key !== '' && $user_email !== '' ){
        
        $subject = 'Your api key - Dialadelivery';
        $custom_message = "Hi, <br /></br>This is your api key <b>$api_key</b>";
		$result = true;
        //$result = delivery_mail_template($user_email, $subject, '', $custom_message);

        if($result){
            $response['status'] = 'success';
            $response['message'] = 'Api key send successfully';
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in sending Api key';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== Send email to vendor ==*/
$app->post('/sendMailVendor', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $email_data = $request_data->emailData;

    if( $email_data->email !== '' && $email_data->subject !== '' && $email_data->message !== '' ){
        
        $subject = $email_data->subject.' - Dialadelivery';
        $custom_message = $email_data->message;
		$result = true;
        //$result = delivery_mail_template($email_data->email, $subject, '', $custom_message);

        if($result){
            $response['status'] = 'success';
            $response['message'] = 'Mail successfully send';
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in sending Api key';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== Edit vendor status and Km/AUD and commistion ==*/
$app->post('/saveVendorByAdmin', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $vendor_data = $request_data->vendorData;

    if( !empty($vendor_data) && sizeof($vendor_data) > 0 ){

        $db = getDB();
        $vendor_per_km_charge = isset($vendor_data->vendor_per_km_charge) ? $vendor_data->vendor_per_km_charge : '';
        $deliveryman_per_km_charge = isset($vendor_data->deliveryman_per_km_charge) ? $vendor_data->deliveryman_per_km_charge : '';
        $commission = isset($vendor_data->commission) ? $vendor_data->commission : '';

        if( checkUserMetaExist($vendor_data->id) ){
            $sql = "UPDATE ".table_prifix."_usermeta SET commission=:commission,vendor_per_km_charge=:vendor_per_km_charge,deliveryman_per_km_charge=:deliveryman_per_km_charge WHERE user_id=:user_id";
        }else {
            $sql = "INSERT INTO ".table_prifix."_usermeta (user_id, commission,vendor_per_km_charge,deliveryman_per_km_charge) VALUES (:user_id,:commission,:vendor_per_km_charge,:deliveryman_per_km_charge)";
        }

        $stmt = $db->prepare($sql);
        $stmt->bindParam("commission", $commission, PDO::PARAM_STR);
        $stmt->bindParam("vendor_per_km_charge", $vendor_per_km_charge, PDO::PARAM_STR);
        $stmt->bindParam("deliveryman_per_km_charge", $deliveryman_per_km_charge, PDO::PARAM_STR);
        $stmt->bindParam("user_id", $vendor_data->id, PDO::PARAM_INT);

        if( isset($vendor_data->angularStatus) ){
            $status = ($vendor_data->angularStatus) ? 1 : 0;
            $sql1 = "UPDATE ".table_prifix."_user SET status=:status WHERE id=:user_id";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("status", $status, PDO::PARAM_INT);
            $stmt1->bindParam("user_id", $vendor_data->user_id, PDO::PARAM_INT);
            $stmt1->execute();
        }

        if($stmt->execute()){
            $response['status'] = 'success';
            $response['message'] = 'user data update successfully';
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in saveing user data';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== Save deliveryman release amount ==*/
$app->post('/saveRealeaseAmount', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $deliveryman_data = $request_data->deliveryManData;

    if( !empty($deliveryman_data) && sizeof($deliveryman_data) > 0 && $deliveryman_data->id ){

        $db = getDB();
        $min_release_amt = isset($deliveryman_data->min_release_amt) ? $deliveryman_data->min_release_amt : '';

        if( checkUserMetaExist($deliveryman_data->id) ) {
            $sql = "UPDATE ".table_prifix."_usermeta SET min_release_amt=:min_release_amt WHERE user_id=:user_id";
        }else{
           $sql = "INSERT INTO ".table_prifix."_usermeta (user_id, min_release_amt) VALUES (:user_id,:min_release_amt)";
        }

        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $deliveryman_data->id, PDO::PARAM_INT);
        $stmt->bindParam("min_release_amt", $min_release_amt, PDO::PARAM_STR);

        if($stmt->execute()){
            $response['status'] = 'success';
            $response['message'] = 'user data update successfully';
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in saveing user data';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== Save deliveryman release amount ==*/
$app->post('/changeDeliveryManUserStatus', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $deliveryman_data = $request_data->deliveryManData;

    if( !empty($deliveryman_data) && sizeof($deliveryman_data) > 0 && $deliveryman_data->id ){
        
        $status = ($deliveryman_data->angularStatus) ? 1 : 0;
        $db = getDB();
        $sql = "UPDATE ".table_prifix."_user SET status=:status WHERE id=:user_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $deliveryman_data->id, PDO::PARAM_INT);
        $stmt->bindParam("status", $status, PDO::PARAM_INT);

        if($stmt->execute()){
            
            $subject = 'Your account activation status - Dialadelivery';

            if($deliveryman_data->angularStatus){
                $custom_message = 'Your account now active, please login with your email id';
            }else {
                $custom_message = 'Your account temporary suspended, Please contact to administrator';
            }

           // delivery_mail_template($deliveryman_data->email, $subject, '', $custom_message);

            $response['status'] = 'success';
            $response['message'] = 'Status changed successfully';

        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in saveing user data';
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Validatation error';
    }

    echo json_encode($response);
    die();
});

/*== getDelerymanOrderAccountData ==*/
$app->get('/getDelerymanOrderAccountData/:user_id', function($user_id){

    $response = array();

    try {

        if ( $user_id > 0 
            && ( $_SESSION["userID"] == apiToken($user_id) || is_deliveryman($user_id) ) 
            && is_deliveryman($user_id) ) {

            $db = getDB();
            $sql = "SELECT order.id,order.order_title,order.delivery_agent_id,order.order_status,order.total_distance,order.deliver_time,order.deliveryman_amount FROM ".table_prifix."_order as `order` WHERE order.delivery_agent_id=:user_id AND order.order_status='complete'";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt->execute();
            $deliveryman_account_data = (array)$stmt->fetchAll(PDO::FETCH_OBJ);

            $sql1 = "SELECT usermeta.release_remainig, usermeta.released_amount FROM ".table_prifix."_usermeta as `usermeta` WHERE usermeta.user_id=:user_id";

            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);
            $stmt1->execute();
            $realease_amount_data = (array)$stmt1->fetch(PDO::FETCH_OBJ);

            if( !empty($deliveryman_account_data) 
                && sizeof($deliveryman_account_data) > 0
                && !empty($realease_amount_data) 
                && sizeof($realease_amount_data) > 0 ){

                foreach ($deliveryman_account_data as $key => $value) {
                    $delivery_time = $value->deliver_time;
                    $value->deliver_time = !empty($delivery_time) ? date('d M Y H:i:s', $delivery_time) : '';
                }

                $response['deliveryBoyOrderData'] = $deliveryman_account_data;
                $response['releaseAmountData'] = $realease_amount_data;
                $response['status'] = 'success';
                $response['message'] = 'data get successfully';
                echo json_encode($response);
            }else {
                $response['status'] = 'error';
                $response['message'] = 'no data found';
                echo json_encode($response);
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = 'no data found';
            echo json_encode($response);
        }

    }catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/*== get Deleryman Order AccountData Requested BY admin ==*/
$app->get('/getDelerymanOrderAccountDataByAdmin/:user_id', function($user_id){

    $response = array();

    try {
 
        if ( $user_id > 0 && $_SESSION["userID"] == apiToken($user_id) && is_admin($user_id) ) {       

            $db = getDB();
            $sql = "SELECT user.id,user.first_name,user.last_name,user.email,user.contact_no,user.display_name,user.user_role, usermeta.released_amount,usermeta.release_remainig  FROM delevering_user as user Left Join delevering_usermeta as usermeta ON usermeta.user_id = user.id WHERE user.user_role='deliveryman'ORDER BY user.first_name ASC";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $deliveryman_account_data = (array)$stmt->fetchAll(PDO::FETCH_OBJ);

            if( !empty($deliveryman_account_data) 
                && sizeof($deliveryman_account_data) > 0 ){
                $response['deliveryBoyData'] = $deliveryman_account_data;
                $response['status'] = 'success';
                $response['message'] = 'data get successfully';
                echo json_encode($response);
            }else {
                $response['status'] = 'error';
                $response['message'] = 'no data found';
                echo json_encode($response);
            }
        }

    }catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Vendor public API
 */
/*== Get order Quote ==*/
$app->post('/getOrderQuote', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $header_key = $request->headers->get('Key');

    if(isset($request_data->pickupLocation) && isset($request_data->dropLocation) ){
        $pickup_location = $request_data->pickupLocation;
        $drop_location = $request_data->dropLocation;
    }else {
        $response['status'] = 'error';
        $response['message'] = 'location not find!';
        echo json_encode($response);
        die();
    }

    if ( isValidApi($header_key) && $pickup_location !== '' && $drop_location !== '' ) {
        
        $vendorData = getUserCommissionAndChargeByApiKey($header_key);
        $distance_arr = get_distance_and_duration($pickup_location, $drop_location);

        if( !empty($distance_arr) && sizeof($distance_arr) > 0 ){
            $distance = $distance_arr['distance'];
            if( !empty($vendorData) && sizeof($vendorData) > 0 ){
                $vendor_commision = $vendorData['commission'];
                $per_km_charge = $vendorData['vendor_per_km_charge'];

                if(/*$vendor_commision > 0 &&*/ $per_km_charge > 0 ){
                    $order_quote = get_order_quote($vendor_commision, $per_km_charge, $distance);
                    if( $order_quote > 0 ){
                        $response['quotePrice'] = $order_quote;
                        $response['estimateDistance'] = $distance_arr['distanceText'];
                        $response['estimateDuration'] = $distance_arr['duration'];
                        $response['pickupLocation'] = $request_data->pickupLocation;
                        $response['dropLocation'] = $request_data->dropLocation;
                        $response['estimateDuration'] = $distance_arr['duration'];
                        $response['status'] = 'success';
                        $response['message'] = 'Order quote get successfully';
                        echo json_encode($response);
                        die();    
                    }else {
                        $response['status'] = 'error';
                        $response['message'] = 'error in geting order quote';
                        echo json_encode($response);
                        die();    
                    }
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'error in geting commission and KM/Charge';
                    echo json_encode($response);
                    die(); 
                }
            }else {
                $response['status'] = 'error';
                $response['message'] = 'error in geting vendor';
                echo json_encode($response); 
                die();
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in validating addressess';
            echo json_encode($response);
            die();
        }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'error in validating data';
        echo json_encode($response);
        die();
    }
});

/*== Create Order by api ==*/
/*$app->post('/apiCreateOrder', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $header_key = $request->headers->get('Key');

    if( isset($request_data->pickupLocation) 
        && isset($request_data->dropLocation) 
        && $request_data->pickupLocation !== ''
        && $request_data->dropLocation !== '' ){
        $pickup_location = $request_data->pickupLocation;
        $drop_location = $request_data->dropLocation;
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Error in getting pickup and drop location!';
        echo json_encode($response);
        die();
    }
    if( isset($request_data->customerName) 
        && isset($request_data->customerContact) 
        && $request_data->customerName !== ''
        && $request_data->customerContact !== '' ){
        $enduser_name = $request_data->customerName;
        $enduser_contact_no = $request_data->customerContact;
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Error in getting customer details!';
        echo json_encode($response);
        die();
    }

    if( isset($request_data->itemCount) 
        && isset($request_data->orderId)
        && $request_data->itemCount > 0
        && $request_data->orderId > 0 ){
        $item_count = $request_data->itemCount;
        $vendor_orderid = $request_data->orderId;
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Error in getting order id and item count!';
        echo json_encode($response);
        die();
    }

    if( isValidApi($header_key) || $header_key !==  ''){
       if( is_order_exist($header_key, $vendor_orderid) ){
            $response['status'] = 'error';
            $response['message'] = 'Order already exist!!';
            echo json_encode($response);
            die();
       }
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Invalid or not foung api key';
        echo json_encode($response);
        die();
    }

    $delivery_type = isset($request_data->deliveryType) ? $request_data->deliveryType : 'dialadelivery';

    if( isValidApi($header_key) &&
        $request_data->pickupLocation !== '' && 
        $request_data->dropLocation !== '' && 
        $request_data->customerName !== '' && 
        $request_data->customerContact !== '' && 
        $request_data->itemCount > 0 && 
        $request_data->orderId > 0 ) {

        $date = new DateTime();
        $timedata = $date->getTimestamp();

        $status = orderStatus(1);
        $db = getDB();

        $order_title = 'Order#'.$request_data->orderId;

        $pickUplocation = $request_data->pickupLocation;
        $droplocation =  $request_data->dropLocation;
        $vendor_api_order_id = $request_data->orderId;

        $drop_date = isset($request_data->shipDate) ? $request_data->shipDate : '';
        $drop_time = isset($request_data->shipTime) ? $request_data->shipTime : '';
        $ship_total = isset($request_data->shipTotal) ? $request_data->shipTotal : '';

        $pickup_lat_long = get_lat_long_form_address($pickUplocation);
        $drop_lat_long = get_lat_long_form_address($droplocation);
        
        $pickup_lat = !empty($pickup_lat_long) ? $pickup_lat_long['lat'] : '';
        $pickup_lng = !empty($pickup_lat_long) ? $pickup_lat_long['lng'] : '';
        $drop_lat = !empty($drop_lat_long) ? $drop_lat_long['lat'] : '';
        $drop_lng = !empty($drop_lat_long) ? $drop_lat_long['lng'] : '';

        $enduser_name = ($request_data->customerName !== '') ? $request_data->customerName : '';
        $enduser_contact_no = ($request_data->customerContact !== '') ? $request_data->customerContact : '';
        $vendorData = getUserCommissionAndChargeByApiKey($header_key);

        $distance_arr = get_distance_and_duration($pickUplocation, $droplocation);
        $distance = isset($distance_arr['distance']) ? $distance_arr['distance'] : 0;

        if($distance < 5){
            $amount_with_Commission = ( isset($vendorData['vendor_per_km_charge']) && $vendorData['vendor_per_km_charge'] > 0 ) ? 5*$vendorData['vendor_per_km_charge'] : 0;
            $deliveryman_amount = ( isset($vendorData['deliveryman_per_km_charge']) && $vendorData['deliveryman_per_km_charge'] > 0 ) ? 5*$vendorData['deliveryman_per_km_charge'] : 0;
        }else {
            $amount_with_Commission = ( isset($vendorData['vendor_per_km_charge']) && $vendorData['vendor_per_km_charge'] > 0 ) ? $distance*$vendorData['vendor_per_km_charge'] : 0;
            $deliveryman_amount = ( isset($vendorData['deliveryman_per_km_charge']) && $vendorData['deliveryman_per_km_charge'] > 0 ) ? $distance*$vendorData['deliveryman_per_km_charge'] : 0;
        }


        if( !empty($vendorData['user_id']) && isset($vendorData['user_id']) ){
            
            $user_id = $vendorData['user_id'];
            $pickup_unique_code = sprintf("%06d", mt_rand(1, 999999));

            $sql = "INSERT INTO ".table_prifix."_order (`order_date`,`order_title`,`status_modified`,`pickup_address`,`deliver_address`,`vendor_id`,`pickup_lat`,`pickup_long`,`drop_lat`,`drop_long`,`order_status`,`end_user_name`,`end_user_contact_no`,`pickup_unique_code`,`vendor_api_order_id`,`amount_with_Commission`,`deliveryman_amount`,`total_distance`,`ship_total`,`item_count`,`delivery_type`) VALUES(:current_timestamp,:order_title,:current_timestamp,:pickup_location,:drop_location,:vendor_id,:pickup_lat,:pickup_long,:drop_lat,:drop_lng,:order_status,:enduser_name,:enduser_contact_no,:pickup_unique_code,:vendor_api_order_id,:amount_with_Commission,:deliveryman_amount,:distance,:ship_total,:item_count,:delivery_type)";

            $stmt = $db->prepare($sql);

            $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);
            $stmt->bindParam("order_title", $order_title, PDO::PARAM_STR);
            $stmt->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
            $stmt->bindParam("pickup_location", $pickUplocation, PDO::PARAM_STR);
            $stmt->bindParam("drop_location", $droplocation, PDO::PARAM_STR);
            $stmt->bindParam("pickup_lat", $pickup_lat, PDO::PARAM_INT);
            $stmt->bindParam("pickup_long", $pickup_lng, PDO::PARAM_INT);
            $stmt->bindParam("drop_lat", $drop_lat, PDO::PARAM_INT);
            $stmt->bindParam("drop_lng", $drop_lng, PDO::PARAM_INT);
            $stmt->bindParam("order_status", $status, PDO::PARAM_STR);
            $stmt->bindParam("pickup_unique_code", $pickup_unique_code, PDO::PARAM_INT);
            $stmt->bindParam("vendor_api_order_id", $vendor_api_order_id, PDO::PARAM_INT);
            $stmt->bindParam("amount_with_Commission", $amount_with_Commission, PDO::PARAM_STR);
            $stmt->bindParam("deliveryman_amount", $deliveryman_amount, PDO::PARAM_STR);
            $stmt->bindParam("enduser_name", $enduser_name, PDO::PARAM_STR);
            $stmt->bindParam("enduser_contact_no", $enduser_contact_no, PDO::PARAM_STR);
            $stmt->bindParam("distance", $distance, PDO::PARAM_STR);
            $stmt->bindParam("ship_total", $ship_total, PDO::PARAM_STR);
            $stmt->bindParam("item_count", $item_count, PDO::PARAM_INT);
            $stmt->bindParam("delivery_type", $delivery_type, PDO::PARAM_STR);

            $deliveryboy_data = getDeliverymanByDeliveryMode($delivery_type, $user_id);
            $verndor_data = getAllUserData($user_id);

            if( $stmt->execute() && !empty($deliveryboy_data) && sizeof($deliveryboy_data) > 0 ){

                $orderID = $db->lastInsertId();
                foreach ($deliveryboy_data as $key => $deliveryboy) {
                    if( is_deliveryman_active($deliveryboy->id) ){

                        $from = '';
                        $subject = 'New '.$order_title.' request - Dialadelivery';
                        $custom_message = '';
                        $custom_message.='We are happy to inform that we got a new order with following details:<br/><br/> Pickup Address - <b>'.$pickUplocation.'</b><br/>Delivery Address - <b>'.$droplocation.'</b></br></br>';
                        $custom_message.= '<br/><b>Note:-</b> Hurry up! If you are interested please accept by <a href="'.SITE_URL.'/#!/order-requests">Clicking here</a>.';
                        $user_email = $deliveryboy->email;
                        $result = "somekey";
                        //$result = delivery_mail_template($user_email, $subject, $from, $custom_message);

                        if($deliveryboy->contact_no > 0 ){
                            $message = 'New '.$order_title.' request from delivering. Order pickup location is: '.$pickUplocation.' and drop location is: '.$droplocation;
                            //send_message($deliveryboy->contact_no, $message);
                        }
                    }
                }

                if ($result) {
                    $traking_url = SITE_URL.'/custom/track-order.php?order='.$orderID;
                    $short_traking_url = short_url($traking_url);
                    $response['status'] = 'success';
                    $response['orderId'] = $orderID;
                    $response['orderPickupCode'] = $pickup_unique_code;
                    $response['trackingLink'] = $short_traking_url;
                    $response['message'] = 'order successfully created';
                    echo json_encode($response);

                }else{
                    $response['status'] = 'error';
                    $response['message'] = 'Error in request sent to delivery boys.';
                    echo json_encode($response);
                }

            }else {
                $response['status'] = 'error';
                $response['message'] = 'Error in Creating order, Because deliveryboy not found';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'error in getting vendor data';
            echo json_encode($response);
            die();
        }

    }else {
        $response['status'] = 'error';
        $response['message'] = 'error in validatin data';
        echo json_encode($response);
        die();
    }
});*/

$app->post('/apiCreateOrder', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());

    $user_id = $request_data->user_id;
    $pickup_unique_code = sprintf("%06d", mt_rand(1, 999999));
    $date = new DateTime();
    $timedata = $date->getTimestamp();

    $user_id = $request_data->user_id;
    $order_title = $request_data->order_title;
    $pickUplocation = $request_data->pickup_address;
    $droplocation = $request_data->delivery_address;

    $pickup_lat_long = get_lat_long_form_address($pickUplocation);
    $drop_lat_long = get_lat_long_form_address($droplocation);
    
    $pickup_lat = !empty($pickup_lat_long) ? $pickup_lat_long['lat'] : '';
    $pickup_lng = !empty($pickup_lat_long) ? $pickup_lat_long['lng'] : '';
    $drop_lat = !empty($drop_lat_long) ? $drop_lat_long['lat'] : '';
    $drop_lng = !empty($drop_lat_long) ? $drop_lat_long['lng'] : '';

    /*$pickup_lat = $request_data->pickup_lat;
    $pickup_lng = $request_data->pickup_long;
    $drop_lat = $request_data->drop_lat;
    $drop_lng = $request_data->drop_long;*/

    $status = orderStatus(1);
    $enduser_name = $request_data->customer_name;
    $enduser_contact_no = $request_data->customer_contact;
    $distance = $request_data->distance;
    $ship_total = isset($request_data->ship_total)?$request_data->ship_total:0;
    $item_count = isset($request_data->item_count)?$request_data->item_count:0;
    // $delivery_type = $request_data->delivery_type;
    $delivery_type = "inhouse";

    if( isset($request_data->customer_name) 
        && isset($request_data->customer_contact) 
        && $request_data->customer_name !== ''
        && $request_data->customer_contact !== '' ){
        $enduser_name = $request_data->customer_name;
        $enduser_contact_no = $request_data->customer_contact;
    }else {
        $response['status'] = 'error';
        $response['message'] = 'Error in getting customer details!';
        echo json_encode($response);
        die();
    }
   

    $db = getDB();
    // $sql = "INSERT INTO ".table_prifix."_order (`order_date`,`order_title`,`status_modified`,`pickup_address`,`deliver_address`,`vendor_id`,`pickup_lat`,`pickup_long`,`drop_lat`,`drop_long`,`order_status`,`end_user_name`,`end_user_contact_no`,`pickup_unique_code`,`vendor_api_order_id`,`amount_with_Commission`,`deliveryman_amount`,`total_distance`,`ship_total`,`item_count`,`delivery_type`) VALUES(:current_timestamp,:order_title,:current_timestamp,:pickup_location,:drop_location,:vendor_id,:pickup_lat,:pickup_long,:drop_lat,:drop_lng,:order_status,:enduser_name,:enduser_contact_no,:pickup_unique_code,:vendor_api_order_id,:amount_with_Commission,:deliveryman_amount,:distance,:ship_total,:item_count,:delivery_type)";
    $sql = "INSERT INTO ".table_prifix."_order (`order_date`,`order_title`,`status_modified`,`pickup_address`,`deliver_address`,`vendor_id`,`pickup_lat`,`pickup_long`,`drop_lat`,`drop_long`,`order_status`,`end_user_name`,`end_user_contact_no`,`pickup_unique_code`,`total_distance`,`ship_total`,`item_count`,`delivery_type`) VALUES(:current_timestamp,:order_title,:current_timestamp,:pickup_location,:drop_location,:vendor_id,:pickup_lat,:pickup_long,:drop_lat,:drop_lng,:order_status,:enduser_name,:enduser_contact_no,:pickup_unique_code,:distance,:ship_total,:item_count,:delivery_type)";

    $stmt = $db->prepare($sql);

    $stmt->bindParam("vendor_id", $user_id, PDO::PARAM_INT);
    $stmt->bindParam("order_title", $order_title, PDO::PARAM_STR);
    $stmt->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
    $stmt->bindParam("pickup_location", $pickUplocation, PDO::PARAM_STR);
    $stmt->bindParam("drop_location", $droplocation, PDO::PARAM_STR);
    $stmt->bindParam("pickup_lat", $pickup_lat, PDO::PARAM_INT);
    $stmt->bindParam("pickup_long", $pickup_lng, PDO::PARAM_INT);
    $stmt->bindParam("drop_lat", $drop_lat, PDO::PARAM_INT);
    $stmt->bindParam("drop_lng", $drop_lng, PDO::PARAM_INT);
    $stmt->bindParam("order_status", $status, PDO::PARAM_STR);
    $stmt->bindParam("pickup_unique_code", $pickup_unique_code, PDO::PARAM_INT);
    // $stmt->bindParam("vendor_api_order_id", $vendor_api_order_id, PDO::PARAM_INT);
    // $stmt->bindParam("amount_with_Commission", $amount_with_Commission, PDO::PARAM_STR);
    // $stmt->bindParam("deliveryman_amount", $deliveryman_amount, PDO::PARAM_STR);
    $stmt->bindParam("enduser_name", $enduser_name, PDO::PARAM_STR);
    $stmt->bindParam("enduser_contact_no", $enduser_contact_no, PDO::PARAM_STR);
    $stmt->bindParam("distance", $distance, PDO::PARAM_STR);
    $stmt->bindParam("ship_total", $ship_total, PDO::PARAM_STR);
    $stmt->bindParam("item_count", $item_count, PDO::PARAM_INT);
    $stmt->bindParam("delivery_type", $delivery_type, PDO::PARAM_STR);
    if( !empty($request_data->item_detail) && sizeof($request_data->item_detail) > 0 && $stmt->execute() ){

        $orderID = $db->lastInsertId();
        // if( !empty($request_data->item_detail) && sizeof($request_data->item_detail) > 0 ){

            $sql2 = "INSERT INTO `delevering_order_items` (`order_id`, `item_name`, `item_image`, `item_type`, `item_quantity`, `item_value`, `item_height`, `item_width`, `item_weight`, `item_status`) VALUES (:order_id,:item_name,:item_image,:item_type,:item_quantity,:item_value,:item_height,:item_width,:item_weight,:item_status)";

            $stmt2 = $db->prepare($sql2);

            foreach ($request_data->item_detail as $item_key => $itemvalue) {

                $stmt2->bindValue('order_id', $orderID);
                $stmt2->bindValue('item_name', $itemvalue->item_name);
                $stmt2->bindValue('item_image', null);
                $stmt2->bindValue('item_type', $itemvalue->item_type);
                $stmt2->bindValue('item_quantity', $itemvalue->item_quantity);
                $stmt2->bindValue('item_value', $itemvalue->item_value);
                $stmt2->bindValue('item_width', $itemvalue->item_weight);
                $stmt2->bindValue('item_height', null);
                $stmt2->bindValue('item_weight', null);
                $stmt2->bindValue('item_status', 1);
                $stmt2->execute();
            }
            $response['status'] = 'success';
            $response['message'] = 'Order added successfully';
            echo json_encode($response);
        // }
    }
    else
    {
        $response['status'] = 'error';
        $response['message'] = 'Some error while inserting data';
        echo json_encode($response);
    }
});


/**
 * Get all User Order
 */
$app->get('/apiGetOrder/:order_id/:user_id', function($order_id, $user_id) {

    try{

        if ( $user_id > 0 /*&& isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id)*/ ) {

            $sql = "SELECT * FROM ".table_prifix."_order LEFT JOIN ".table_prifix."_order_items ON delevering_order.id = delevering_order_items.order_id WHERE ( vendor_id=:user_id AND delevering_order.id=:order_id) OR ( delivery_agent_id=:user_id AND delevering_order.id=:order_id)";

            $db = getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() ){
                $userOrders = array();
                $userOrders = $stmt->fetchAll(PDO::FETCH_OBJ);


                $order_data = array();
                if( !empty($userOrders) && sizeof($userOrders) > 0 ){
                    foreach ($userOrders as $key => $value) {
                        $temp = $itemmeta = array();
                        if($value->id > 0 ){
                            if( in_array($value->id, $order_data) ){
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data['orderItems'][] = $itemmeta;
                            }else {

                                $temp['order_id'] = $value->id;
                                $temp['vendor_id'] = $value->vendor_id;
                                $temp['order_title'] = $value->order_title;
                                $temp['order_note'] = $value->order_note;
                                $temp['pickup_address'] = $value->pickup_address;
                                $temp['deliver_address'] = $value->deliver_address;
                                $temp['pickup_time'] = !empty($value->pickup_time) ? date('d M Y H:i:s', $value->pickup_time) : '';
                                $temp['deliver_time'] = !empty($value->deliver_time) ? date('d M Y H:i:s', $value->deliver_time) : '';
                                $temp['order_date'] = isset($value->order_date) ? date('d M Y H:i:s', $value->order_date) : '';
                                $temp['status_modified'] = $value->status_modified;
                                $temp['delivery_agent_id'] = ($value->delivery_agent_id) > 0 ? $value->delivery_agent_id : '';                                
                                $temp['order_status'] = $value->order_status;
                                $temp['pickup_unique_code'] = $value->pickup_unique_code;
                                $temp['end_user_name'] = $value->end_user_name;
                                $temp['end_user_contact_no'] = $value->end_user_contact_no;
                                $temp['ship_total'] = $value->ship_total;
                                $temp['item_count'] = $value->item_count;

                                if( $value->delivery_agent_id > 0 ){
                                    $user_data =getAllUserData($value->delivery_agent_id);
                                    $temp['delivery_agent_name'] = $user_data['first_name'];
                                    $temp['delivery_agent_number'] = $user_data['contact_no'];
                                }else {
                                    $temp['delivery_agent_name'] = '';
                                    $temp['delivery_agent_number'] = '';
                                }

                                $order_data = $temp;
                                $itemmeta['item_name'] = $value->item_name;
                                $itemmeta['item_image'] = $value->item_image;
                                $itemmeta['item_type'] = $value->item_type;
                                $itemmeta['item_quantity'] = $value->item_quantity;
                                $itemmeta['item_value'] = $value->item_value;
                                $itemmeta['item_height'] = $value->item_height;
                                $itemmeta['item_width'] = $value->item_width;
                                $itemmeta['item_weight'] = $value->item_weight;
                                $itemmeta['item_status'] = $value->item_status;
                                $order_data['orderItems'][] = $itemmeta;
                            }
                        }
                    }

                    $response['status'] = 'success';
                    $response['message'] = 'User orders get successfully';
                    $response['userOrders'] = $order_data;
                    // $response['userOrders'] = $userOrders;
                    echo json_encode($response);
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'No order data found along with this order id';
                    echo json_encode($response);
                }
            } else {
                $response['status'] = 'error';
                $response['message'] = $stmt1->error;
                echo json_encode($response);
            }

            $db = null;
        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Vendor Sign Up API for YFO
 */
$app->post('/apiVendorSignup', function() {
    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    // echo "<pre>"; print_r($user_data);exit;
    // $data = $user_data->userData;
    // date_default_timezone_set("Asia/Kolkata");
    // $data = $_POST['userData'];

    $fname = $data->fname;
    $lname = $data->lname;
    $email = $data->email;
    $phone_no = $data->phone;
    $dialingNumber = isset($data->dialingNumber) ? $data->dialingNumber : null;
    $gender = isset($data->gender) ? $data->gender : "M";
    $password = $data->password;
    // $rePassword = $data->rePassword;
    $requestFor = "vendor";

    /*$dateofbirth = !empty($data->dateofbirth) ? $data->dateofbirth : '';
    $dobforapi = date("Y-m-d",strtotime($dateofbirth));
    // echo "<pre>"; print_r(date("Y-m-d",strtotime($dateofbirth)));exit;
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $suburbs = !empty($data->suburbs) ? json_encode($data->suburbs) : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';

    $company = !empty($data->company) ? $data->company : '';
    $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';
    $availability_slots = isset($data->availability_slots) ? json_encode($data->availability_slots) : '';
    $min_release_amt = isset($data->min_release_amt) ? $data->min_release_amt : '';
    $vendor_per_km_charge = isset($data->vendor_per_km_charge) ? $data->vendor_per_km_charge : '';
    $deliveryman_per_km_charge = isset($data->deliveryman_per_km_charge) ? $data->deliveryman_per_km_charge : '';
    $commission = isset($data->commission) ? $data->commission : '';
    $time_avalibility_from = isset($data->time_avalibility_from) ? $data->time_avalibility_from : '';
    $time_avalibility_to = isset($data->time_avalibility_to) ? $data->time_avalibility_to : '';
*/
    // $display_name = $data->fname.' '.$data->lname;
    $display_name = isset($data->display_name) ? $data->display_name : $data->fname.' '.$data->lname;
    $newfilename = isset($data->filename)? $data->filename: null;

    try {
        $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
        $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);
        if ( strlen( trim($password) ) > 0 && strlen( trim($email) ) > 0 && $email_check > 0 && $password_check > 0 /*&& $password == $rePassword*/ ) {

            $db = getDB();
            $userData = '';
            $sql = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email,PDO::PARAM_STR);
            $stmt->execute();
            $mainCount=$stmt->rowCount();

            $created=time();
            $password = md5($password);
            $status = 1;
            $phone = $dialingNumber.$phone_no;

            if($mainCount==0) {
                /*Inserting user values*/
                $date = new DateTime();
                $timedata = $date->getTimestamp();
                
                $sql1 = "INSERT INTO ".table_prifix."_user (first_name,last_name,email,contact_no,password,display_name,user_role,status,regisration_date,image_url) VALUES(:fname,:lname,:email,:contact_no,:password,:display_name,:requestFor,:status,:current_timestamp,:image_url)";

                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("status", $status, PDO::PARAM_STR);
                $stmt1->bindParam("fname", $fname, PDO::PARAM_STR);
                $stmt1->bindParam("lname", $lname, PDO::PARAM_STR);
                $stmt1->bindParam("email", $email, PDO::PARAM_STR);
                $stmt1->bindParam("password", $password, PDO::PARAM_STR);
                $stmt1->bindParam("display_name", $display_name, PDO::PARAM_STR);
                $stmt1->bindParam("requestFor", $requestFor, PDO::PARAM_STR);
                $stmt1->bindParam("contact_no", $phone, PDO::PARAM_INT);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
                $stmt1->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                $stmt1->execute();

                $userData = internalUserDetails($email);
                $response['status'] = 'success';
                $response['message'] = 'User successfully Registered';
                $response['userData'] = $userData;
                echo json_encode($response);
                /*if($requestFor == "deliveryman")
                {

                    $data = [
                        "client_ref" => "abcde",
                        "cost_centre" => "Marketing",
                        "first_name" => $fname,
                        "middle_name" => "",
                        "last_name" => $lname,
                        "single_name" => false,
                        "dob" =>  $dobforapi,
                        "birth_place" => $city,
                        "birth_state" => $state,
                        "birth_country" => 'AUS',
                        "sex" => $gender,
                        "email" => $email,
                        "resid" => [
                            "street" => $address_1.' '.$address_2,
                            "suburb" => $suburbs,
                            "state" => $state,
                            "postcode" => $zip,
                            "years" => 3,
                            "months" => 2
                        ],
                        "type" => "EMPLOYMENT",
                        "services" => ["Vevo", "Bankruptcy"],
                        "reason" => "Driver registration",
                        "result_webhook" => "http://example.com/result"
                    ];

                    // if(in_array($state,["ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA"]))
                    // {
                    //     $data['birth_state'] = $state;
                    //     $data['resid']['state'] = $state;
                    // }
                    $dataresult = nccCreateCheck($data);
                    if(is_array($dataresult))
                    {
                        if(isset($dataresult['id']))
                        {
                            $stmt1->execute();
                            $user_id = $db->lastInsertId(); 

                            $sql2 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,suburbs,city,state,zip,country,commission,deliveryman_per_km_charge,vendor_per_km_charge,min_release_amt,delivery_mode,transport_mode,inhouse_vendor,state_avalibility, dateofbirth,time_avalibility_from,time_avalibility_to,image_url, availability_slots, ncc_id, continue_url) VALUES (:user_id,:company,:address_one,:address_two,:suburbs,:city,:state,:zip,:country,:commission,:deliveryman_per_km_charge,:vendor_per_km_charge,:min_release_amt,:delivery_mode,:transport_mode,:inhouse_vendor,:state_avalibility,:dateofbirth,:time_avalibility_from,:time_avalibility_to,:image_url,:availability_slots,:nccid, :continue_url)";

                        
                            $stmt2 = $db->prepare($sql2);

                            $stmt2->bindParam("user_id", $user_id, PDO::PARAM_INT);
                            $stmt2->bindParam("company", $company, PDO::PARAM_STR);
                            $stmt2->bindParam("dateofbirth", $dateofbirth, PDO::PARAM_STR);
                            $stmt2->bindParam("address_one", $address_1, PDO::PARAM_STR);
                            $stmt2->bindParam("address_two", $address_2, PDO::PARAM_STR);
                            $stmt2->bindParam("suburbs", $suburbs, PDO::PARAM_STR);
                            $stmt2->bindParam("city", $city, PDO::PARAM_STR);
                            $stmt2->bindParam("state", $state, PDO::PARAM_STR);
                            $stmt2->bindParam("zip", $zip, PDO::PARAM_INT);
                            $stmt2->bindParam("country", $country, PDO::PARAM_STR);
                            $stmt2->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
                            $stmt2->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);
                            $stmt2->bindParam("availability_slots", $availability_slots, PDO::PARAM_STR);
                            $stmt2->bindParam("commission", $commission, PDO::PARAM_STR);
                            $stmt2->bindParam("deliveryman_per_km_charge", $deliveryman_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("vendor_per_km_charge", $vendor_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("min_release_amt", $min_release_amt, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_from", $time_avalibility_from, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_to", $time_avalibility_to, PDO::PARAM_STR);
                            $stmt2->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                            $stmt2->bindParam("nccid", $dataresult['id'], PDO::PARAM_STR);
                            $stmt2->bindParam("continue_url", $dataresult['continue_url'], PDO::PARAM_STR);
                            $stmt1_result = $stmt2->execute();
                            

                            // $apires = json_decode($dataresult);
                            // $sqlupdate = "UPDATE ".table_prifix."_usermeta SET ncc_id=:nccid, continue_url=:continue_url WHERE user_id=:user_id";
                            // $stmt1update = $db->prepare($sqlupdate);
                            // $stmt1update->bindParam("user_id", $user_id, PDO::PARAM_STR);
                            // $res = $stmt1update->execute();
                        }
                        
                        $userData = internalUserDetails($email);
                        $db = null;

                        if($userData){
                            $response['status'] = 'success';
                            $response['message'] = 'User successfully Registered';
                            $response['userData'] = $userData;
                            $response['verificationData'] = $dataresult;

                            $subject = 'Thanks for your registration';
                            $custom_message = 'Hi '.$fname.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$email.', We will inform you once your account is active.';
                            //$result = delivery_mail_template($email, $subject, '', $custom_message);
                            echo json_encode($response);
                        } else {
                            $response['status'] = 'error';
                            $response['message'] = 'Error in getting userdata';
                            echo json_encode($response);
                        }
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['message'] = 'Verification Error';
                        $response['tech'] = $dataresult;
                        echo json_encode($response);
                        
                    }
                    
                    
                    //if($phone > 0 ){
                    //    $message = "hi you OTP is 1234";
                    //    send_message($phone, $message);
                    //}
                    

                }
                else
                {
                    $userData = internalUserDetails($email);
                    $response['status'] = 'success';
                    $response['message'] = 'User successfully Registered';
                    $response['userData'] = $userData;
                    echo json_encode($response);
                }*/
            }else {
                $response['status'] = 'error';
                $response['message'] = 'User already registered, Please try again with diffrent email id';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Deliveryman Sign Up API for YFO
 */
$app->post('/apiDeliverymanSignup', function() {
    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());

    $fname = $data->fname;
    $lname = $data->lname;
    $email = $data->email;
    $phone_no = $data->phone;
    $dialingNumber = isset($data->dialingNumber) ? $data->dialingNumber : null;
    $gender = isset($data->gender) ? $data->gender : "M";
    $password = $data->password;
    $requestFor = "deliveryman";

    $dateofbirth = !empty($data->dateofbirth) ? $data->dateofbirth : '';
    $dobforapi = date("Y-m-d",strtotime($dateofbirth));

    // echo "<pre>"; print_r($dobforapi);exit;
    $address_1 = !empty($data->address_1) ? $data->address_1 : '';
    $address_2 = !empty($data->address_2) ? $data->address_2 : '';
    $suburbs = !empty($data->suburbs) ? json_encode($data->suburbs) : '';
    $city = !empty($data->city) ? $data->city : '';
    $state = !empty($data->state) ? $data->state : '';
    $zip = !empty($data->zip) ? $data->zip : '';
    $country = !empty($data->country) ? $data->country : '';

    $company = !empty($data->company) ? $data->company : '';
    // $delivery_mode = isset($data->delivery_mode) ? $data->delivery_mode : '';
    $delivery_mode = "inhouse";
    $transport_mode = isset($data->transport_mode) ? $data->transport_mode : '';
    $inhouse_vendor = isset($data->inhouse_vendor) ? $data->inhouse_vendor : '';
    $state_avalibility = isset($data->stateAvailability) ? json_encode($data->stateAvailability) : '';
    $availability_slots = isset($data->availability_slots) ? json_encode($data->availability_slots) : '';
    $min_release_amt = isset($data->min_release_amt) ? $data->min_release_amt : '';
    $vendor_per_km_charge = isset($data->vendor_per_km_charge) ? $data->vendor_per_km_charge : '';
    $deliveryman_per_km_charge = isset($data->deliveryman_per_km_charge) ? $data->deliveryman_per_km_charge : '';
    $commission = isset($data->commission) ? $data->commission : '';
    $time_avalibility_from = isset($data->time_avalibility_from) ? $data->time_avalibility_from : '';
    $time_avalibility_to = isset($data->time_avalibility_to) ? $data->time_avalibility_to : '';

    $display_name = $data->fname.' '.$data->lname;
    $newfilename = isset($data->filename)? $data->filename: null;

    try {

        $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
        $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);

        if ( strlen( trim($password) ) > 0 && strlen( trim($email) ) > 0 && $email_check > 0 && $password_check > 0 /*&& $password == $rePassword*/ ) {

            $db = getDB();
            $userData = '';
            $sql = "SELECT id FROM ".table_prifix."_user WHERE email=:email";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email,PDO::PARAM_STR);
            $stmt->execute();
            $mainCount=$stmt->rowCount();

            $created=time();
            $password = md5($password);
            $status = 1;
            $phone = $dialingNumber.$phone_no;

            if($mainCount==0) {
                /*Inserting user values*/
                $date = new DateTime();
                $timedata = $date->getTimestamp();
                
                $sql1 = "INSERT INTO ".table_prifix."_user (first_name,last_name,email,contact_no,password,display_name,user_role,status,regisration_date,image_url) VALUES(:fname,:lname,:email,:contact_no,:password,:display_name,:requestFor,:status,:current_timestamp,:image_url)";

                $stmt1 = $db->prepare($sql1);

                $stmt1->bindParam("status", $status, PDO::PARAM_STR);
                $stmt1->bindParam("fname", $fname, PDO::PARAM_STR);
                $stmt1->bindParam("lname", $lname, PDO::PARAM_STR);
                $stmt1->bindParam("email", $email, PDO::PARAM_STR);
                $stmt1->bindParam("password", $password, PDO::PARAM_STR);
                $stmt1->bindParam("display_name", $display_name, PDO::PARAM_STR);
                $stmt1->bindParam("requestFor", $requestFor, PDO::PARAM_STR);
                $stmt1->bindParam("contact_no", $phone, PDO::PARAM_INT);
                $stmt1->bindParam("current_timestamp", $timedata, PDO::PARAM_INT);
                $stmt1->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                // if($requestFor != "deliveryman")
                // {
                //     $stmt1->execute();
                // }


                // if($requestFor == "deliveryman")
                // {

                    $data = [
                        "client_ref" => "abcde",
                        "cost_centre" => "Marketing",
                        "first_name" => $fname,
                        "middle_name" => "",
                        "last_name" => $lname,
                        "single_name" => false,
                        "dob" =>  $dobforapi,
                        "birth_place" => $city,
                        "birth_state" => $state,
                        "birth_country" => 'AUS',
                        "sex" => $gender,
                        "email" => $email,
                        "resid" => [
                            "street" => $address_1.' '.$address_2,
                            "suburb" => $suburbs,
                            "state" => $state,
                            "postcode" => $zip,
                            "years" => 3,
                            "months" => 2
                        ],
                        "type" => "EMPLOYMENT",
                        "services" => ["Vevo", "Bankruptcy"],
                        "reason" => "Driver registration",
                        "result_webhook" => "http://example.com/result"
                    ];

                    /*if(in_array($state,["ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA"]))
                    {
                        $data['birth_state'] = $state;
                        $data['resid']['state'] = $state;
                    }*/
                    $dataresult = nccCreateCheck($data);
                    if(is_array($dataresult))
                    {
                        if(isset($dataresult['id']))
                        {
                            $stmt1->execute();
                            $user_id = $db->lastInsertId(); 

                            $sql2 = "INSERT INTO ".table_prifix."_usermeta (user_id,company,address_1,address_2,suburbs,city,state,zip,country,commission,deliveryman_per_km_charge,vendor_per_km_charge,min_release_amt,delivery_mode,transport_mode,inhouse_vendor,state_avalibility, dateofbirth,time_avalibility_from,time_avalibility_to,image_url, availability_slots, ncc_id, continue_url) VALUES (:user_id,:company,:address_one,:address_two,:suburbs,:city,:state,:zip,:country,:commission,:deliveryman_per_km_charge,:vendor_per_km_charge,:min_release_amt,:delivery_mode,:transport_mode,:inhouse_vendor,:state_avalibility,:dateofbirth,:time_avalibility_from,:time_avalibility_to,:image_url,:availability_slots,:nccid, :continue_url)";

                        
                            $stmt2 = $db->prepare($sql2);

                            $stmt2->bindParam("user_id", $user_id, PDO::PARAM_INT);
                            $stmt2->bindParam("company", $company, PDO::PARAM_STR);
                            $stmt2->bindParam("dateofbirth", $dobforapi, PDO::PARAM_STR);
                            $stmt2->bindParam("address_one", $address_1, PDO::PARAM_STR);
                            $stmt2->bindParam("address_two", $address_2, PDO::PARAM_STR);
                            $stmt2->bindParam("suburbs", $suburbs, PDO::PARAM_STR);
                            $stmt2->bindParam("city", $city, PDO::PARAM_STR);
                            $stmt2->bindParam("state", $state, PDO::PARAM_STR);
                            $stmt2->bindParam("zip", $zip, PDO::PARAM_INT);
                            $stmt2->bindParam("country", $country, PDO::PARAM_STR);
                            $stmt2->bindParam("delivery_mode", $delivery_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("transport_mode", $transport_mode, PDO::PARAM_STR);
                            $stmt2->bindParam("inhouse_vendor", $inhouse_vendor, PDO::PARAM_INT);
                            $stmt2->bindParam("state_avalibility", $state_avalibility, PDO::PARAM_STR);
                            $stmt2->bindParam("availability_slots", $availability_slots, PDO::PARAM_STR);
                            $stmt2->bindParam("commission", $commission, PDO::PARAM_STR);
                            $stmt2->bindParam("deliveryman_per_km_charge", $deliveryman_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("vendor_per_km_charge", $vendor_per_km_charge, PDO::PARAM_STR);
                            $stmt2->bindParam("min_release_amt", $min_release_amt, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_from", $time_avalibility_from, PDO::PARAM_STR);
                            $stmt2->bindParam("time_avalibility_to", $time_avalibility_to, PDO::PARAM_STR);
                            $stmt2->bindParam("image_url", $newfilename, PDO::PARAM_STR);
                            $stmt2->bindParam("nccid", $dataresult['id'], PDO::PARAM_STR);
                            $stmt2->bindParam("continue_url", $dataresult['continue_url'], PDO::PARAM_STR);
                            $stmt1_result = $stmt2->execute();
                            

                            // $apires = json_decode($dataresult);
                            // $sqlupdate = "UPDATE ".table_prifix."_usermeta SET ncc_id=:nccid, continue_url=:continue_url WHERE user_id=:user_id";
                            // $stmt1update = $db->prepare($sqlupdate);
                            // $stmt1update->bindParam("user_id", $user_id, PDO::PARAM_STR);
                            // $res = $stmt1update->execute();
                        }
                        
                        $userData = internalUserDetails($email);
                        $db = null;

                        if($userData){
                            $response['status'] = 'success';
                            $response['message'] = 'User successfully Registered';
                            $response['userData'] = $userData;
                            $response['verificationData'] = $dataresult;

                            $subject = 'Thanks for your registration';
                            $custom_message = 'Hi '.$fname.', <br /><br /> Thank you for registration on Dialadelivery <br /><br /> Your login email is: '.$email.', We will inform you once your account is active.';
                            //$result = delivery_mail_template($email, $subject, '', $custom_message);
                            echo json_encode($response);
                        } else {
                            $response['status'] = 'error';
                            $response['message'] = 'Error in getting userdata';
                            echo json_encode($response);
                        }
                    }
                    else
                    {
                        $response['status'] = 'error';
                        $response['message'] = 'Verification Error';
                        $response['tech'] = $dataresult;
                        echo json_encode($response);
                        
                    }
                    
                    
                    /*if($phone > 0 ){
                        $message = "hi you OTP is 1234";
                        send_message($phone, $message);
                    }*/
                    

                /*}
                else
                {
                    $userData = internalUserDetails($email);
                    $response['status'] = 'success';
                    $response['message'] = 'User successfully Registered';
                    $response['userData'] = $userData;
                    echo json_encode($response);
                }*/
            }else {
                $response['status'] = 'error';
                $response['message'] = 'User already registered, Please try again with diffrent email id';
                echo json_encode($response);
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

$app->post('/apiTestPost', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    // $request_data = $request->getBody();
    // echo "<pre>"; print_r($request_data->user_id);exit;
    $user_id = $request_data->user_id;

    if($user_id > 10){
            $response['status'] = 'success';
            $response['message'] = 'Successfully value is greater than 10';
            echo json_encode($response);
    }
    else
    {
        $response['status'] = 'error';
        $response['message'] = 'Some error value less than 10';
        echo json_encode($response);
    }
});

$app->get('/apiTestGet/:id', function($user_id){

    $response = array();

    if($user_id > 10){
            $response['status'] = 'success';
            $response['message'] = 'Successfully value is greater than 10';
            echo json_encode($response);
    }
    else
    {
        $response['status'] = 'error';
        $response['message'] = 'Some error value less than 10';
        echo json_encode($response);
    }
});




/*== Regenrate delivery code ==*/
$app->post('/apiRegenrateDeliveryCode', function(){

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $request_data = json_decode($request->getBody());
    $header_key = $request->headers->get('Key');

    if( isset($request_data->customerContact) ){
        $enduser_contact_no = filter_var($request_data->customerContact, FILTER_SANITIZE_NUMBER_INT);
    }else {
        $response['status'] = 'error';
        $response['message'] = 'End customer details not found!';
        echo json_encode($response);
        die();
    }

    if(isset($request_data->orderID) && $request_data->orderID > 0 && $request_data->customerContact !== '' ){
        
        $vendor_id = getUserIDByApiKey($header_key);
        if( $vendor_id > 0 ){
            $orders = getVendorOrders($vendor_id); 

            if( array_key_exists($request_data->orderID, $orders) ){
                
                $db = getDB();
                $num_str = sprintf("%06d", mt_rand(1, 999999));
                
                $sql = "UPDATE ".table_prifix."_order SET delivery_unique_code=:delivery_unique_code WHERE id=:order_id";

                $stmt = $db->prepare($sql);
                $stmt->bindParam("order_id", $order_id, PDO::PARAM_STR);
                $stmt->bindValue('delivery_unique_code', $num_str);

                if( $stmt->execute() ){

                    if( $enduser_contact_no !== '' ){
                        $message = 'Your Order#'.$request_data->orderID.', Delivery code is '.$num_str;
                        //send_message($enduser_contact_no, $message);
                    }

                    $response['status'] = 'success';
                    $response['message'] = 'Delivery code is send to end customer';
                    echo json_encode($response);
                    die();
                }else {
                    $response['status'] = 'error';
                    $response['message'] = 'sorry order id not belong with vendor';
                    echo json_encode($response);
                    die();
                }

            }else{
                $response['status'] = 'error';
                $response['message'] = 'sorry order id not belong with vendor';
                echo json_encode($response);
                die();
            }
        }else {
            $response['status'] = 'error';
            $response['message'] = 'vendor not found along with this api key';
            echo json_encode($response);
            die();
        }

    }else {
        $response['status'] = 'error';
        $response['message'] = 'OrderID not found!';
        echo json_encode($response);
        die();
    }
});

/**
 * Update User Work Status
 */
$app->post('/changeUserDndmode', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    
    $deliveryman_id = $user_data->userID;
    $deliveryman_data = $user_data->userData;

    try{
        
        if ( $deliveryman_id > 0 
            && $_SESSION["userID"] == apiToken($deliveryman_id) 
            && is_deliveryman($deliveryman_id) 
            && isset($deliveryman_data->dndMode) ) {

            $status = ($deliveryman_data->dndMode) ? 1 : 0;
            $db = getDB();

            $sql1 = "UPDATE ".table_prifix."_user SET dnd_mode=:status WHERE id=:user_id";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("status", $status, PDO::PARAM_INT);
            $stmt1->bindParam("user_id", $deliveryman_id, PDO::PARAM_INT);

            if( $stmt1->execute() ){
                $response['status'] = 'success';
                $response['message'] = 'User Status changed';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating user data';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Update User Work Status
 */
$app->post('/releaseAmountFordeliveryman', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
    
    $release_amt = $user_data->releaseAmount;
    $deliveryman_id = $user_data->deliverymanId;
    $admin_id = $user_data->adminId;

    try{
        
        if ( $deliveryman_id > 0 
            && $_SESSION["userID"] == apiToken($admin_id) 
            && is_deliveryman($deliveryman_id) 
            && is_admin($admin_id) ) {

            $db = getDB();

            $release_data = get_released_amount($deliveryman_id);
            print_r($release_data);
            die();

            $sql1 = "UPDATE ".table_prifix."_usermeta SET dnd_mode=:status WHERE id=:user_id";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("status", $status, PDO::PARAM_INT);
            $stmt1->bindParam("user_id", $deliveryman_id, PDO::PARAM_INT);

            if( $stmt1->execute() ){
                $response['status'] = 'success';
                $response['message'] = 'User Status changed';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in updating user data';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Delete User Account
 */
$app->post('/deleteUser', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());

    
    $user_email = $user_data->emailData;
    $user_id = $user_data->userID;
    $admin_user_id = $user_data->adminID;

    try{
        
        if ( $user_id > 0 
            && $_SESSION["userID"] == apiToken($admin_user_id)             
            && is_admin($admin_user_id) ) {
        
            $db = getDB();

            $sql = "DELETE FROM ".table_prifix."_user WHERE id=:user_id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);

            $sql1 = "DELETE FROM ".table_prifix."_usermeta WHERE user_id=:user_id";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindParam("user_id", $user_id, PDO::PARAM_INT);

            if( $stmt->execute() && $stmt1->execute() ){
                $response['status'] = 'success';
                $response['message'] = 'User Delete successfully';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in deleting user';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get all active TM list
 */
$app->get('/getActiveTransportationModes', function() {

    $response = array();

    try{
        $tmData = getTransportationModesByStatus(1);
        if(!empty($tmData) ){
            $response['status'] = 'success';
            $response['message'] = 'TM get successfully';
            $response['tmData'] = $tmData;
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'No TM found';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get TM by id
 */
$app->post('/getTMByID/:user_id', function($user_id) {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $tm_data = json_decode($request->getBody());
    $tmid = $tm_data->tmid;
    try {
        $db = getDB();
        $sql = "select * from ".table_prifix."_transportation_modes_master WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $tmid,PDO::PARAM_INT);
        $stmt->execute();
        $final_arr = (array)$stmt->fetch(PDO::FETCH_OBJ);
        if($final_arr){
            $response['status'] = 'success';
            $response['message'] = 'Transportation mode successfully updated';
            $response['tmData'] = $final_arr;
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error deleting';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get all active TM list
 */
$app->get('/adminGetAllTMs/:user_id', function($user_id) {

    $response = array();

    try{

        if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) && is_admin($user_id)) {

            $tmData = getAllTransportationModes();
            if(!empty($tmData) ){
                $response['status'] = 'success';
                $response['message'] = 'TM get successfully';
                $response['tmData'] = $tmData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No TM found';
                echo json_encode($response);
            }

        }else {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Admin add new transportation mode
 */
$app->post('/adminAddNewTM/:user_id', function($user_id) {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $tm_data = json_decode($request->getBody());
    $name = $tm_data->name;
    $status = $tm_data->status;

    try {
        $db = getDB();
        $sql = "insert into ".table_prifix."_transportation_modes_master (name, status) values(:name, :status)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("name", $name,PDO::PARAM_STR);
        $stmt->bindParam("status", $status,PDO::PARAM_INT);
        $stmt->execute();
        
        if(true){
            $response['status'] = 'success';
            $response['message'] = 'Transportation mode successfully added';
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error deleting';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Admin update transportation mode
 */
$app->post('/adminUpdateTM/:user_id', function($user_id) {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $tm_data = json_decode($request->getBody());
    $tmid = $tm_data->tmid;
    $name = $tm_data->name;
    $status = $tm_data->status;
    try {
        $db = getDB();
        $sql = "Update ".table_prifix."_transportation_modes_master set name=:name, status=:status WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("name", $name,PDO::PARAM_STR);
        $stmt->bindParam("status", $status,PDO::PARAM_INT);
        $stmt->bindParam("id", $tmid,PDO::PARAM_INT);
        $stmt->execute();
        
        if(true){
            $response['status'] = 'success';
            $response['message'] = 'Transportation mode successfully updated';
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error deleting';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Admin delete transportation mode
 */
$app->post('/adminDeleteTM/:user_id', function($user_id) {

    $response = array();

    $request = \Slim\Slim::getInstance()->request();
    $tm_data = json_decode($request->getBody());
    $tmid = $tm_data->tmid;

    try {
        $db = getDB();
        $sql = "delete FROM ".table_prifix."_transportation_modes_master WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $tmid,PDO::PARAM_INT);
        $stmt->execute();
        
        if(true){
            $response['status'] = 'success';
            $response['message'] = 'Transportation mode successfully deleted';
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Error deleting';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
 * Get all states name list
 */
$app->get('/getAllStates', function() {

    $response = array();

    try{
        
            $stateData = getAllStates();
            if(!empty($stateData) ){
                $response['status'] = 'success';
                $response['message'] = 'states get successfully';
                $response['stateData'] = $stateData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No states found';
                echo json_encode($response);
            }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get all cities from states name
 */
$app->get('/getStateCities/:state_code', function($state_code) {

    $response = array();

    try{
        if ( strlen($state_code) > 0 ) {
            $cityData = getCityNameByStateCode($state_code);
            $suburbsData = getSuburbsNameByStateCode($state_code);
            if(!empty($cityData) ){
                $response['status'] = 'success';
                $response['message'] = 'city get successfully';
                $response['cityData'] = $cityData;
                $response['suburbsData'] = $suburbsData;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No city found';
                echo json_encode($response);
            }
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get deliveryman pickup count 
 * Allowing maximum of 2 pickups to the delivery man
 */
$app->get('/getPickupCount/:userid', function($userid) {

    $response = array();

    try{
        if ( strlen($userid) > 0 ) {
            $pickupcount = getPickupCount($userid);
            // echo "<pre>"; print_r($pickupcount);exit;
            if($pickupcount >= 0){
                $response['status'] = 'success';
                $response['message'] = '';
                $response['pickupcount'] = $pickupcount;
                echo json_encode($response);
            } else {
                $response['status'] = 'error3';
                $response['message'] = '';
                echo json_encode($response);
            }
        }
        else
        {
            $response['status'] = 'error1';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error2';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get deliveryman pickup count 
 * Allowing maximum of 2 pickups to the delivery man
 */
$app->get('/getPickupCriteria/:userid/:orderid', function($userid, $orderid) {

    $response = array();

    try{
        if ( strlen($userid) > 0 ) {
            $pickupcount = getPickupCount($userid);
            if(!empty($pickupcount) ){
                $response['status'] = 'success';
                $response['message'] = '';
                $response['pickupcount'] = $pickupcount;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = '';
                echo json_encode($response);
            }
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get deliveryman pickup count 
 * Allowing maximum of 2 pickups to the delivery man
 */
$app->get('/getDeliverySuburbs/:userid', function($userid) {

    $response = array();

    try{
        if ( strlen($userid) > 0 ) {
            $delLocations = getDeliverySuburbs($userid);
            if(!empty($delLocations) ){
                $response['status'] = 'success';
                $response['message'] = '';
                $response['delLocations'] = $delLocations;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = '';
                echo json_encode($response);
            }
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

/**
* User Feedback Page
*/

$app->get('/getFeedbackDetails/:userid', function($userid) {

    $response = array();

    try{
        if ( strlen($userid) > 0 ) {
            $feedbackdetails = getFeedback($userid);
            if(!empty($feedbackdetails) ){
                $response['status'] = 'success';
                $response['message'] = '';
                $response['feedbackdetails'] = $feedbackdetails;
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = '';
                echo json_encode($response);
            }
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'Error in validating data';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});

$app->post('/feedback', function() {

    $request = \Slim\Slim::getInstance()->request();
    $user_data = json_decode($request->getBody());
	$data = $user_data->userData;
    $from_user_id = $data->from_user_id;
    $to_user_id = $data->to_user_id;
    $order_id = $data->order_id;
    $created_on = date('Y-m-d');

    $description = $data->description;
    $rating = $data->rating;
    $date = new DateTime();
   //echo "fid=$from_user_id,tid=$to_user_id,oid=$order_id,cd=$created_on,dsc=$description,rt=$rating";exit;
    
    try{

            $db = getDB();

            $sql1 = "INSERT INTO ".table_prifix."_tbl_reviews 
            (from_user_id,to_user_id,order_id,description,rating,created_on) VALUES
            (:from_user_id,:to_user_id,:order_id,:description,:rating,:created_on)";
            
            $stmt = $db->prepare($sql1);
            $stmt->bindParam("from_user_id", $from_user_id, PDO::PARAM_INT);
            $stmt->bindParam("to_user_id", $to_user_id, PDO::PARAM_INT);
            $stmt->bindParam("order_id", $order_id, PDO::PARAM_INT);
            $stmt->bindParam("description", $description, PDO::PARAM_STR);
            $stmt->bindParam("rating", $rating, PDO::PARAM_INT);
            $stmt->bindParam("created_on", $created_on, PDO::PARAM_STR);

            $stmt1_result = $stmt->execute();
            if( $stmt1_result){
                $response['status'] = 'success';
                $response['message'] = 'Feedback successfully !';
                echo json_encode($response);
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Error in user data';
                echo json_encode($response);
            }

    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }
});


/**
 * Get System generated avg per km price
 */
$app->get('/getSystemAvgPrice/:user_id', function($user_id) {

    $db = getDB();
    $response = array();

    if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {
        $userData = getAllUserData($user_id);
        if(!empty($userData) ){

            $sql = "SELECT avg(deliveryman_per_km_charge) as price from delevering_usermeta WHERE transport_mode = :mode";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("mode", $userData['transport_mode'],PDO::PARAM_STR);
            $stmt->execute();
            $avgprice = $stmt->fetch(PDO::FETCH_OBJ);
            // echo "<pre>"; print_r($avgprice->price);exit;
            $response['status'] = 'success';
            $response['message'] = 'avg price successfully';
            $response['price'] = $avgprice->price;
            echo json_encode($response);
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'invalid user';
            echo json_encode($response);
        }
    }
    else
    {
        $response['status'] = 'error';
        $response['message'] = 'invalid user';
        echo json_encode($response);
    }
    /*try{
        $tmData = getTransportationModesByStatus(1);
        if(!empty($tmData) ){
            $response['status'] = 'success';
            $response['message'] = 'TM get successfully';
            $response['tmData'] = $tmData;
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'No TM found';
            echo json_encode($response);
        }
    } catch(PDOException $e) {
        $response['status'] = 'error';
        $response['message'] = $e->getMessage();
        echo json_encode($response);
    }*/
});


$app->get('/getSystemModePrice/:user_id', function($user_id) {

    $db = getDB();
    $response = array();

    if ( strlen($user_id) > 0 && isset($_SESSION["userID"]) && $_SESSION["userID"] == apiToken($user_id) ) {
        $userData = getAllUserData($user_id);
        // echo $userData['transport_mode'];exit;
        if(!empty($userData) ){
            $sql = "SELECT count(deliveryman_per_km_charge) as count, deliveryman_per_km_charge as price FROM `delevering_usermeta` WHERE transport_mode = :mode GROUP by deliveryman_per_km_charge order by count desc";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("mode", $userData['transport_mode'],PDO::PARAM_STR);
            $stmt->execute();
            $avgprice = $stmt->fetch(PDO::FETCH_OBJ);
            $response['status'] = 'success';
            $response['message'] = 'mode price successfully';
            $response['price'] = $avgprice->price;
            echo json_encode($response);
        }
        else
        {
            $response['status'] = 'error';
            $response['message'] = 'invalid user';
            echo json_encode($response);
        }
    }
    else
    {
        $response['status'] = 'error';
        $response['message'] = 'invalid user';
        echo json_encode($response);
    }
});

/**
 * dummy api for test
 */
/*$app->get('/test_getSystemAvgPrice/:user_id', function($user_id) {

    $response['status'] = 'success';
    $response['message'] = 'avg price';
    $response['price'] = mt_rand( 0, 100 ) / 10;
    echo json_encode($response);
});*/

$app->run();