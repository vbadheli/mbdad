<?php
mediaupload();
//require '../wp-load.php';

function mediaupload(){

		$response = array('error' => false);

		$actual_filename = $_FILES['file']['name'];

		$extension = end( ( explode('.', $_FILES['file']['name']) ) );

		$file_name = md5(time()).'.'.$extension;
		
		$target_dir = $_SERVER['DOCUMENT_ROOT'].'/csd/uploads/';

		$target_file = $target_dir . basename($file_name);
		
		$file_type = pathinfo($target_file, PATHINFO_EXTENSION);
		
		if( file_exists($target_file) ) { // Check if file already exists
			$response['status'] = 'error';
			$response['message'] = 'Sorry, file already exists';
		}else{
		    if( move_uploaded_file($_FILES['file']['tmp_name'], $target_file) ) {
		    	$response['status'] = 'success';
		    	$response['message'] = 'The file '.$actual_filename.' has been successfully uploaded';
		    	$response['uploaded_file'] = $file_name;
				$infomationArr = json_decode($_REQUEST['infomationArr']);
				$infoKey = $_REQUEST['typeKey'];
				
				foreach ($infomationArr as $data) {
					if($data->key == $infoKey){
						if(empty($data->value)){
							$data->value = $file_name;	
						}
					}					
				}

				if(!empty($infomationArr)){
					$informationJson = json_encode($infomationArr);
					//update_option( 'csd_information_data', $informationJson );
				}	

			}else{
		    	$response['status'] = 'error';
		    	$response['message'] = 'Sorry, there was an error uploading your file';
		    }
		}

		echo json_encode($response);
		exit();
	}
?>