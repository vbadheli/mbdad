<!DOCTYPE html>
<html ng-app="myApp" ng-controller="trackerController"> 
<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="../assets/css/angular-material.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../assets/css/custom-style.css">
    <!-- <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,900" rel="stylesheet"> 
    <title>Delivering</title>
</head>
<body>
    <div layout="column" layout-fill class="header-wrap header-btm-tabs header-btm-tabs blue-pinck-bg" id="mainheader">
        <div class="header dv-padding">
            <div class="header-title-wrap">
                <div class="header-title">Track Order</div>
            </div>
        </div>
    </div>
    <!-- /.header-wrap -->
    <div layout="column" class="footer-padding" layout-fill>
        <div class="das-profile-wrap container-white-bg" style="min-height:{{documentHeight}}px;">
            <md-content class="store-list-wrap">
                <md-card md-theme="default'" md-theme-watch>
                    <md-card-title>
                        <!-- <md-card-title-text ng-if="data.status == 'success'">
                            <span class="store-list-des"><strong>Pickup Address</strong>: {{data.orderDetailData.pickup_address}}</span>
                            <span class="store-list-des"><strong>Delivery Address</strong>: {{data.orderDetailData.deliver_address}}</span>
                        </md-card-title-text>
                        <md-card-title-text ng-if="data.status == 'error'">
                            <span class="store-list-des"><strong>{{data.message}}</strong></span>
                        </md-card-title-text> -->
                    </md-card-title>
                    <div class="map tracker-map form-group">
                        <ng-map zoom="16"></ng-map> 
                    </div>
                </md-card>
            </md-content>
        </div>
    </div>

    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAZkFMyrSZMtHqmN3xeMNzg7LZyOXwmeN8&libraries=places"></script>
    <!-- #Incude Anguler libraries -->
    <script src="../assets/js/angular/angular.min.js"></script>
    <script src="../assets/js/angular/angular-animate.min.js"></script>
    <script src="../assets/js/angular/angular-aria.min.js"></script>
    <script src="../assets/js/angular/angular-loader.min.js"></script>
    <script src="../assets/js/angular/angular-messages.min.js"></script>
    <script src="../assets/js/angular/angular-sanitize.min.js"></script>
    <script src="../assets/js/angular/angular-material.min.js"></script>
    <script src="../assets/js/angular/ng-map.min.js"></script>

    <script type="text/javascript">
        var app = angular.module('myApp', ['ngMaterial','ngAnimate','ngAria','ngMessages', 'ngMap']);
        app.constant('base', {
            siteName:'Delivering',
            siteUrl: 'https://dialadelivery.online',
            apiUrl: 'https://dialadelivery.online/api/v1/'  
        });
        

        app.controller('trackerController', function($scope, NgMap, $interval, $http, base){
            var marker = [];
            var car = base.siteUrl+'/assets/images/car-location-25.png';

            function getLocation(){
                var deliveryBoyData = [{lat:22.7533,lng:75.8937},{lat:22.7205,lng:75.9084},{lat:22.7441,lng:75.8940},{lat:22.7344,lng:75.8903}];


                var x = 0;
                angular.forEach(deliveryBoyData, function(value, key){
                      setTimeout(function(y) {    
                        var mylatLong = new google.maps.LatLng({lat: parseFloat(value.lat), lng: parseFloat(value.lng)});
                        NgMap.getMap().then(function(map){
                            if(marker != undefined && marker.length != 0){
                                marker.setPosition(mylatLong);
                                map.panTo(mylatLong);
                                marker.setMap(map);
                                /*marker.setMap(null);
                                marker = [];
                                console.log(value.lat);*/
                            }else {
                                map.setZoom(16);
                                map.setCenter(mylatLong);
                                marker = new google.maps.Marker({
                                    map: map,
                                    position: mylatLong,
                                    title: 'Your Package',
                                    icon: car
                                });
                            }

                        });
                      }, x * 5000, x); // we're passing x
                    x++;
                });
            }

            getLocation();
            $scope.repeat = $interval(function(){
                getLocation();
            },5000);
        });
    </script>
</body>
</html>