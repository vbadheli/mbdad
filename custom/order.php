<?php
$key = $_GET['key'];
$user_agent = base64_decode($key);
$udata = explode('#', $user_agent);
$order_id = base64_decode($udata[0]);
$agentID = base64_decode($udata[1]);
?>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			  crossorigin="anonymous"></script>
<script type="text/javascript">
	// A $( document ).ready() block.
	jQuery( document ).ready(function($) {

        $('.succes-msg-box').css("display", "non");
        $('.error-msg-box').css("display", "none");
	    
		var agentID = <?php echo $agentID;?>;
		var orderID = <?php echo $order_id;?>;

		console.log(agentID+' '+ orderID);
	    $.ajax({
            type: "POST",
            url: 'http://earth.ourgoogle.in/clients/dipen-patel/delivering/api/v1/orderAssign',
            data: { 
                'agentID' : agentID,
                'orderID' : orderID
            },
            cache: false,
            success: function(result){
               var response = JSON.parse(result);

             	if (response.status == "success") {
                    $('#successm').html(response.message);
                    $('.succes-msg-box').css("display", "block");
                    $('.errer-msg-box').css("display", "none");
                }else{
                    $('#errorm').html(response.message);
                    $('.errer-msg-box').css("display", "block");
                    $('.succes-msg-box').css("display", "none");
                }
        	}
        });
	});
</script>
<!DOCTYPE html>
<html> 
<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="../assets/css/angular-material.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../assets/css/custom-style.css">    
    <!-- <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,900" rel="stylesheet"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Delivering</title>
</head>
<body>  
<div layout="column" layout-fill class="header-logo-wrap">
    <div class="header-logo">
       <img src="../assets/images/logo.png" alt="Logo">
    </div>
</div><!-- /.header-wrap -->
<div layout="column" layout-fill class="login-signup-wrap">
    <div class="login-signup-tabs">

        <md-content class="dv-padding">
             <div class="custom-msg-box succes-msg-box" > 
                    <div class="msg-box-wrap">
                        <div class="msg-box-des" align="center">
                            <span class="msg-box-icon" style="position: relative;left: px;top: 0;line-height: 18px;"><i class="material-icons">check_circle</i> </span>
                            <h2 style="text-align:center">SUCCESS</h2>
                        <span id="successm"></span></div>
                    </div>
                </div>
                <div class="custom-msg-box errer-msg-box"> 

                    <div class="msg-box-wrap">
                        <div class="msg-box-des" align="center">
                            <span class="msg-box-icon" style="position: relative;left: px;top: 0;line-height: 18px;"><i class="material-icons">error</i> </span>
                             <h2 style="text-align:center">SORRY</h2>
                            
                            <span id="errorm"></span> 
                        </div>
                    </div>
                </div>
        </md-content>
    </div>
</div>
  
    <!-- <script type="text/javascript" src="assets/js/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- <script type="text/javascript" src="assets/js/jquery/materialize.min.js"></script> -->
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCaHbsH1oYOtOiQ3RBTaWoaWW_rlibWtcM&libraries=places"></script>
    <!-- #Incude Anguler libraries -->
    <script src="../assets/js/angular/angular.min.js"></script>
    <script src="../assets/js/angular/angular-animate.min.js"></script>
    <script src="../assets/js/angular/angular-aria.min.js"></script>
    <script src="../assets/js/angular/angular-cookies.min.js"></script>
    <script src="../assets/js/angular/angular-loader.min.js"></script>
    <script src="../assets/js/angular/angular-messages.min.js"></script>
    <script src="../assets/js/angular/angular-route.min.js"></script>
    <script src="../assets/js/angular/angular-sanitize.min.js"></script>
    <script src="../assets/js/angular/angular-material.min.js"></script>
    <script src="../assets/js/angular/angular-ui-router.min.js"></script>
    <script src="../assets/js/angular/ng-map.min.js"></script>
    <script src="../assets/js/angular/ngAutocomplete.js"></script>
    
</body>
</html>