<?php
    if(isset($_GET['order']) && $_GET['order'] != ''){
        $order_id = $_GET['order'];
    }
    else{
        header('location: https://dialadelivery.online');
    }
?>
<!DOCTYPE html>
<html ng-app="myApp" ng-controller="trackerController"> 
<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="../assets/css/angular-material.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../assets/css/custom-style.css">
    <!-- <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,900" rel="stylesheet"> 
    <title>Delivering</title>
</head>
<body>
    <div layout="column" layout-fill class="header-wrap header-btm-tabs header-btm-tabs blue-pinck-bg" id="mainheader">
        <div class="header dv-padding">
            <div class="header-title-wrap">
                <div class="header-title">Track Order</div>
            </div>
        </div>
    </div>
    <!-- /.header-wrap -->
    <div layout="column" class="footer-padding" layout-fill>
        <div class="das-profile-wrap container-white-bg" style="min-height:{{documentHeight}}px;">
            <md-content class="store-list-wrap">
                <md-card md-theme="default'" md-theme-watch>
                    <md-card-title>
                        <md-card-title-text ng-if="data.status == 'success'">
                            <span class="store-list-des"><strong>Pickup Address</strong>: {{data.orderDetailData.pickup_address}}</span>
                            <span class="store-list-des"><strong>Delivery Address</strong>: {{data.orderDetailData.deliver_address}}</span>
                        </md-card-title-text>
                        <md-card-title-text ng-if="data.status == 'error'">
                            <span class="store-list-des"><strong>{{data.message}}</strong></span>
                        </md-card-title-text>
                    </md-card-title>
                    <div class="map tracker-map form-group" ng-if="data.status == 'success'">
                        <ng-map zoom="16">
                            <directions draggable="false" panel="directions-panel" travel-mode="DRIVING" origin="{{data.orderDetailData.pickup_address}}" destination="{{data.orderDetailData.deliver_address}}">
                            </directions>
                        </ng-map> 
                    </div>
                </md-card>
            </md-content>
        </div>
    </div>

    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAZkFMyrSZMtHqmN3xeMNzg7LZyOXwmeN8&libraries=places"></script>
    <!-- #Incude Anguler libraries -->
    <script src="../assets/js/angular/angular.min.js"></script>
    <script src="../assets/js/angular/angular-animate.min.js"></script>
    <script src="../assets/js/angular/angular-aria.min.js"></script>
    <script src="../assets/js/angular/angular-loader.min.js"></script>
    <script src="../assets/js/angular/angular-messages.min.js"></script>
    <script src="../assets/js/angular/angular-sanitize.min.js"></script>
    <script src="../assets/js/angular/angular-material.min.js"></script>
    <script src="../assets/js/angular/ng-map.min.js"></script>

    <script type="text/javascript">
        var app = angular.module('myApp', ['ngMaterial','ngAnimate','ngAria','ngMessages', 'ngMap']);
        app.constant('base', {
            siteName:'Delivering',
            siteUrl: 'https://dialadelivery.online',
            apiUrl: 'https://dialadelivery.online/api/v1/'  
        });
        app.controller('trackerController', function($scope, NgMap, $interval, $http, base){
            var marker = [];
            var car = base.siteUrl+'/assets/images/car-location-25.png';

            $scope.order_id = '<?php echo $order_id; ?>';


            $scope.getLocations = function(){
                $http({
                    method : "GET",
                    url : base.apiUrl + '/trackOrderByLink/' + $scope.order_id,
                    ignoreLoadingBar: true,
                    data: {
                        encodedorderID : $scope.order_id,
                    },
                }).then(function mySuccess(response) {
                    $scope.data = response.data;
                    if($scope.data.status == 'success'){
                        NgMap.getMap().then(function(map){
                            if(marker != undefined && marker.length != 0){
                                marker.setMap(null);
                                marker = [];
                            }

                            /*var icon = {
                                path: car,
                                scale: .7,
                                strokeColor: 'white',
                                strokeWeight: .10,
                                fillOpacity: 1,
                                fillColor: '#404040',
                                offset: '15%',
                                anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 
                            };*/

                            map.setCenter({lat: parseFloat($scope.data.deliveryBoyData.lat), lng: parseFloat($scope.data.deliveryBoyData.lng)});
                            marker = new google.maps.Marker({
                                map: map,
                                position: {lat: parseFloat($scope.data.deliveryBoyData.lat), lng: parseFloat($scope.data.deliveryBoyData.lng)},
                                title: 'Your Package',
                                icon: car
                            });
                            //marker.setAnimation(google.maps.Animation.BOUNCE);
                        });
                    } else{
                        $interval.cancel($scope.repeat);
                    }
                }, function myError(response) {
                    $scope.myWelcome = response.statusText;
                });
            }

            $scope.getLocations();

            $scope.repeat = $interval(function(){
                $scope.getLocations();
            },5000);
        });
    </script>
</body>
</html>