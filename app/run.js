app.run(function($http, $timeout, $rootScope, $cookieStore, userService, check, $location, base, $window) {

	$rootScope.documentHeight = window.innerHeight-110;
	$rootScope.bodylayout = '';
	$rootScope.orderNotification = false;
	$rootScope.toUrl = '';
	$rootScope.fromUrl = '';

	/*== Check login ==*/
	$rootScope.$on('$locationChangeStart', function(event, toUrl, fromUrl) {
		var userData = $cookieStore.get('loginUserData');
	  	if( !check.isEmpty(userData) ){
		  	userService.isLoggedIn(userData.id, function(data){
		  		console.log(data);
		  		if(data === true && $location.path() !== '/signup' && $location.path() !== '/forgot-password'){
		  			console.log('root login true');
		  			//Do nothing
		  		}else{
		  			if( $location.path() !== '/signup' && $location.path() !== '/forgot-password'){
		  				console.log('root login false');
		  				$location.path( "/login" );
		  			}
		  		}
		  	});
	  	}else {
			if( $location.path() !== '/signup' && $location.path() !== '/forgot-password' ){
	  			console.log('root localStorage empty');
  				$location.path( "/login" );
  			}
  		}

  		var locationPath = $location.path();

  		/*== Conditional footer ==*/
  		if( locationPath !== '/login' 
  			&& locationPath !== '/signup' 
  			&& locationPath !== '/forgot-password' 
  			&& ( locationPath.indexOf("thank-you") == -1 ) ){
  			$rootScope.showFooter = true;
  		}else {
  			var documentHeight = angular.copy($rootScope.documentHeight);
  			$rootScope.showFooter = false;
  		}
	});

	/*== Get distance of two lat/long==*/
	$rootScope.getDistance = function(lat1, lon1, lat2, lon2, unit) {

		var radlat1 = Math.PI * lat1/180
		var radlat2 = Math.PI * lat2/180
		var theta = lon1-lon2
		var radtheta = Math.PI * theta/180
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist)
		dist = dist * 180/Math.PI
		dist = dist * 60 * 1.1515
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist
	}

	/*== Get absolute path ==*/
	$rootScope.hrefAbsolutePath = function(path){
		return $location.path($path);
	}

	/*== Get absolute path ==*/
	$rootScope.getFormattedAddress = function(address1='',address2='',city='', state='', country='', zip='', company = ''){
		return address1+''+address2+' </br>'+city+', '+zip+'</br>'+state+', '+country;
	}

	/*== Get absolute path ==*/
	$rootScope.getFormattedAddressWithoutHtml = function(address1='', address2='', city='', state='', country='', zip='', company = ''){
		return address1+', '+address2+', '+city+', '+zip+', '+state+', '+country;
	}
});
