/*== Config Routes ==*/
app.config(function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';

    $stateProvider
    .state('testing',{
        url: '/testing',
        cache: false,
        templateUrl: 'views/test.html',
        controller: 'testCtrl'
    })
    .state('login',{
        url: '/login',
        cache: false,
        templateUrl: 'views/login.html',
        controller: 'loginController'
    })
    .state('signup',{
        url: '/signup',
        cache: false,
        templateUrl: 'views/sign-up.html',
        controller: 'registerController'
    })
    .state('forgotPassword',{
        url: '/forgot-password',
        cache: false,
        templateUrl: 'views/forgot-password.html',
        controller: 'forgotPasswordController'
    })
    .state('pickupLocation',{
        url: '/new-order',
        cache: false,
        templateUrl: 'views/pickup.html',
        controller: 'pickupController'
    })
    .state('dropLocation',{
        url: '/drop',
        cache: false,
        templateUrl: 'views/drop.html',
        controller: 'dropCotroller'
    })
    .state('itemDetails',{
        url: '/item-details',
        cache: false,
        templateUrl: 'views/item-details.html',
        controller: 'itemDetailsController'
    })
    .state('orderSummary',{
        url: '/order-summary',
        cache: false,
        templateUrl: 'views/order-summary.html',
        controller: 'orderSummaryController'
    })
    .state('thankyou',{
        url: '/thank-you/:id',
        cache: false,
        templateUrl: 'views/thank-you.html',
        controller: 'myOrderController'
    })
    .state('dashboard',{
        url: '/dashboard',
        cache: false,
        templateUrl: 'views/dashboard.html',
        controller: 'dashboardController'
    })
    .state('adminDashboard',{
        url: '/admin-dashboard',
        cache: false,
        templateUrl: 'views/admin-dashboard.html',
        controller: 'adminDashboardController'
    })
    .state('adminManageTM',{
        url: '/admin-manage-tm',
        cache: false,
        templateUrl: 'views/admin-manage-tm.html',
        controller: 'adminTransportModeController'
    })
    .state('adminAddUpdateTM',{
        url: '/admin-addupdate-tm',
        cache: false,
        templateUrl: 'views/admin-addupdate-tm.html',
        controller: 'adminTransportModeController'
    })
    .state('editTMAdmin',{
        url: '/editTMAdmin/:id',
        cache: false,
        templateUrl: 'views/admin-update-tm.html',
        controller: 'adminTransportModeController'
    })
    .state('editProfile',{
        url: '/editProfile',
        cache: false,
        templateUrl: 'views/edit-Profile.html',
        controller: 'dashboardController'
    })
    .state('addUserProfileByAdmin',{
        url: '/addUserProfile',
        cache: false,
        templateUrl: 'views/edit-user-profile-by-admin.html',
        controller: 'adminUserManagementController'
    })
    .state('editUserProfileByAdmin',{
        url: '/editUserProfile/:id',
        cache: false,
        templateUrl: 'views/edit-user-profile-by-admin.html',
        controller: 'adminUserManagementController'
    })
    .state('userStore',{
        url: '/user-stores',
        cache: false,
        templateUrl: 'views/user-store.html',
        controller: 'storeAddressCtrl'
    })
    .state('userOrders',{
        url: '/user-orders',
        cache: false,
        templateUrl: 'views/user-orders.html',
        controller: 'myOrderController'
    })
    .state('orderDetails',{
        url: '/order-details/:id',
        cache: false,
        templateUrl: 'views/order-details.html',
        controller: 'orderDetailsController'
    })
    .state('trackOrder',{
        url: '/track-order/:id',
        cache: false,
        templateUrl: 'views/track-order.html',
        controller: 'trackOrderController'
    })
    .state('pendingOrderRequests',{
        url: '/order-requests',
        templateUrl: 'views/order-requests.html',
        controller: 'pendingOrderRequestsController'
    })
    .state('deliveryBoys',{
        url: '/delivery-boys',
        cache: false,
        templateUrl: 'views/delivery-boys.html',
        controller: 'deliveryBoysController'
    })
    .state('updateAddress',{
        url: '/updateAddress',
        cache: false,
        templateUrl: 'views/edit-Profile.html',
        controller: 'updateAddressController'
    })
    .state('adminTransactions',{
        url: '/adminTransaction',
        cache: false,
        templateUrl: 'views/admin-transaction.html',
        controller: 'adminTransactionsController'
    })
    .state('deliverymanTransactions',{
        url: '/deliverymanTransactions',
        cache: false,
        templateUrl: 'views/deliveryman-transaction.html',
        controller: 'deliverymanTransactionsController'
    })
    .state('editAvailability',{
        url: '/editAvailability',
        cache: false,
        templateUrl: 'views/edit-availability.html',
        controller: 'editAvailabilityController'
    })
    .state('paymentRelease',{
        url: '/paymentRelease/:id',
        cache: false,
        templateUrl: 'views/release-payment.html',
        controller: 'adminPaymentReleaseController'
    });
    $urlRouterProvider.otherwise('/login');
});