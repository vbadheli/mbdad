var app = angular.module('myApp', ['ngMaterial','ngAnimate','ngAria','ngMessages', 'ui.router', 'ngCookies', 'ngMap', 'ngAutocomplete', 'chieffancypants.loadingBar', 'md.time.picker', 'multipleSelect','angularFileUpload','ngFileUpload','ngTagsInput','angular-input-stars']);

app.constant('base', {
	siteName:'Delivering',
  // siteUrl: 'https://dialadelivery.online/newstaging/',
  // apiUrl: 'https://dialadelivery.online/newstaging/api/v1/'
  // siteUrl: 'https://dialadelivery.online/',
  // apiUrl: 'https://dialadelivery.online/api/v1/'
  siteUrl: 'http://localhost/mbdad/',
	apiUrl: 'http://localhost/mbdad/api/v1/'

});

/*== This is test controller ==*/
app.controller('testCtrl', function($scope, check, $rootScope, $interval, base, $http, $interval, $window, $cookieStore, userService){

/*    window.lat = 37.8199;
    window.lng = -122.4783;

    var map;
    var mark;
    var lineCoords = [];

    var initialize = function() {
      map  = new google.maps.Map(document.getElementById('map-canvas'), {center:{lat:lat,lng:lng},zoom:12});
      mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
      lineCoords.push(new google.maps.LatLng(window.lat, window.lng));
    };

    $window.initialize = initialize;

    var redraw = function(payload) {
      lat = payload.message.lat;
      lng = payload.message.lng;

      console.log(payload);

      map.setCenter({lat:lat, lng:lng, alt:0});
      mark.setPosition({lat:lat, lng:lng, alt:0});
      lineCoords.push(new google.maps.LatLng(lat, lng));

    var lineCoordinatesPath = new google.maps.Polyline({
        path: lineCoords,
        geodesic: true,
        strokeColor: '#2E10FF'
      });
      
      lineCoordinatesPath.setMap(map);
    };

    var pnChannel = "map-channel";

    var pubnub = new PubNub({
      publishKey: 'pub-c-d5569807-1806-493b-819a-1955474e5aa8',
      subscribeKey: 'sub-c-35445b4a-bbd2-11e7-bf1e-62e28d924c11'
    });

    pubnub.subscribe({channels: [pnChannel]});
    pubnub.addListener({message:redraw});

    setInterval(function() {
      pubnub.publish({channel:pnChannel, message:{lat:window.lat + 0.001, lng:window.lng + 0.01}});
    }, 5000);*/


    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: Infinity
    };

    function success(pos) {
      var crd = pos.coords;

      console.log('Your current position is:');
      console.log(`Latitude : ${crd.latitude}`);
      console.log(`Longitude: ${crd.longitude}`);
      alert(crd.longitude);
      console.log(`More or less ${crd.accuracy} meters.`);
    };

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    };

    navigator.geolocation.getCurrentPosition(success, error, options);
});
