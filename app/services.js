/*== Check object empty or not ==*/
app.service('check', function() {
    this.isEmpty = function(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    this.getLocation = function(callback) {
        navigator.geolocation.getCurrentPosition(function (position) {
            mysrclat = position.coords.latitude;
            mysrclong = position.coords.longitude;
            callback({lat:mysrclat,lng:mysrclong});
        });
    }


});

/*== Make Service for geting user data ==*/
app.service('userService', function($http, base, check) {
    /*== Check User Logged in or not ==*/
    this.isLoggedIn = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'checkLogin/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(true);
            }else {
                callback(false);
            }
        },function errorCallback(response) {

        });
    }

    /*== Check User Logged in or not ==*/
    this.isAdmin = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'isAdmin/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(true);
            }else {
                callback(false);
            }
        },function errorCallback(response) {

        });
    }

    /*== Check User Logged in or not ==*/
    this.isDeliveryMan = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'isDeliveryMan/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(true);
            }else {
                callback(false);
            }
        },function errorCallback(response) {

        });
    }

    /*== Check User Logged in or not ==*/
    this.isVendor = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'isVendor/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(true);
            }else {
                callback(false);
            }
        },function errorCallback(response) {

        });
    }

    /*== Get user Current Location ==*/
    this.getLocation = function(callback) {
        navigator.geolocation.getCurrentPosition(function (position) {
            mysrclat = position.coords.latitude;
            mysrclong = position.coords.longitude;
            callback({lat:mysrclat,lng:mysrclong});
        });
    }

    /*== Get all stores of user ==*/
    this.getAllStores = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getStores/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(response.data);
            }else {
                callback(false);
            }
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get User Meta ==*/
    this.getUserData = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getUserData/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(response.data);
            }else {
                console.log(response);
                callback(false);
            }
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get User vendor ==*/
    this.getVendorUserData = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getVendorUserData/'+userId,
        }).then(function successCallback(response) {
            if(response.data.status == 'success'){
                callback(response.data);
            }else {
                console.log(response);
                callback(false);
            }
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get all Order of user ==*/
    this.getUserDataByAdmin = function (AdminId, userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getUserDataByAdmin/'+AdminId+'/'+userId,
        }).then(function successCallback(response) {
            console.log("response");
            console.log(response.data);
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get all Order of user ==*/
    this.getVendorUserDataByAdmin = function (AdminId, userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getVendorUserDataByAdmin/'+AdminId+'/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            console.log("err");
            console.log(response);
        });
    }

    /*== User log out service ==*/
    this.logOutUser = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'logOut/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== User log out service ==*/
    this.getAllVenderOrders = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getAllOrder/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

	this.getDeliveryCount = function (userId, callback){
		$http({
			method: 'GET',
			url: base.apiUrl+'getPickupCount/'+userId,
		}).then(function successCallback(response){
			callback(response.data);
		},function errorCallback(response){
			
		});
	}
    /*== Get all Order of user ==*/
    this.getVenderOrder = function (userId, orderId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getOrder/'+orderId+'/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get all user feedbacks ==*/
    this.getfeedbacks = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getUserFeedbacks/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get all user feedbacks ==*/
    this.getGivenfeedbacks = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getUserGivenFeedbacks/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        },function errorCallback(response) {
            //Do nothing
        });
    }

    /*== Get One Delivery Boy ==*/
    this.getOneDeliveryBoy = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getOneDeliveryBoy/'+userId,
            ignoreLoadingBar: true,
        }).then(function successCallback(response) {
            if(response.data.status == "success"){
                if( !check.isEmpty(response.data.deliveryBoyData[0]) ){
                    var deliveryBoyLat = response.data.deliveryBoyData[0].lat;
                    var deliveryBoyLng = response.data.deliveryBoyData[0].lng;
                    var deliveryBoyStatus = response.data.deliveryBoyStatus;
                    callback(deliveryBoyLat, deliveryBoyLng, deliveryBoyStatus);
                }else{
                  callback(false, false, false);
                }
            }else {
                callback(false, false, false);
            }
        });
    }

    /*== Get One Delivery Boy ==*/
    this.checkNewOrder = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'checkNewOrder/'+userId,
            ignoreLoadingBar: true,
        }).then(function successCallback(response) {
            if(response.data.status == "success"){
                callback(response.data.orderNotification);
            }else {
                callback(false);
            }
        });
    }

    /*== Insert / Update Delivery boy Cordinate ==*/
    this.insertUpdateCordinates = function (userId, userLat, userLng, callback) {
        $http({
            method: 'POST',
            url: base.apiUrl+'insertUpdateCordinates',
            ignoreLoadingBar: true,
            data: {
                userId : userId,
                userLat : userLat,
                userLng : userLng
            },
        }).then(function successCallback(response) {
            callback(response);
        });
    }

    /*== Get all DeliveryBoys ==*/
    this.getAllDeliveryBoy = function (userId, distance, workstatus, callback) {
        check.getLocation(function(current_location){
            console.log(current_location);
        });
        // check.getLocation(function(current_location){
            $http({
                method: 'POST',
                url: base.apiUrl+'getAllDeliveryBoy',
                data: {
                    userId : userId,
                    userLat : 22.7533,
                    userLng: 75.8937,
                    distance: distance,
                    workStatus : workstatus
                },
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response){
                console.table(response);
            });
        // });
    }

    /*== Get all DeliveryBoys ==*/
    this.getAllDeliveryBoyOrders = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getAllDeliveryBoyOrders/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        }, function errorCallback(response){
            console.table(response);
        });
    }

    /*== Get All on-hold status order  ==*/
    this.getAllAwaitingOrder = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getAllAwaitingOrder/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        }, function errorCallback(response){
            console.table(response);
        });
    }

    /*== Get All on-hold status order  ==*/
    this.getDeliveryManSuburbs = function (userId, callback) {
        $http({
            method: 'GET',
            url: base.apiUrl+'getDeliverySuburbs/'+userId,
        }).then(function successCallback(response) {
            callback(response.data);
        }, function errorCallback(response){
            console.table(response);
        });
    }
});
