/*== This is main Controller ==*/
app.config(function($mdAriaProvider) {
   // Globally disables all ARIA warnings.
   $mdAriaProvider.disableWarnings();
});

app.controller('mainController', function($timeout, $scope, check, $rootScope, $interval, base, $http, $interval, $window, $cookieStore, userService, $location, cfpLoadingBar, $mdDialog){

	/*== Cf loading bar ==*/
	cfpLoadingBar.start();
	$scope.currentUser = {};
	$scope.currentUser.itemDetail = [];

	/*== Item details Page Event ==*/
	$scope.items = [{id: 1}];
	$scope.currentUser.item = '';
	$scope.currentUser.endUser = {};

	/*== Get current user data ==*/
	var userData = $cookieStore.get('loginUserData');
	$rootScope.loginUserDatas = angular.copy(userData);


	function check_for_new_order(){
		if ( !check.isEmpty(userData) && userData.user_role == 'deliveryman'){
			/*== Get User all stores ==*/
			userService.checkNewOrder(userData.id, function(orderNotification){
				if(orderNotification){
					$rootScope.orderNotification = true;
				}else {
					$rootScope.orderNotification = false;
				}
			});
		}
	}

	/*== Check login ==*/
	$rootScope.$on('$locationChangeStart', function(event, toUrl, fromUrl) {
		var userData = $cookieStore.get('loginUserData');
		if( !check.isEmpty(userData) ){
			userService.isLoggedIn(userData.id, function(data){
				if(data === true && $location.path() !== '/signup' && $location.path() !== '/forgot-password'){
		  			//Do nothing
		  			console.log('main controller login true');
		  		}else{
		  			if( $location.path() !== '/signup' && $location.path() !== '/forgot-password'){
		  				$location.path( "/login" );
		  				console.log('main controller login false');
		  			}
		  		}
		  	});
		}else {
			if( $location.path() !== '/signup' && $location.path() !== '/forgot-password' ){
				console.log('main controller localStorage empty');
				$location.path( "/login" );
			}
		}
	});

	/*== Update Deleveryman cordinate ==*/
	function geo_success(position) {

		if ( !check.isEmpty(userData) && userData.user_role == 'deliveryman'){
			/*== Apply nosleep while deliveryman tracking in progress ==*/
			var noSleep = new NoSleep();
			var wakeLockEnabled = false;

			$rootScope.bodylayout = 'delivery-man';
			var currentLat = position.coords.latitude;
			var currentLng = position.coords.longitude;

			/*== Get User all stores ==*/
			userService.getOneDeliveryBoy(userData.id, function(deliveryBoyPreviouslat, deliveryBoyPreviouslng, deliveryBoyStatus){
				var currentLat = position.coords.latitude;
				var currentLng = position.coords.longitude;

				if(deliveryBoyStatus && !wakeLockEnabled){
					noSleep.enable();
					wakeLockEnabled = true;
					console.log('no sleep enable');
				}else {
					noSleep.disable(); 
					wakeLockEnabled = false;
					console.log('no sleep disable');
				}

				if(deliveryBoyPreviouslat && deliveryBoyPreviouslng){
					var distance = $rootScope.getDistance(deliveryBoyPreviouslat, deliveryBoyPreviouslng, currentLat, currentLng, "K");
					if( parseFloat(distance) > 0.02 ){
						userService.insertUpdateCordinates(userData.id, position.coords.latitude, position.coords.longitude, function(response){
							console.log(response);
						});
					}
				}else {
					userService.insertUpdateCordinates(userData.id, position.coords.latitude, position.coords.longitude, function(response){
						console.log(response);
					});
				}
			});
		}else {
			$rootScope.bodylayout = 'vendor';
		}
	}

	/*== Run function when geo location send error ==*/
	function geo_error(error){
		console.log ("gps lost");
	}

	var watchID = $window.navigator.geolocation.watchPosition(geo_success, geo_error, {enableHighAccuracy: true });

	/*== User logout ==*/
	$scope.userLogOut = function(ev){
		if ( !check.isEmpty(userData) ){
				// Appending dialog to document.body to cover sidenav in docs app
				var confirm = $mdDialog.confirm({
					onComplete: function afterShowAnimation() {
						var $dialog = angular.element(document.querySelector('md-dialog'));
						var $actionsSection = $dialog.find('md-dialog-actions');
						var $cancelButton = $actionsSection.children()[0];
						var $confirmButton = $actionsSection.children()[1];
						angular.element($confirmButton).addClass('md-raised md-warn');
						angular.element($cancelButton).addClass('md-raised');
					}
				})
				.title('Are you sure, you want to log out?')
				.ariaLabel('Lucky day')
				.targetEvent(ev)
				.cancel('No')
				.ok('Yes');

				$mdDialog.show(confirm).then(function() {
					userService.logOutUser(userData.id, function(data){
						if(data.status == 'success'){
							$cookieStore.remove('currentUser');
							$cookieStore.remove('loginUserData');
							$location.path("/login");
						}else {
							$cookieStore.remove('currentUser');
							$cookieStore.remove('loginUserData');
							$location.path("/login");
						}
					});
				}, function() {
					console.log('You decided to keep your debt.');
				});
			}else {
				$cookieStore.remove('currentUser');
				$cookieStore.remove('loginUserData');
				$location.path("/login");
			}
		}

		/*== check for new incomming order ==*/
		$interval(function(){
			check_for_new_order();
			$window.navigator.geolocation.getCurrentPosition(geo_success, geo_error, {enableHighAccuracy: true });
		}, 30000);
	});

/*== This is registration Controller ==*/
app.controller('registerController', function($scope, $http, base, $location, $state, $timeout, $cookieStore, check, FileUploader,Upload, $mdDialog){
	// console.log($cookieStore.get('loginUserData'), check.isEmpty($cookieStore.get('loginUserData')));
	$scope.user = {};
	$scope.selectfile;
	$scope.tags=[];
	$scope.ajaxResponseMessage = 'This is test message';
	$scope.showSuccess = false;
	$scope.showError = false;
	var myDate = new Date();
	
	var uploader = $scope.uploader = new FileUploader({
		url: base.apiUrl+'upload',
		queueLimit: 1,
	});
	$scope.maxDate = new Date((myDate.getFullYear() - 16), myDate.getMonth(), (myDate.getDate()-1));
	$scope.minDate = new Date((myDate.getFullYear() - 100), myDate.getMonth(), (myDate.getDate()-1));
	// console.log(maxDate.getFullYear());

	$scope.defaultAge = 16;
	

	uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
		
		// console.log("uploader",uploader);
	});

	uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
         // console.info('onWhenAddingFileFailed', item, filter, options);
         
        /*$mdDialog.show({
			// targetEvent: $event,
			scope: $scope,
			templateUrl: 'assets/dialog/invalidFileType.tmpl.html',
			controller: 'registerController',
			preserveScope: true
		});*/
		if(filter.name=='queueLimit')
		{
			//item.remove();
			alert("Remove Selected image");
		}
		else
		{
			alert("Select image file type");
		}
	};

	uploader.onSuccessItem = function(fileItem, response, status, headers) {
            // console.info('onSuccessItem', fileItem, response, status, headers);
            if(response.status == "success")
            {
            	$scope.user.filename = response.filename;
            	$scope.user.availability_slots = $scope.tags;
            	// $scope.user.time_avalibility_from = $scope.user.time_avalibility_from.toTimeString();
    			// $scope.user.time_avalibility_to = $scope.user.time_avalibility_to.toTimeString();
    			$http({
    				method: 'POST',
    				url: base.apiUrl+'signup',
    				data: {
    					userData: $scope.user,
    				},
    				headers: { 'Content-Type': undefined},
    			}).then(function successCallback(response) {
    				if(response.data.status == 'success'){
    					if($scope.user.requestFor == 'deliveryman')
    					{
    						var url = response.data.verificationData.continue_url;
	    					window.open(url, '_blank');
	    					// window.location = response.data.verificationData.continue_url;
    					}
    					$scope.ajaxResponseMessage = response.data.message;
    					$scope.showSuccess = true;
    					$scope.showError = false;
    					$timeout(function() {
    						$state.go("login");
    					}, 500);
    				}else {
    					$scope.ajaxResponseMessage = response.data.message;
    					$scope.showSuccess = false;
    					$scope.showError = true;
    				}
    			});
    		}
    		console.info('uploader', uploader);
    	};
    	uploader.onBeforeUploadItem = function(item) {
			// item.remove();
			$scope.uploader.clearQueue();
			console.info('onBeforeUploadItem', item);
		};

		uploader.onAfterAddingFile = function(fileItem) {

			console.info('onAfterAddingFile', fileItem);
			$scope.selectfile = fileItem;
		};

		$scope.loadTimePickerPopup = function($event)
		{
			console.log($event);
			$scope.daysWithTime={};
			$mdDialog.show({
				targetEvent: $event,
				scope: $scope,
				templateUrl: 'assets/dialog/newDeliverymanTimeAvailability.tmpl.html',
				controller: '',
				preserveScope: true
			});
		};

		$scope.daysWithTime={};
		$scope.addNewday = function()
		{
			var fullfromTimearr = $scope.daysWithTime.fromTime.split(" ");
			var fulltoTimearr = $scope.daysWithTime.toTime.split(" ");

			var fromTimearr = fullfromTimearr[0].split(":");
			var toTimearr = fulltoTimearr[0].split(":");

			/*if(parseInt(fromTimearr[0]) > parseInt(toTimearr[0]))
			{
				alert("from time should be greater");
			}
			else {*/
	        	// console.log(fromtimearr);
	        	$scope.daysWithTime.display = $scope.daysWithTime.day+" "+$scope.daysWithTime.fromTime+" to "+$scope.daysWithTime.toTime;
	        	$scope.tags.push($scope.daysWithTime);// = [{text:"mon 8am to 10am"}];
	        	$mdDialog.hide();
			// }
		};

	/*$scope.uploadPic = function(file){
        	file.upload = Upload.upload({
        		// url:base.apiUrl+'upload',
        		// data:{username:$scope.user.fname, file:file}
        		url: base.apiUrl+'signup',
				data: {
					userData: $scope.user, file:file
				},
        	}).then(function (response) {
	            console.log('Success ' + response.config.data.file.name + 'uploaded. Response: ' + response.data);
	            if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					$timeout(function() {
						$state.go("login");
					}, 500);
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}
	        }, function (resp) {
	            console.log('Error status: ' + resp.status);
	        }, function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
	        });
        	
	    }*/

	function getAllStates(){
		// Get all states
		$http({
			method: 'GET',
			url: base.apiUrl+'getAllStates',
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.statesData = response.data.stateData;
				// $scope.statesData = [{'state_code':'1', 'state':'BCd'},{'state_code':'2', 'state':'def'}];
			}
		});
	}

	$scope.getCities = function(){
		// Get all state cities
		$scope.cityData = [];
		$scope.suburbsData=[];
		$scope.user.suburbs = [];
		$http({
			method: 'GET',
			url: base.apiUrl+'getStateCities/'+$scope.user.state,
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.cityData = response.data.cityData;
				$scope.suburbsData = response.data.suburbsData;
			}
		});
	}

	function getTransportationModes() {
		$http({
			method: 'GET',
			url: base.apiUrl+'getActiveTransportationModes',
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				console.log(response);
				$scope.tmData = response.data.tmData;
			}
		});
	}

	getAllStates();
	getTransportationModes();
	/*uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem.file.name);
    };*/
// $scope.user.time_avalibility_from = new Date();
// $scope.user.time_avalibility_to = new Date();

$scope.userRegister = function() {

uploader.uploadAll();
	/*if($scope.user.requestFor == 'deliveryman')
	{
    		// console.log($scope.user.time_avalibility_from.toTimeString());
    		// console.log($scope.user.time_avalibility_to);
    		uploader.uploadAll();
    	}
    	else
    	{

    		uploader.uploadAll();
    		$http({
    			method: 'POST',
    			url: base.apiUrl+'signup',
    			data: {
    				userData: $scope.user,
    			},
    			headers: { 'Content-Type': undefined},
    		}).then(function successCallback(response) {
    			if(response.data.status == 'success'){
    				$scope.ajaxResponseMessage = response.data.message;
    				$scope.showSuccess = true;
    				$scope.showError = false;
    				$timeout(function() {
    					$state.go("login");
    				}, 500);
    			}else {
    				$scope.ajaxResponseMessage = response.data.message;
    				$scope.showSuccess = false;
    				$scope.showError = true;
    			}
    		});
    	}*/
    };
});

/*== This is login Controller ==*/
app.controller('loginController', function($scope, $http, base, $location, $cookieStore, $timeout, $state, check, $interval, $rootScope){

	// console.log($cookieStore.get('loginUserData'), check.isEmpty($cookieStore.get('loginUserData')));
	$scope.user = {};
	$scope.ajaxResponseMessage = '';

	$scope.userLogin = function() {
		$http({
			method: 'POST',
			url: base.apiUrl+'login',
			data: {
				userData : $scope.user
			},
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$cookieStore.put('loginUserData', response.data.userData);
				$rootScope.loginUserDatas = response.data.userData;
				$scope.showSuccess = true;
				$scope.showError = false;
				$scope.ajaxResponseMessage = response.data.message;
				console.log(response.data.userData);
				var userData = response.data.userData
				$timeout(function() {
					$state.go("dashboard");
				}, 500);
			}else {
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = false;
				$scope.showError = true;
				$scope.user = {};
			}
		});
	};
});

app.controller('forgotPasswordController', function($scope, check, base, $http, $location, $timeout, $state, $cookieStore){

	// console.log($cookieStore.get('loginUserData'), check.isEmpty($cookieStore.get('loginUserData')));

	$scope.user = {};

	$scope.forgotPasswordSubmit = function(){
		if( !check.isEmpty($scope.user) ){
			$http({
				method: 'POST',
				url: base.apiUrl+'forgotPass',
				data: {
					userEmail : $scope.user
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showSuccess = true;
					$scope.showError = false;
					$scope.ajaxResponseMessage = response.data.message;
					$timeout(function() {
						$state.go("login");
					}, 500);
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
					$scope.user = {};
				}
			});
		}
	}
});

/*== This is update user address Controller ==*/
app.controller('updateAddressController', function($scope, $http, base, $location, userService ,check, $cookieStore, $state, $mdDialog){

	$scope.userData = $cookieStore.get('loginUserData');
	$scope.base = base;
	var userData = angular.copy($scope.userData);

	if ( !check.isEmpty(userData) ){
		/*== Get User all stores ==*/
		userService.getUserData(userData.id, function(data){
			$scope.updateProfileData = data.userData;
		});
	}

	/*== User logout ==*/
	$scope.updateUserProfile = function(){
		if ( !check.isEmpty(userData) ){
			$http({
				method: 'POST',
				url: base.apiUrl+'updateUser/',
				data: {
					userID : userData.id,
					userData : $scope.updateProfileData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showSuccess = true;
					$scope.showError = false;
					$scope.ajaxResponseMessage = response.data.message;
					userService.getUserData(userData.id, function(data){
						$scope.updateProfileData = data.userData;
						$state.go("dashboard");
					});
				}else {
					$scope.showSuccess = false;
					$scope.showError = true;
					$scope.ajaxResponseMessage = response.data.message;
					$scope.ajaxResponseMessage = response.data.message;
				}
			});
		}
	}

});

/*== This is Dashboard Controller ==*/
app.controller('dashboardController', function($scope, $http, base, $location, userService ,check, $cookieStore, $state, $mdDialog, $timeout, $mdpTimePicker){

	$cookieStore.remove('currentUser');
	$scope.base = base;
	$scope.userData = $cookieStore.get('loginUserData');
	var userData = angular.copy($scope.userData);

	if ( !check.isEmpty(userData) ){
		//Do nothing
	}else {
		$location.path( "/login" );
	}

	$scope.deliveryCharges = function(){
		if($scope.updateProfileData.pay_sys_type=="average"){
			$http({
				method: 'GET',
				url: base.apiUrl+'getSystemAvgPrice/'+$scope.userData.id,
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.updateProfileData.deliveryman_per_km_charge = response.data.price;
				}else {
					console.log("Error","error");
				}
			});
		}
		else if($scope.updateProfileData.pay_sys_type=="mode"){
			$http({
				method: 'GET',
				url: base.apiUrl+'getSystemModePrice/'+$scope.userData.id,
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.updateProfileData.deliveryman_per_km_charge = response.data.price;
				}else {
					console.log("Error","error");
				}
			});
		}
	}
	
	/*== Get edit user data ==*/
	function getEditUserData() {

		userService.getUserData(userData.id, function(data){
			let updateProfileData = data.userData;

			if(updateProfileData.time_avalibility_from !== ''){
				updateProfileData['time_avalibility_from'] = new Date(updateProfileData.time_avalibility_from);
			}else {
				updateProfileData['time_avalibility_from'] = new Date();
			}

			if(updateProfileData.time_avalibility_to !== ''){
				updateProfileData['time_avalibility_to'] = new Date(updateProfileData.time_avalibility_to);
			}else {
				updateProfileData['time_avalibility_to'] = new Date();
			}

			$scope.updateProfileData = data.userData;
			console.log("profiledata");
			console.log($scope.updateProfileData);
			userService.getVendorUserData(userData.id, function(vendordata){
				// let updateVendorProfileData = vendordata.userData;

				/*if(updateProfileData.time_avalibility_from !== ''){
					updateProfileData['time_avalibility_from'] = new Date(updateProfileData.time_avalibility_from);
				}else {
					updateProfileData['time_avalibility_from'] = new Date();
				}

				if(updateProfileData.time_avalibility_to !== ''){
					updateProfileData['time_avalibility_to'] = new Date(updateProfileData.time_avalibility_to);
				}else {
					updateProfileData['time_avalibility_to'] = new Date();
				}*/

				$scope.updateVendorProfileData = vendordata.userData;
			});
		});
	}

	/*== GET edit user data when user on edit screen ==*/
	getEditUserData();	
	
	/*== User logout ==*/
	$scope.updateUserProfile = function(){
		if ( !check.isEmpty(userData) ){
			$http({
				method: 'POST',
				url: base.apiUrl+'updateUser/',
				data: {
					userID : userData.id,
					userData : $scope.updateProfileData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showSuccess = true;
					$scope.showError = false;
					$scope.ajaxResponseMessage = response.data.message;
					$timeout(function(){
						getEditUserData();
						$state.go("dashboard");
					}, 2000);
				}else {
					$scope.showSuccess = false;
					$scope.showError = true;
					$scope.ajaxResponseMessage = response.data.message;
				}
			});
		}
	}

	/*== Add / Edit Time Availability  ==*/
	$scope.editTimeAvailability = function($event, deliverymanData){

		if( !check.isEmpty(deliverymanData) ){
			$timeout(function() {
				$scope.deliverymanTimeAvailability = deliverymanData;
			}, 1000);

			$mdDialog.show({
				targetEvent: $event,
				scope: $scope,
				templateUrl: 'assets/dialog/deliverymanTimeAvailability.tmpl.html',
				controller: 'dashboardController',
				preserveScope: true
			});
		}else {
			alert('data not found!');
		}
	}

	/*== Close dialog ==*/
	$scope.closeDialog = function(){
		$mdDialog.hide();
	}

	/*== Change work status ==*/
	$scope.changeDeliveryManWorkStatus = function(userData, ev){
		
		var dndMessage = '';

		if(userData.dndMode){
			dndMessage = 'If you have activate DND mode then you no longer get order Notifications and orders!!'
		}else {
			dndMessage = 'If you have inactive DND mode then you can get order Notifications and orders!!'
		}

		// Appending dialog to document.body to cover sidenav in docs app
		var confirm = $mdDialog.confirm({
			onComplete: function afterShowAnimation() {
				var $dialog = angular.element(document.querySelector('md-dialog'));
				var $actionsSection = $dialog.find('md-dialog-actions');
				var $cancelButton = $actionsSection.children()[0];
				var $confirmButton = $actionsSection.children()[1];
				angular.element($confirmButton).addClass('md-raised md-warn');
				angular.element($cancelButton).addClass('md-raised');
			}
		})
		.title(dndMessage)
		.ariaLabel('Lucky day')
		.targetEvent(ev)
		.cancel('No')
		.ok('Yes');

		$mdDialog.show(confirm).then(function() {
			$http({
				method: 'POST',
				url: base.apiUrl+'changeUserDndmode',
				data: {
					userID : userData.id,
					userData : userData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					//Do nothing
				}else {
					//Do nothing
				}
			});
		});
	}
});

/*== Controller For Pickup Location ==*/
app.controller('pickupController', function($scope, $state, check, NgMap, $rootScope, $timeout, $cookieStore, userService){

	var userData = $cookieStore.get('loginUserData');

	/*== Get data from cookie store ==*/
	if ( !check.isEmpty($cookieStore.get('currentUser')) ){
		$scope.currentUser = $cookieStore.get('currentUser');
	}else {
		$scope.currentUser = {};
	}


	$scope.userProfileData = {};
	$scope.selectedUser = {};
	var options = { types: ['geocode'] };

	/*== Google Map API data ==*/
	var geocoder = new google.maps.Geocoder();

	/*== Auto Populate Address ==*/
	$scope.pickupAddressChange = function(){
		NgMap.getMap().then(function(map) {
			geocoder.geocode({'address': angular.copy($scope.currentUser.pickUplocation)}, function(results, status) {
				if(Object.keys(results).length > 0){
					$scope.currentUser.pickLatlng = {
						lat: results[0].geometry.location.lat(),
						lng: results[0].geometry.location.lng()
					};
				}
			});
		});
	}

	$scope.goTodroploc = function(){
		$cookieStore.put('currentUser', $scope.currentUser);
		$state.go("dropLocation");
	}

	if ( !check.isEmpty(userData) ){
		/*== Get User all stores ==*/
		userService.getAllStores(userData.id, function(data){
			if ( !check.isEmpty(userData) ){
				$scope.userStores = data.storeData;
			}else {
				$scope.userStores = null;
			}
		});

		/*== Get User all stores ==*/
		userService.getUserData(userData.id, function(data){
			$scope.userProfileData = data.userData;
		});
	}

	/*== Change Address On Radio button Changet ==*/
	$scope.changePickupAddress = function(selectedUser, addressType){

		if( !check.isEmpty(selectedUser) ){
			if(addressType == 'storeAddress'){ // If User Select Store Address
				$scope.selectedUser.address = addressType;
				if ( !check.isEmpty($scope.userStores) ){
					var userStores = angular.copy($scope.userStores[0]);
					if( userStores.address_1 !== ''
						&& userStores.city !== ''
						&& userStores.state !== ''
						&& userStores.country !== ''
						&& userStores.zip !== ''
						&& userStores.company !== '' ){
						var pickUplocation = $rootScope.getFormattedAddressWithoutHtml(userStores.address_1,userStores.address_2, userStores.city, userStores.state, userStores.country, userStores.zip);
					$scope.currentUser.pickUplocation = pickUplocation;
					$scope.pickupAddressChange();
				}else {
					$scope.currentUser.pickUplocation = '';
				}
			}else {
				$scope.currentUser.pickUplocation = '';
			}
		}

			if(addressType == 'baseAddress'){ // If User Select Base Address
				$scope.selectedUser.address = addressType;
				if ( !check.isEmpty($scope.userProfileData) ) {
					var userProfileData = angular.copy($scope.userProfileData);
					if( userProfileData.address_1 !== ''
						&& userProfileData.city !== ''
						&& userProfileData.state !== ''
						&& userProfileData.country !== ''
						&& userProfileData.zip !== '' 
						&& typeof(userProfileData.address_1) !== 'undefined'
						&& typeof(userProfileData.city) !== 'undefined'
						&& typeof(userProfileData.state) !== 'undefined'
						&& typeof(userProfileData.country) !== 'undefined'
						&& typeof(userProfileData.zip) !== 'undefined' ) {

						console.log(userProfileData);
					var pickUplocation = $rootScope.getFormattedAddressWithoutHtml(userProfileData.address_1,userProfileData.address_2, userProfileData.city, userProfileData.state, userProfileData.country, userProfileData.zip);
					$scope.currentUser.pickUplocation = pickUplocation;
					$scope.pickupAddressChange();
				}
			}else {
				$scope.currentUser.pickUplocation = '';
			}
		}

		if( addressType == 'customAddress' ){
			$scope.selectedUser.address = addressType;
			$scope.currentUser.pickUplocation = '';
		}
	}
}

/*== Change Address On Radio button Change ==*/
$scope.changePickupStore = function(userStores){
	if ( !check.isEmpty(storeData) ){
		if( userStores.address_1 !== ''
			&& storeData.address_2 !== ''
			&& storeData.city !== ''
			&& storeData.state !== ''
			&& storeData.country !== ''
			&& storeData.zip !== ''
			&& storeData.company !== '' ){
			var pickUplocation = $rootScope.getFormattedAddressWithoutHtml(storeData.address_1,storeData.address_2, storeData.city, storeData.state, storeData.country, storeData.zip);
		$scope.currentUser.pickUplocation = pickUplocation;
		$scope.pickupAddressChange();
	}else {
		$scope.currentUser.pickUplocation = '';
	}
}else {
	$scope.currentUser.pickUplocation = '';
}
}
});

/*== Drop Controller For Drop Location ==*/
app.controller('dropCotroller', function($scope, check, NgMap, $rootScope, $timeout, $location, $cookieStore, $state, userService){

	if( !$scope.currentUser ){
		$state.go("pickupLocation");
	}

	/*== Get data from cookie store ==*/
	if ( !check.isEmpty($cookieStore.get('currentUser')) ){
		$scope.currentUser = $cookieStore.get('currentUser');
	}else {
		$scope.currentUser = {};
	}

	/*== Google Map API data ==*/
	var geocoder = new google.maps.Geocoder();
	var marker = new google.maps.Marker();

	/*== Drop Location Data ==*/
	userService.getLocation(function(data){
		if( !check.isEmpty(data) ){
			NgMap.getMap().then(function(map) {
				map.setCenter({lat: data.lat, lng: data.lng});
				/*marker = new google.maps.Marker({
					map: map,
					position: {lat: data.lat, lng: data.lng},
					draggable: true,
					title: 'My Title'
				});*/
				geocodePosition(marker.getPosition());
			});
		}
	});

	/*== Auto Populate Address ==*/
	$scope.addressChange = function(){
		NgMap.getMap().then(function(map) {
			geocoder.geocode({'address': angular.copy($scope.currentUser.droplocation)}, function(results, status) {
				if(!check.isEmpty(results)){
					map.setCenter(results[0].geometry.location);
					//marker.setPosition(results[0].geometry.location);
					//geocodePosition(marker.getPosition());
					$scope.currentUser.dropLatlng = {
						lat: results[0].geometry.location.lat(),
						lng: results[0].geometry.location.lng()
					};
					marker.setMap(null);
					marker = new google.maps.Marker({
						map: map,
						position: {lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()},
						draggable: true,
						title: 'My Title'
					});
				}
			});
		});
	}

	$scope.goToItemDetails = function(){
		$cookieStore.put('currentUser', $scope.currentUser);
		$state.go('itemDetails');
	}

	/*== Drop Location Event After drag and drop marker  ==*/
	NgMap.getMap().then(function(map) {
		google.maps.event.addListener(marker, 'dragend', function() {
			geocodePosition(marker.getPosition());
			map.panTo(marker.getPosition());
			console.log('dragend');
		});

		google.maps.event.addListener(map, 'click', function(e) {
			geocodePosition(marker.getPosition());
			marker.setPosition(e.latLng);
			map.panTo(marker.getPosition());
		});
	});

	function geocodePosition(pos) {
		geocoder.geocode({
			latLng: pos
		}, function(responses) {
			if (responses && responses.length > 0) {
				$scope.currentUser.droplocation = '';
				$timeout(function() {
				//$scope.currentUser.droplocation = angular.copy(responses[0].formatted_address);
				geocoder.geocode({'address': angular.copy($scope.currentUser.droplocation)}, function(results, status) {
					if(!check.isEmpty(results)){
						$scope.currentUser.dropLatlng = {
							lat: results[0].geometry.location.lat(),
							lng: results[0].geometry.location.lng()
						};
					}
				});
			}, 100);
			}
		});
	}
});

/*== Item details controller ==*/
app.controller('itemDetailsController', function($scope, check, NgMap, $cookies, $cookieStore, $rootScope, $timeout, $state, $location){

	$scope.currentUser = $cookieStore.get('currentUser');
	if( !$scope.currentUser || !$scope.currentUser.pickUplocation  || !$scope.currentUser.droplocation ){
		$state.go("pickupLocation");
	}

	/*== Send Push Notification ==*/
	$scope.sendPush = function(){

		var title = 'title=New Order Request From Delivering';
		var content = '&content=Please check your mail and your order';
		var url = '&redirect='+base.siteUrl;
		var icon = '&icon='+base.siteUrl+'/assets/images/logo.png';

		$http({
			method: 'POST',
			url: base.apiUrl+'sendPush',
			data: {
				pushdata : title+content+url+icon,
				url : "https://pushmesh.com/api/v1/sendpush/40",
				key : '80101e602c61afa07481864312374698'
			},
		}).then(function successCallback(orderResponse) {
			if(orderResponse.data.status == 'success'){
				console.log(orderResponse.data.message);
			}else {
				console.log(orderResponse.data.message);
			}
		});
	}

	$scope.activeNext = true;

	/*== Check item details blank or not ==*/
	$scope.checkItemDetails = function(){
		angular.forEach($scope.currentUser.itemDetail, function(value, key){
			if(value.item_name && value.item_type && value.item_qty){
				$scope.activeNext = false;
			}else {
				$scope.activeNext = true;
			}
		});
	}

	/*== Check item details ==*/
	$scope.checkItemDetails();

	/*== Add new Item ==*/
	$scope.addNewitem = function() {
		var newItemNo = $scope.items.length+1;
		$scope.items.push({'id':newItemNo});
	};

	/*== Remove items ==*/
	$scope.removeItem = function(index) {
		$scope.items.splice(index, 1);
		$scope.itemDetail.splice(index, 1);
	};

	/*== Go to Order Summery Page ==*/
	$scope.gotoOrderSummery = function(itemDetail){
		if( $scope.activeNext === false ){
			$cookieStore.put('currentUser', $scope.currentUser);
			$state.go("orderSummary");
		}else {
			alert('Something went wrong');
		}
	}
});

/*== Order Summary Controller ==*/
app.controller('orderSummaryController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $interval, $http, base, $rootScope){

	var userData = $cookieStore.get('loginUserData');
	$scope.clickOnConformOrder = false;

	$scope.currentUser = $cookieStore.get('currentUser');
	if( !$scope.currentUser || !$scope.currentUser.pickUplocation  || !$scope.currentUser.droplocation || $scope.currentUser.itemDetail.length < 1 ){
		$state.go("pickupLocation");
	}else {

		if(!check.isEmpty($scope.currentUser.pickLatlng) && !check.isEmpty($scope.currentUser.pickLatlng) ){
			var pickuplat = $scope.currentUser.pickLatlng.lat;
			var pickuplng = $scope.currentUser.pickLatlng.lng;
			var drplat = $scope.currentUser.dropLatlng.lat;
			var drplng = $scope.currentUser.dropLatlng.lng;

			/*== Get distance==*/
			$scope.totalDistance = $rootScope.getDistance(pickuplat, pickuplng, drplat, drplng, "K");
		}
	}

	/*== Drop Location Event After drag and drop marker  ==*/
	NgMap.getMap().then(function(map){
		$scope.mapResponse = {};
		var marker = new google.maps.Marker();
		marker.setMap(null);
		var geocoder = new google.maps.Geocoder;
		var origin1 = new google.maps.LatLng($scope.currentUser.pickLatlng.lat, $scope.currentUser.pickLatlng.lng);
		var origin2 = $scope.currentUser.pickUplocation;

		var destinationA = $scope.currentUser.droplocation;
		var destinationB = new google.maps.LatLng($scope.currentUser.dropLatlng.lat, $scope.currentUser.dropLatlng.lng);
		var service = new google.maps.DistanceMatrixService;
		service.getDistanceMatrix({
			origins: [origin1, origin2],
			destinations: [destinationA, destinationB],
			travelMode: 'DRIVING',
			unitSystem: google.maps.UnitSystem.METRIC,
			avoidHighways: false,
			avoidTolls: false
		},function(response, status) {
			if (status !== 'OK') {
				$scope.message.error = "Sorry but there was an Error: " + status;
			} else {
				$timeout(function() {
					$scope.mapResponse = response;
				}, 10);
			}
		});
	});

	$scope.confirmVendorOrder = function(){
		if( !check.isEmpty(userData) && !check.isEmpty($scope.currentUser) ){
			$http({
				method: 'POST',
				url: base.apiUrl+'createOrder',
				data: {
					userID : userData.id,
					userData : angular.copy($scope.currentUser),
				},
			}).then(function successCallback(orderResponse) {
				if(orderResponse.data.status == 'success'){
					$scope.ajaxResponseMessage = orderResponse.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					$timeout(function() {
						$scope.ajaxResponseMessage = orderResponse.data.message;
						$cookieStore.put('currentUser', {});
						$state.go("thankyou", {id: 7 });
						/*== Item blank after thank you page ==*/
						$scope.currentUser.endUser = {};
						$scope.items = [{id: 1}];
					}, 1500);
				}else {
					$scope.ajaxResponseMessage = orderResponse.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}
			});
		}else {
			aler('User Not logged in, Unable to preccess request');
		}
	}
});

/*== Store Address Controller ==*/
app.controller('storeAddressCtrl', function($scope, check, NgMap, $rootScope, $timeout, $http, base, $cookieStore, userService){

	$scope.store = {};
	var userData = $cookieStore.get('loginUserData');
	$scope.showStoreForm = false;

	if ( !check.isEmpty(userData) ){
		/*== Get User all stores ==*/
		userService.getAllStores(userData.id, function(data){
			if ( !check.isEmpty(userData) ){
				$scope.userStores = data.storeData;
			}else {
				$scope.userStores = null;
			}
		});

		/*== Get User all stores ==*/
		userService.getUserData(userData.id, function(data){
			$scope.userMeta = data.userData;
		});
	}

	$scope.editUserStoreData = function(userData){
		$scope.showStoreForm = true;
		$scope.store = userData;
	}

	/*== Shiw addstore Form ==*/
	$scope.addStore = function(){
		$scope.showStoreForm = true;
		$scope.store = {};
	}

	/*== Insert User store data in database ==*/
	$scope.addStoreData = function(){
		if( !check.isEmpty($scope.store) && !check.isEmpty(userData) ){
			$http({
				method: 'POST',
				url: base.apiUrl+'addStoreData',
				data: {
					userID : userData.id,
					storeData : angular.copy($scope.store)
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showStoreForm = false;
					/*== Get User all stores ==*/
					userService.getAllStores(userData.id, function(data){
						if ( !check.isEmpty(userData) ){
							$scope.userStores = data.storeData;
						}else {
							$scope.userStores = null;
						}
					});
				}else {
					//Do Nothing
				}
			});
		}else {
			aler('User Not logged in, Unable to preccess request');
		}
	}

	/*== Delete user Store ==*/
	$scope.deleteStore = function(storeId){
		if( storeId > 0 && !check.isEmpty(userData) ){
			$http({
				method: 'DELETE',
				url: base.apiUrl+'deleteStore/'+storeId,
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					/*== Get User all stores ==*/
					userService.getAllStores(userData.id, function(data){
						if ( !check.isEmpty(userData) ){
							$scope.userStores = data.storeData;
							$state.go("thankyou");
						}else {
							$scope.userStores = null;
						}
					});
				}else {
					//Do Nothing
				}
			});
		}else {
			alert('User Not logged in, Unable to preccess request');
		}
	}

	$scope.checkUndefined = function(requestVar){
		return ( typeof requestVar === 'undefined' || requestVar == '');
	}
});

/*== View Order Controller ==*/
app.controller('myOrderController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService){

	var userData = $scope.userData = $cookieStore.get('loginUserData');
	if( !check.isEmpty(userData) ){
		if(userData.user_role == 'vendor'){
			userService.getAllVenderOrders(userData.id, function(data){
				if(data.status == "success"){
					$scope.userOrders = data.userOrders;
				}
			});
		}else {
			userService.getAllDeliveryBoyOrders(userData.id, function(data){
				if(data.status == "success"){
					$scope.userOrders = data.userOrders;
				}
			});
		}
	}else {
		$location.path( "/login" );
	}
});

app.controller('orderDetailsController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService, $mdDialog){

	var userData = $cookieStore.get('loginUserData');
	$scope.userData = userData;
	$scope.userOrders = {};
	$scope.orderDeliverUniqueCode = "";

	if( !check.isEmpty(userData) && $stateParams.id > 0 ){
		userService.getVenderOrder(userData.id, $stateParams.id, function(data){
			if(data.status == "success"){
				$scope.userOrders = data.userOrders;
				if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
					$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
				}
			}
		});

		$scope.openInGoogleMap = function(location){
			var link = "http://maps.google.com/maps?q=" + encodeURIComponent(location);
			window.open( link, '_blank' );
		}

		/*== Check order unique code ==*/
		$scope.checkPickupOrderUniqueCode = function(orderUniqueCode){
			$http({
				method: 'POST',
				url: base.apiUrl+'checkOrderPickupUniqueCode',
				data: {
					userID : userData.id,
					orderId : $stateParams.id,
					orderUniqueCode : orderUniqueCode
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					userService.getVenderOrder(userData.id, $stateParams.id, function(data){
						if(data.status == "success"){
							$scope.userOrders = data.userOrders;
							if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
								$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
							}
						}
					});
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function(){
					$scope.showSuccess = false;
					$scope.showError = false;
				}, 2500);

			},function errorCallback(response) {
	            //Do nothing
	        });
		}

		/*== Check order unique code ==*/
		$scope.orderPickedup = function(ev){

			var deliveryManCurrentLatitude=0;
			var deliveryManCurrentLongitude=0;

  			// $scope.totalDistance = $rootScope.getDistance(pickuplat, pickuplng, drplat, drplng, "K");

  			// Appending dialog to document.body to cover sidenav in docs app
  			var confirm = $mdDialog.confirm({
  				onComplete: function afterShowAnimation() {
  					var $dialog = angular.element(document.querySelector('md-dialog'));
  					var $actionsSection = $dialog.find('md-dialog-actions');
  					var $cancelButton = $actionsSection.children()[0];
  					var $confirmButton = $actionsSection.children()[1];
  					angular.element($confirmButton).addClass('md-raised md-warn');
  					angular.element($cancelButton).addClass('md-raised');
  				}
  			})
  			.title('Are you sure, you want picke-up this order?')
  			.ariaLabel('Lucky day')
  			.targetEvent(ev)
  			.cancel('No')
  			.ok('Yes');

  			$mdDialog.show(confirm).then(function() {
  				$http({
  					method: 'POST',
  					url: base.apiUrl+'checkOrderPickupUniqueCode',
  					data: {
  						userID : userData.id,
  						orderId : $stateParams.id,
  					},
  				}).then(function successCallback(response) {
  					if(response.data.status == 'success'){
  						$scope.ajaxResponseMessage = response.data.message;
  						$scope.showSuccess = true;
  						$scope.showError = false;
  						userService.getVenderOrder(userData.id, $stateParams.id, function(data){
  							if(data.status == "success"){
  								$scope.userOrders = data.userOrders;
  								if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
  									$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
  								}
  							}
  						});
  					}else {
  						$scope.ajaxResponseMessage = response.data.message;
  						$scope.showSuccess = false;
  						$scope.showError = true;
  					}

  					$timeout(function(){
  						$scope.showSuccess = false;
  						$scope.showError = false;
  					}, 2500);

  				},function errorCallback(response) {
		            //Do nothing
		        });
  			});
  		}

  		/*== Check order Delivery unique code ==*/
  		$scope.checkOrderDeliveryUniqueCode = function(orderDeliverUniqueCode){
  			$http({
  				method: 'POST',
  				url: base.apiUrl+'checkOrderDeliverCode',
  				data: {
  					userID : userData.id,
  					orderId : $stateParams.id,
  					orderUniqueCode : orderDeliverUniqueCode
  				},
  			}).then(function successCallback(response) {
  				if(response.data.status == 'success'){
  					$scope.ajaxResponseMessage = response.data.message;
  					$scope.showSuccess = true;
  					$scope.showError = false;
  					userService.getVenderOrder(userData.id, $stateParams.id, function(data){
  						if(data.status == "success"){
  							$scope.userOrders = data.userOrders;
  							if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
  								$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
  							}
  						}
  					});
  				}else {
  					$scope.ajaxResponseMessage = response.data.message;
  					$scope.showSuccess = false;
  					$scope.showError = true;
  				}

  				$timeout(function(){
  					$scope.showSuccess = false;
  					$scope.showError = false;
  				}, 2500);

  			},function errorCallback(response) {
	            //Do nothing
	        });
  		}	

  		/*== Check order Delivery unique code ==*/
  		$scope.orderCompleteRequest = function(ev){

  			// Appending dialog to document.body to cover sidenav in docs app
  			var confirmComplete = $mdDialog.confirm({
  				onComplete: function afterShowAnimation() {
  					var $dialog = angular.element(document.querySelector('md-dialog'));
  					var $actionsSection = $dialog.find('md-dialog-actions');
  					var $cancelButton = $actionsSection.children()[0];
  					var $confirmButton = $actionsSection.children()[1];
  					angular.element($confirmButton).addClass('md-raised md-warn');
  					angular.element($cancelButton).addClass('md-raised');
  				}
  			})
  			.title('Are you sure, you want complete mark this order?')
  			.ariaLabel('Lucky day')
  			.targetEvent(ev)
  			.cancel('No')
  			.ok('Yes');

  			$mdDialog.show(confirmComplete).then(function() {
  				$http({
  					method: 'POST',
  					url: base.apiUrl+'checkOrderDeliverCode',
  					data: {
  						userID : userData.id,
  						orderId : $stateParams.id,
  						orderUniqueCode : $scope.orderDeliverUniqueCode
  					},
  				}).then(function successCallback(response) {
  					if(response.data.status == 'success'){
  						$scope.ajaxResponseMessage = response.data.message;
  						$scope.showSuccess = true;
  						$scope.showError = false;
  						userService.getVenderOrder(userData.id, $stateParams.id, function(data){
  							if(data.status == "success"){
  								$scope.userOrders = data.userOrders;
  								if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
  									$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
  								}
  							}
  						});
  					}else {
  						$scope.ajaxResponseMessage = response.data.message;
  						$scope.showSuccess = false;
  						$scope.showError = true;
  					}

  					$timeout(function(){
  						$scope.showSuccess = false;
  						$scope.showError = false;
  					}, 2500);

  				},function errorCallback(response) {
		            //Do nothing
		        });
  			});
  		}
  	}

  	$scope.$on('$destroy',function(){
  		if($scope.repeat)
  			$interval.cancel($scope.repeat);   
  	});
  });

app.controller('trackOrderController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService, $rootScope){

	var userData = $cookieStore.get('loginUserData');
	$scope.trackOrderData = {};
	$scope.data;
	var marker = [];

	var car = base.siteUrl+'assets/images/car-location-25.png';

	function getLocations(){
		$rootScope.bodylayout = 'getting-location';
		$http({
			method : "GET",
			url : base.apiUrl + '/trackOrderByLink/' + $stateParams.id,
			ignoreLoadingBar: true,
			data: {
				encodedorderID : $stateParams.id,
			},
		}).then(function mySuccess(response) {
			$scope.data = response.data;
			$timeout(function(){
				$rootScope.bodylayout = 'delivery-man';
			}, 1500)
			if($scope.data.status == 'success'){
				var mylatLong = new google.maps.LatLng({lat: parseFloat($scope.data.deliveryBoyData.lat), lng: parseFloat($scope.data.deliveryBoyData.lng)});
				var to = new google.maps.LatLng({lat: parseFloat(28.460596), lng: parseFloat(77.039597)});
				NgMap.getMap().then(function(map){
					if(marker != undefined && marker.length != 0){
						marker.setPosition(mylatLong);
						map.panTo(mylatLong);
						marker.setMap(map);                        
					}else {
						marker = new google.maps.Marker({
							map: map,
							position: mylatLong,
							title: 'Your Package',
							icon: car
						});
					}
				});
			} else{
				$interval.cancel($scope.repeat);
			}
		}, function myError(response) {
            //$scope.myWelcome = response.statusText;
        });
	}

	$scope.showYourPosition = function(){
		NgMap.getMap().then(function(map){
			map.setZoom(16);
			map.setCenter({lat: parseFloat($scope.data.deliveryBoyData.lat), lng: parseFloat($scope.data.deliveryBoyData.lng)});
		});
	}

	if( !check.isEmpty(userData) && $stateParams.id > 0 ){
		userService.getVenderOrder(userData.id, $stateParams.id, function(data){
			if(data.status == "success"){
				$scope.trackOrderData = data.userOrders;
				NgMap.getMap().then(function(map){
					getLocations();
					$scope.repeat = $interval(function(){
						getLocations();
					},5000);
				});
			}
		});
	}

	$scope.$on('$destroy',function(){
		if($scope.repeat)
			$interval.cancel($scope.repeat);   
	});
});

app.controller('deliveryBoysController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService){

	var userData = $cookieStore.get('loginUserData');
	$scope.options = {};

	if( !check.isEmpty(userData)){
		/*== GET delivery boys when changed option ==*/
		$scope.optionChanged = function(){
			if($scope.options.distance_covered != undefined && $scope.options.work_status != undefined){
				userService.getAllDeliveryBoy(userData.id, $scope.options.distance_covered, $scope.options.work_status, function(data){
					if(data.deliveryBoyData.length > 0){
						$scope.delivery_boys = data.deliveryBoyData;
						NgMap.getMap().then(function(map){
							var bounds = new google.maps.LatLngBounds();
							var marker = [];
							var infowindow = [];
							angular.forEach(data.deliveryBoyData, function(value, key) {
								console.log(parseFloat(value.lat) );

								marker[parseInt(value.user_id)] = new google.maps.Marker({
									map: map,
									position: {lat: parseFloat(value.lat), lng: parseFloat(value.lng)},
									title: value.name
								});
								bounds.extend(marker[parseInt(value.user_id)].getPosition());
								map.fitBounds(bounds);
								infowindow[parseInt(value.user_id)] = new google.maps.InfoWindow({
									content: '<h3>' + value.name + ' </h3>' + value.address
								});
								marker[parseInt(value.user_id)].addListener('click', function() {
									marker[parseInt(value.user_id)].setAnimation(google.maps.Animation.BOUNCE);
									infowindow[parseInt(value.user_id)].open(map, marker[parseInt(value.user_id)]);
									$timeout(function(){
										marker[parseInt(value.user_id)].setAnimation(null);
									}, 1500);
								});
							});
						});
					}else {
						$scope.delivery_boys = false;
					}
				});
			}
		}

		/*== Sync delivery Boys ==*/
		$scope.sycDeliveryBoys = function(){
			$scope.optionChanged();
		}
	}
});

app.controller('pendingOrderRequestsController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService, $rootScope){

	//clat and clng for current lat and long of user
	var userData = $cookieStore.get('loginUserData');
	// console.log("userData");
	// console.log(userData.delivery_mode);
	// console.log(userData.inhouse_vendor);
	var userLocation=null;
	var count=null;
	var dist=null;
	$scope.awatingUserOrders=null;
	/*check.getLocation(function(data){
		console.log("location = "+data.lat+" "+data.lng);
		userLocation = data;
	});*/
	$scope.count=0;
	$scope.deliveryLocations = [];
	userService.getDeliveryCount(userData.id, function(data){
		$scope.count = data.pickupcount;

		$http({
			method: 'GET',
			url: base.apiUrl+'getDeliverySuburbs/'+userData.id,
		}).then(function successCallback(response) {
	            $scope.deliveryLocations = response.data.delLocations;

	            if( !check.isEmpty(userData) && userData.user_role == "deliveryman" ){
	            	if($scope.count < 2) {
	            		userService.getAllAwaitingOrder(userData.id, function(data){
	            			if(data.status == "success") {
	            				check.getLocation(function(locdata){
									userLocation = locdata;
									checkPickupCriteria(data.userOrders, data.userAcceptedOrders, userLocation, count);
								});
								// console.log(data.userAcceptedOrders);
								// $scope.awatingUserOrders = data.userOrders;
							}
						});
	            	}

	            	/*== Sync New Order ==*/
	            	$scope.syncNewAwitingOrder = function(){
	            		if($scope.count < 2)
	            		{
	            			$scope.awatingUserOrders = null;
	            			userService.getAllAwaitingOrder(userData.id, function(data){
	            				if(data.status == "success"){
							// checkPickupCriteria(data.userOrders,data.userAcceptedOrders, userLocation, count);
							// $scope.awatingUserOrders = data.userOrders;
									check.getLocation(function(locdata){
										userLocation = locdata;
										// if(userData.delivery_mode !="inhouse" && userData.inhouse_vendor !="")
										checkPickupCriteria(data.userOrders, data.userAcceptedOrders, userLocation, count);
									});
								}
							});
	            		}
	            	}
	            }
	        }, function errorCallback(response){
	        	console.table(response);
	        });

	});

	$scope.acceptOrderByDeliveryBoy = function(orderId){
		$http({
			method: 'POST',
			url: base.apiUrl+'acceptOrderByDeliveryBoy',
			data: {
				userID : userData.id,
				orderId : orderId,
			},
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.ajaxResponseMessage = response.data.message;
				$rootScope.orderNotification = false;
				$scope.showSuccess = true;
				$scope.showError = false;
			}else {
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = false;
				$scope.showError = true;
			}

			$timeout(function() {
				$scope.syncNewAwitingOrder();
				location.reload();
				$scope.showError = false;
				$scope.showSuccess = false;
				$rootScope.orderNotification = false;
			},1800);

		},function errorCallback(response) {
		//Do nothing
	});
	}

	function checkPickupCriteria(userOrderData, userAcceptedOrders, userLocation, count)
	{
		for(var orderItem in userOrderData)
		{
			console.log(orderItem.delivery_type);
			if(orderItem.delivery_type == "")
			{
				// c1distance is distance between drivers current position and pickup position (< 1km)
				// c2distance is distance between order 1 drop position and order 2 drop position (< 2km)

				// console.log(userOrderData[orderItem].drop_lat, userOrderData[orderItem].drop_long, userAcceptedOrders[0].drop_lat, userAcceptedOrders[0].drop_long);
				// var cshowtemp="yes";
				// var msg = "You can Pickup";
				/*if(count >= 2){
					cshowtemp = "no";
					msg = "Your already have 2 orders delivery";
				}*/
				// else if(count <= 1){
					var c1distance = $rootScope.getDistance(userLocation.lat, userLocation.lng, userOrderData[orderItem].pickup_lat, userOrderData[orderItem].pickup_long, "K");
					var c2distance = 0;
					var c3distance = 0;
					// c3distance = $rootScope.getDistance(userOrderData[orderItem].drop_lat, userOrderData[orderItem].drop_long, userLocation.lat, userLocation.lng, "M");
					// console.log("c3:",c3distance);
					if(userAcceptedOrders.length > 0)
					{
						// c1distance = $rootScope.getDistance(userLocation.lat, userLocation.lng, userOrderData[orderItem].pickup_lat, userOrderData[orderItem].pickup_long, "K");
						c2distance = $rootScope.getDistance(userOrderData[orderItem].drop_lat, userOrderData[orderItem].drop_long, userAcceptedOrders[0].drop_lat, userAcceptedOrders[0].drop_long, "K");
						
					}
					var c3distancecond = false;

					for(var deliverylocationItem in $scope.deliveryLocations)
					{
						// console.log($scope.deliveryLocations[deliverylocationItem]);
						// console.log("pickup_lat: "+userOrderData[orderItem].pickup_lat);
						// console.log("pickup_long: "+userOrderData[orderItem].pickup_long);
						// console.log("deliverylocationItem.latitude: "+$scope.deliveryLocations[deliverylocationItem].latitude);
						// console.log("deliverylocationItem.longitude: "+$scope.deliveryLocations[deliverylocationItem].longitude);
						c3distance = $rootScope.getDistance(userOrderData[orderItem].pickup_lat, userOrderData[orderItem].pickup_long, $scope.deliveryLocations[deliverylocationItem].latitude, $scope.deliveryLocations[deliverylocationItem].longitude, "M");
						// console.log("c3distancecond: "+c3distance);
						// console.log("elevation: "+$scope.deliveryLocations[deliverylocationItem].elevation);
						if(c3distance <= $scope.deliveryLocations[deliverylocationItem].elevation)
						{
							console.log("c3distancecond matched");
							c3distancecond = true;
							break;
						}
						else 
							console.log("c3distancecond not matched");
					}
					// userOrderData[orderItem].c1distance = c1distance;
					// userOrderData[orderItem].c2distance = c2distance;
					/*if(c1distance > 1 || c2distance > 2){
						cshowtemp = "no";
						msg = "Distance not sufficient";
					}*/
				// }
				// userOrderData[orderItem].cshow = cshowtemp;
				// userOrderData[orderItem].cshow = cshowtemp;
				userOrderData[orderItem].c1distance = c1distance;
				userOrderData[orderItem].c2distance = c2distance;
				userOrderData[orderItem].c3distance = c3distance;
				userOrderData[orderItem].c3distancecond = c3distancecond;
			}
			else
			{
				userOrderData[orderItem].c1distance = 0;
				userOrderData[orderItem].c2distance = 0;
				userOrderData[orderItem].c3distance = 0;
				userOrderData[orderItem].c3distancecond = true;
			}
		}
		
		$scope.awatingUserOrders = userOrderData;
	}
});

/*== Admin Dashboard Controller ==*/
app.controller('adminDashboardController', function($scope, $cookieStore, userService, $rootScope, check, $http, $state, base, $mdDialog, $timeout){
	
	$scope.userData = $cookieStore.get('loginUserData');
	var userData = angular.copy($scope.userData);
	$scope.user = {};
	$scope.userEmailData = {};
	$scope.userSearch = '';

	function getAllUser(){
		// Get vendors and delivery man
		$http({
			method: 'GET',
			url: base.apiUrl+'adminGetAllUsers/'+userData.id,
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.deliveryman = response.data.deliveryBoys;
				$scope.vendors = response.data.vendors;
			}
		});
	}

	/**
	* Function to produce UUID.
	*/
	function generateUUID()	{		
		var d = new Date().getTime();
		if( window.performance && typeof window.performance.now === "function" ) {
			d += performance.now();
		}

		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x3|0x8)).toString(16);
		});

		return uuid;
	}

	if ( !check.isEmpty(userData) ){
		/*== Get User ==*/
		userService.getUserData(userData.id, function(data){
			if(data.userData.user_role == 'administrator'){
				//Do nothing
				getAllUser();
			}else {
				$timeout(function() {
					$state.go("login");
				}, 500);
			}
		});
	}else {
		$timeout(function() {
			$state.go("login");
		}, 500);
	}
	
	/*== Genrate new api keys ==*/
	$scope.genrateNewApi = function(){
		$scope.user.apiKey = generateUUID();
	}

	/**/
	$scope.requestGenrateApi = function($event, userID, apiKey, emailId) {

		if(apiKey !== '') {
			$timeout(function() {
				$scope.user.apiKey = apiKey;			
				$scope.user.userID = userID;			
				$scope.user.email = emailId;			
			}, 1000);
		}

		$mdDialog.show({
			targetEvent: $event,
			scope: $scope,
			templateUrl: 'assets/dialog/genrateKey.tmpl.html',
			controller: 'adminDashboardController',
			preserveScope: true
		});
	}

	/*== Send email to vendor ==*/
	$scope.sendEmail = function(vendorEmailData){

		if( vendorEmailData.email !== '' && vendorEmailData.subject !== '' && vendorEmailData.message !== ''){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'sendMailVendor',
				data: {
					emailData : vendorEmailData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
					$scope.closeDialog();
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}

	/*== Request for email send ==*/
	$scope.requestSendEmail = function($event, emailId) {

		if(emailId !== ''){
			$timeout(function(){
				$scope.userEmailData.email = emailId;
			}, 1000);
		}else {
			$scope.userEmailData.email = '';
		}

		$mdDialog.show({
			targetEvent: $event,
			scope: $scope,
			templateUrl: 'assets/dialog/sendMail.tmpl.html',
			controller: 'adminDashboardController',
			preserveScope: true
		});
	}

	/*== Request for email send ==*/
	$scope.requestForDeleteAccount = function($event, userID, emailId) {

		if(userID > 0 && emailId !== ''){
    		// Appending dialog to document.body to cover sidenav in docs app
    		var confirm = $mdDialog.confirm({
    			onComplete: function afterShowAnimation() {
    				var $dialog = angular.element(document.querySelector('md-dialog'));
    				var $actionsSection = $dialog.find('md-dialog-actions');
    				var $cancelButton = $actionsSection.children()[0];
    				var $confirmButton = $actionsSection.children()[1];
    				angular.element($confirmButton).addClass('md-raised md-warn');
    				angular.element($cancelButton).addClass('md-raised');
    			}
    		})
    		.title('Are you sure, you want to Delete this account ?')
    		.ariaLabel('Lucky day')
    		.targetEvent($event)
    		.cancel('No')
    		.ok('Yes');

    		$mdDialog.show(confirm).then(function() {
    			$http({
    				method: 'POST',
    				url: base.apiUrl+'deleteUser',
    				data: {
    					emailData : emailId,
    					userID : userID,
    					adminID : userData.id
    				},
    			}).then(function successCallback(response) {
    				if(response.data.status == 'success'){
    					$scope.ajaxResponseMessage = response.data.message;
    					$scope.showSuccess = true;
    					$scope.showError = false;
    				}else {
    					$scope.ajaxResponseMessage = response.data.message;
    					$scope.showSuccess = false;
    					$scope.showError = true;
    				}

    				$timeout(function() {
    					$scope.showSuccess = false;			
    					$scope.showError = false;
    					/*== Get User ==*/
    					userService.getUserData(userData.id, function(data){
    						if(data.userData.user_role == 'administrator'){
								//Do nothing
								getAllUser();
							}else {
								$timeout(function() {
									$state.go("login");
								}, 500);
							}
						});
    				}, 3000);
    			});
    		});
    	}	
    }	

    /*== Close dialog ==*/
    $scope.closeDialog = function(){
    	$mdDialog.hide();
    }

    /*== Send api key ==*/
    $scope.sendApiKeyToEmail = function(){

    	if( $scope.user.email !== '' && $scope.user.apiKey !== '' ){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'sendApiKeyToEmail',
				data: {
					apiKey : $scope.user.apiKey,
					userEmail : $scope.user.email,
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					getAllUser();
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}

	/*== Save api key ==*/
	$scope.saveApiKey = function(){

		if($scope.user.apiKey !== '' && $scope.user.userID > 0 ){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'insertUpdateApiKey',
				data: {
					userID : $scope.user.userID,
					apiKey : $scope.user.apiKey,
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					getAllUser();
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}

	/*== Request for edit vendor ==*/
	$scope.requestForEditVendor = function($event, vendor){

		if( !check.isEmpty(vendor) ){
			$timeout(function() {
				$scope.vendorEditData = vendor;
			}, 1000);

			$mdDialog.show({
				targetEvent: $event,
				scope: $scope,
				templateUrl: 'assets/dialog/vendorEdit.tmpl.html',
				controller: 'adminDashboardController',
				preserveScope: true
			});
		}
	}

	/*== Save vendor changes ==*/
	$scope.saveVendorChanges = function(vendorData){
		
		if( !check.isEmpty(vendorData) ){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'saveVendorByAdmin',
				data: {
					vendorData : vendorData,
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					getAllUser();
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}

	/*== Request for edit vendor ==*/
	$scope.requestForEditReleaseAmount = function($event, deliveryman){

		if( !check.isEmpty(deliveryman) ){
			$timeout(function() {
				$scope.deliveryEditData = deliveryman;
			}, 1000);

			$mdDialog.show({
				targetEvent: $event,
				scope: $scope,
				templateUrl: 'assets/dialog/deliveryman-release.tmpl.html',
				controller: 'adminDashboardController',
				preserveScope: true
			});
		}else {
			alert('data not found!');
		}
	}

	/*== Save Release Amount ==*/
	$scope.saveRealeaseAmount = function(deliveryManData){
		
		if( !check.isEmpty(deliveryManData) ){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'saveRealeaseAmount',
				data: {
					deliveryManData : deliveryManData,
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					getAllUser();
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}

	/*== chnage DeliveryMan Staus ==*/
	$scope.changeDeliveryManUserStatus = function(deliveryManData){

		if( !check.isEmpty(deliveryManData) ){
			// Get vendors and delivery man
			$http({
				method: 'POST',
				url: base.apiUrl+'changeDeliveryManUserStatus',
				data: {
					deliveryManData : deliveryManData,
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = true;
					$scope.showError = false;
					getAllUser();
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}

				$timeout(function() {
					$scope.showSuccess = false;			
					$scope.showError = false;
				}, 3000);
			});
		}else {
			alert('data not found');
		}
	}
});



/*== Admin Transportation Modes Controller ==*/
app.controller('adminTransportModeController', function($scope, $cookieStore, userService, $rootScope, check, $http, $state, base, $mdDialog, $timeout,$stateParams){
	
	$scope.userData = $cookieStore.get('loginUserData');
	var userData = angular.copy($scope.userData);
	$scope.tm = {};

	if( !check.isEmpty(userData) && $stateParams.id > 0 ){
		$http({
			method: 'POST',
			url: base.apiUrl+'getTMByID/'+userData.id,
			data: {
				tmid : $stateParams.id,
			},
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.updateTmData = response.data.tmData;
				if(response.data.tmData.status == 0)
					$scope.updateTmData.status = false;
				else
					$scope.updateTmData.status = true;


			}
		});
	}else if(!check.isEmpty(userData) && check.isEmpty($stateParams.id)){
		$scope.updateTmData = {};
	}
	
	function getAllTMs() {
		$http({
			method: 'GET',
			url: base.apiUrl+'adminGetAllTMs/'+userData.id,
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.tmData = response.data.tmData;
			}
		});
	}
	getAllTMs();

	$scope.addTM = function() {
		$http({
			method: 'POST',
			url: base.apiUrl+'adminAddNewTM/'+userData.id,
			data: {
				name : $scope.tm.name,
				status : $scope.tm.status
			},
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = true;
				$scope.showError = false;
				$timeout(function() {
					$state.go("adminManageTM");
				}, 500);
			}else {
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = false;
				$scope.showError = true;
			}
		});
	}

	$scope.updateTM = function() {
		$http({
			method: 'POST',
			url: base.apiUrl+'adminUpdateTM/'+userData.id,
			data: {
				tmid : $scope.updateTmData.id,
				name : $scope.updateTmData.name,
				status : $scope.updateTmData.status
			},
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = true;
				$scope.showError = false;
				$timeout(function() {
					$state.go("adminManageTM");
				}, 500);
			}else {
				$scope.ajaxResponseMessage = response.data.message;
				$scope.showSuccess = false;
				$scope.showError = true;
			}
		});
	}

	$scope.deleteTM = function($id) {
		// console.log($id);
		var tmidtodel = $id;

		var confirm = $mdDialog.confirm({
			onComplete: function afterShowAnimation() {
				var $dialog = angular.element(document.querySelector('md-dialog'));
				var $actionsSection = $dialog.find('md-dialog-actions');
				var $cancelButton = $actionsSection.children()[0];
				var $confirmButton = $actionsSection.children()[1];
				angular.element($confirmButton).addClass('md-raised md-warn');
				angular.element($cancelButton).addClass('md-raised');
			}
		})
		.title('Are you sure, you want to delete?')
		.ariaLabel('Lucky day')
				// .targetEvent(ev)
				.cancel('No')
				.ok('Yes');

				$mdDialog.show(confirm).then(function() {
					$http({
						method: 'POST',
						url: base.apiUrl+'adminDeleteTM/'+userData.id,
						data: {
							tmid : tmidtodel
						},
					}).then(function successCallback(response) {
						if(response.data.status == 'success'){
							$scope.ajaxResponseMessage = response.data.message;
							$scope.showSuccess = true;
							$scope.showError = false;
							$timeout(function() {
								getAllTMs();
								$state.go("adminManageTM");
							}, 500);
						}else {
							$scope.ajaxResponseMessage = response.data.message;
							$scope.showSuccess = false;
							$scope.showError = true;
						}
					});
				}, function() {
					console.log('You decided to keep your debt.');
				});

			}

		});

/*== Admin Transaction Controller ==*/
app.controller('adminTransactionsController', function($scope, $cookieStore, userService, $rootScope, check, $http, $state, base, $mdDialog, $timeout) {
	
	$scope.deliveryBoyData = {};

	var userData = $cookieStore.get('loginUserData');
	if( !check.isEmpty(userData) ){
		userService.isAdmin(userData.id, function(data){
			if(data == true){
				//Do nothing
			}else {
				$location.path( "/dashboard" );
			}			
		});
	}

	/*== Get deliveryman account data ==*/
	function getAllDeliveryManAccountData(){
		// Get Delivery man account data 
		if(userData.id !== ''){
			$http({
				method: 'GET',
				url: base.apiUrl+'getDelerymanOrderAccountDataByAdmin/'+userData.id,
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.deliveryBoyData = response.data.deliveryBoyData;
				}
			});
		}else{
			alert('error in getting data');
		}
	}	

	/*== Call function when controller call ==*/
	getAllDeliveryManAccountData();

	/*== Get deliveryman account data ==*/
	$scope.getDeliveryManReleaseData = function(userID){
		if(userID !== ''){
			$state.go('paymentRelease', { 'id':userID});
		}
	}
});

/*== Admin Payment Release Controller ==*/
app.controller('adminPaymentReleaseController', function($scope, $cookieStore, userService, $rootScope, check, $http, $state, base, $mdDialog, $timeout, $stateParams, $timeout) {
	
	$scope.deliveryBoyData = {};
	const deliverymanId = $stateParams.id;

	var userData = $cookieStore.get('loginUserData');
	if( !check.isEmpty(userData) ){
		userService.isAdmin(userData.id, function(data){
			if(data == true){
				//Do nothing
			}else {
				$location.path( "/dashboard" );
			}			
		});
	}

	// Get Delivery man account data 
	if( deliverymanId !== ''){
		$http({
			method: 'GET',
			url: base.apiUrl+'getDelerymanOrderAccountData/'+$stateParams.id,
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$state.go('paymentRelease');
				$scope.deliveryBoyOrderData = response.data.deliveryBoyOrderData;
				$scope.releaseAmountData = response.data.releaseAmountData;
			}

		});
	}else{
		alert('error in getting data');
	}

	/*== Add / Edit Time Availability  ==*/
	$scope.requestReleaseAmount = function($event, releaseRemaining){

		userService.isAdmin(userData.id, function(data){
			if(data == true && deliverymanId !== ''){

				$scope.releaseRemaining = releaseRemaining;
				$mdDialog.show({
					targetEvent: $event,
					scope: $scope,
					templateUrl: 'assets/dialog/admin-release-amount.tmpl.html',
					controller: 'adminPaymentReleaseController',
					preserveScope: true
				});
			}else {
				$location.path( "/dashboard" );
			}			
		});
	}

	/*== Close dialog ==*/
	$scope.closeDialog = function(){
		$mdDialog.hide();
	}	

	/*== Release Amount ==*/
	$scope.releaseAmount = function(releaseAmt, releaseRemaining){
		console.log(releaseAmt);
		console.log(deliverymanId);

		if( deliverymanId > 0 ){

			if(releaseRemaining < releaseAmt ){
				alert('Release amount can not be greater than remaining amount');
			}else {
				$http({
					method: 'POST',
					url: base.apiUrl+'releaseAmountFordeliveryman',
					data: {
						'releaseAmount': releaseAmt,
						'deliverymanId': deliverymanId,
						'adminId': userData.id
					}
				}).then(function successCallback(response) {
					if(response.data.status == 'success'){
						console.log(response);
					}
				});
			}

		}else {
			alert('error in getting data');
		}

	}
});

/*== Deliveryman Transaction Controller ==*/
app.controller('deliverymanTransactionsController', function($scope, $cookieStore, userService, $rootScope, check, $http, $state, base, $mdDialog, $timeout) {

	var userData = $cookieStore.get('loginUserData');
	if( !check.isEmpty(userData) ){
		userService.isDeliveryMan(userData.id, function(data){
			if(data == true){
				//Do nothing
			}else {
				$location.path( "/login" );
			}			
		});
	}

	/*== Get deliveryman account data ==*/
	function getAllDeliveryManAccountData(){
		// Get Delivery man account data 
		if(userData.id !== ''){
			$http({
				method: 'GET',
				url: base.apiUrl+'getDelerymanOrderAccountData/'+userData.id,
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.deliveryBoyOrderData = response.data.deliveryBoyOrderData;
					$scope.releaseAmountData = response.data.releaseAmountData;
				}
			});
		}else{
			alert('error in getting data');
		}
	}	

	getAllDeliveryManAccountData();
});

/*== This is Dashboard Controller ==*/
app.controller('editAvailabilityController', function($scope, $http, base, $location, userService ,check, $cookieStore, $state, $mdDialog, $timeout, $mdpTimePicker, $rootScope){

	$cookieStore.remove('currentUser');
	$scope.base = base;
	$scope.userData = $cookieStore.get('loginUserData');
	var userData = angular.copy($scope.userData);
	$scope.daysWithTime={};
	$scope.tags=[];
	$scope.user={};
	function getAllStates(){
		// Get all states
		$http({
			method: 'GET',
			url: base.apiUrl+'getAllStates',
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.statesData = response.data.stateData;
				// $scope.statesData = [{'state_code':'1', 'state':'BCd'},{'state_code':'2', 'state':'def'}];
			}
		});
	}

	$scope.getCities = function(){
		// Get all state cities
		$scope.cityData = [];
		$scope.suburbsData=[];
		$scope.user.suburbs = [];
		$http({
			method: 'GET',
			url: base.apiUrl+'getStateCities/'+$scope.user.state,
		}).then(function successCallback(response) {
			if(response.data.status == 'success'){
				$scope.cityData = response.data.cityData;
				$scope.suburbsData = response.data.suburbsData;
			}
		});
	}

	$scope.loadTimePickerPopup = function($event)
	{
		console.log($event);
		$scope.daysWithTime={};
		$mdDialog.show({
			targetEvent: $event,
			scope: $scope,
			templateUrl: 'assets/dialog/newDeliverymanTimeAvailability.tmpl.html',
			controller: '',
			preserveScope: true
		});
	};


	$scope.addNewday = function()
	{
		var fullfromTimearr = $scope.daysWithTime.fromTime.split(" ");
		var fulltoTimearr = $scope.daysWithTime.toTime.split(" ");

		var fromTimearr = fullfromTimearr[0].split(":");
		var toTimearr = fulltoTimearr[0].split(":");

		/*if(parseInt(fromTimearr[0]) > parseInt(toTimearr[0]))
		{
			alert("from time should be greater");
		}
		else {*/
        	// console.log(fromtimearr);
        	$scope.daysWithTime.display = $scope.daysWithTime.day+" "+$scope.daysWithTime.fromTime+" to "+$scope.daysWithTime.toTime;
        	$scope.tags.push($scope.daysWithTime);// = [{text:"mon 8am to 10am"}];
        	$mdDialog.hide();
		// }
	};

	getAllStates();

	$scope.isHover=false;

	if ( !check.isEmpty(userData) ){
		//Do nothing
	}else {
		$location.path( "/login" );
	}

	/*== State List ==*/
	$scope.optionsList = [
	{id: 3,  name : "Queensland"},
	{id: 4,  name : "Tasmania"},
	{id: 5,  name : "Northern Territory"},
	{id: 6,  name : "Australian Capital Territory"},
	{id: 7,  name : "Christmas Island"},
	{id: 8,  name : "Australian Capital Territory"},
	{id: 9,  name : "Cocos (Keeling) Islands"},
	{id: 10,  name : "Jervis Bay Territory"},
	{id: 11,  name : "Norfolk Island"},
	{id: 12,  name : "South Australia"}
	];

	/*== Get edit user data ==*/
	function getEditUserData() {
		if( !check.isEmpty(userData) ){
			userService.getUserData(userData.id, function(data){
				
				// console.log(data.userData.availability_slots[0].display);
				$scope.tags = data.userData.availability_slots;
				$scope.user.state = data.userData.state;
				$scope.getCities();
				$scope.user.city = data.userData.city;
				$scope.user.suburbs = data.userData.suburbs;

				var updateProfileData = data.userData;

				if(updateProfileData.time_avalibility_from !== ''){
					updateProfileData['time_avalibility_from'] = new Date(updateProfileData.time_avalibility_from);
				}else {
					updateProfileData['time_avalibility_from'] = new Date();
				}

				if(updateProfileData.time_avalibility_to !== ''){
					updateProfileData['time_avalibility_to'] = new Date(updateProfileData.time_avalibility_to);
				}else {
					updateProfileData['time_avalibility_to'] = new Date();
				}

				if( !check.isEmpty(updateProfileData.state_avalibility) ){
					$scope.stateAvailability = JSON.parse(updateProfileData.state_avalibility);
				}else {
					$scope.stateAvailability = [];
				}

				$scope.updateProfileData = data.userData;
			});
		}
	}

	/*== GET edit user data when user on edit screen ==*/
	getEditUserData();	
	
	/*== User logout ==*/
	$scope.updateUserTimeAvailability = function(updateProfileData, stateAvailability){
		if ( !check.isEmpty(userData) ){

			updateProfileData['stateAvailability'] = stateAvailability;
			$http({
				method: 'POST',
				url: base.apiUrl+'updateUser/',
				data: {
					userID : userData.id,
					userData : $scope.updateProfileData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showSuccess = true;
					$scope.showError = false;
					$scope.ajaxResponseMessage = response.data.message;
					$timeout(function(){
						getEditUserData();
						$state.go("dashboard");
					}, 2000);
				}else {
					$scope.showSuccess = false;
					$scope.showError = true;
					$scope.ajaxResponseMessage = response.data.message;
				}
			});
		}
	}

	/*== User logout ==*/
	$scope.updateUserAvailability = function(updateProfileData, stateAvailability){
		if ( !check.isEmpty(userData) ){

			updateProfileData['availability_slots'] = $scope.tags;
			updateProfileData['state'] = $scope.user.state;
			updateProfileData['city'] = $scope.user.city;
			updateProfileData['suburbs'] = $scope.user.suburbs;
			$http({
				method: 'POST',
				url: base.apiUrl+'updateUserAvailability/',
				data: {
					userID : userData.id,
					userData : $scope.updateProfileData
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.showSuccess = true;
					$scope.showError = false;
					$scope.ajaxResponseMessage = response.data.message;
					$timeout(function(){
						getEditUserData();
						$state.go("dashboard");
					}, 2000);
				}else {
					$scope.showSuccess = false;
					$scope.showError = true;
					$scope.ajaxResponseMessage = response.data.message;
				}
			});
		}
	}
});

/*== Create User by admin ==*/
app.controller('adminUserManagementController', function($scope, check, NgMap, $cookies, $cookieStore, $timeout, $state, $stateParams, $interval, $http, base, userService, $mdDialog){

	var userData = $cookieStore.get('loginUserData');
	$scope.userData = userData;
	$scope.userOrders = {};

	if( !check.isEmpty(userData) && $stateParams.id > 0 ){
		userService.getUserDataByAdmin(userData.id, $stateParams.id, function(data){
			if(data.status == "success"){
				if(data.userData.user_role =='vendor' || data.userData.user_role =='deliveryman' ){
					$scope.updateProfileData = data.userData;
					
					userService.getVendorUserDataByAdmin(userData.id, $stateParams.id, function(vendordata){
						if(vendordata.status == "success"){
							$scope.updateVendorProfileData = vendordata.userData;
							console.log("updateVendorProfileData");
							console.log($scope.updateVendorProfileData);
						}
					});
				}
			}
		});
	}else if(!check.isEmpty(userData) && check.isEmpty($stateParams.id)){
		$scope.updateProfileData = {};
	}

	/*== User logout ==*/
	$scope.updateUserProfileByAdmin = function(updateProfileData){

		$scope.updateProfileData = angular.copy(updateProfileData);

		if ( $stateParams.id > 0) {
			if ( !check.isEmpty(userData) && !check.isEmpty(updateProfileData)){
				$http({
					method: 'POST',
					url: base.apiUrl+'updateUserByAdmin/',
					data: {
						userID : updateProfileData.id,
						adminID : userData.id,
						userData : $scope.updateProfileData
					},
				}).then(function successCallback(response) {
					if(response.data.status == 'success'){
						$scope.showSuccess = true;
						$scope.showError = false;
						$scope.ajaxResponseMessage = response.data.message;
						userService.getUserData(userData.id, function(data){
							$scope.updateProfileData = data.userData;
							$state.go("dashboard");
						});
					}else {
						$scope.showSuccess = false;
						$scope.showError = true;
						$scope.ajaxResponseMessage = response.data.message;
						$scope.ajaxResponseMessage = response.data.message;
					}
				});
			}
		}else{
			if ( !check.isEmpty(userData) && !check.isEmpty(updateProfileData)){
				$http({
					method: 'POST',
					url: base.apiUrl+'addUserByAdmin/',
					data: {
						adminID : userData.id,
						userData : $scope.updateProfileData
					},
				}).then(function successCallback(response) {
					if(response.data.status == 'success'){
						$scope.showSuccess = true;
						$scope.showError = false;
						$scope.ajaxResponseMessage = response.data.message;
						userService.getUserData(userData.id, function(data){
							$scope.updateProfileData = data.userData;
							$state.go("adminDashboard");
						});
					}else {
						$scope.showSuccess = false;
						$scope.showError = true;
						$scope.ajaxResponseMessage = response.data.message;
						$scope.ajaxResponseMessage = response.data.message;
					}
				});
			}
		}
	}
});

app.controller('feedbackcontroller', function($scope, $http, base, $location, $cookieStore, $timeout, $state, $stateParams, check, $interval, $rootScope){
	$scope.feed = {};
	$scope.ajaxResponseMessage = '';
	var userData = $cookieStore.get('loginUserData');

	$scope.userData = userData;
	$scope.userOrders = {};
	console.log("userData: ",$scope.userData);
	$scope.feedbacks = {};
	if( !check.isEmpty(userData) && $stateParams.id > 0 ){
		$scope.feed.to_user_id = $stateParams.touserid;
		$scope.feed.order_id = $stateParams.id;
		/*userService.getVenderOrder(userData.id, $stateParams.id, function(data){
			if(data.status == "success"){
				$scope.userOrders = data.userOrders;
				if( data.userOrders.pickup_unique_code > 0 &&  data.userOrders.order_status=='picked-up'){
					$scope.orderUniqueCode = data.userOrders.pickup_unique_code;
				}
			}
		});*/


		$scope.sendfeedback = function() {
			// $scope.feed.to_user_id="35";
			// $scope.feed.order_id="5";
			$scope.feed.from_user_id = userData.id;
			// console.log("data",$scope.feed);
			$http({
				method: 'POST',
				url: base.apiUrl+'feedback',
				data: {
					userData : $scope.feed
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = "Thank you! Your feedback is saved";
					$scope.showSuccess = true;
					$scope.showError = false;
					$timeout(function() {
						$state.go("dashboard");
					}, 500);
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}
			});
		};
	}
});

app.controller('feedbackscontroller', function($scope, $http, base, $location, $cookieStore, $timeout, $state, $stateParams, check, $interval, $rootScope, userService){
	$scope.feed = {};
	$scope.ajaxResponseMessage = '';
	var userData = $cookieStore.get('loginUserData');

	$scope.userData = userData;
	$scope.feedbacks = {};
	if( !check.isEmpty(userData)){

		userService.getfeedbacks(userData.id, function(data){
			if(data.status == "success"){
				$scope.feedbacks = data.userFeedbacks;
				console.log($scope.feedbacks);
			}
		});

		userService.getGivenfeedbacks(userData.id, function(data){
			if(data.status == "success"){
				$scope.givenfeedbacks = data.userFeedbacks;
				console.log($scope.givenfeedbacks);
			}
		});

		$scope.updatefeedback = function() {
			// $scope.feed.to_user_id="35";
			// $scope.feed.order_id="5";
			$scope.feed.from_user_id = userData.id;
			// console.log("data",$scope.feed);
			$http({
				method: 'POST',
				url: base.apiUrl+'feedback',
				data: {
					userData : $scope.feed
				},
			}).then(function successCallback(response) {
				if(response.data.status == 'success'){
					$scope.ajaxResponseMessage = "Thank you! Your feedback is saved";
					$scope.showSuccess = true;
					$scope.showError = false;
					$timeout(function() {
						$state.go("dashboard");
					}, 500);
				}else {
					$scope.ajaxResponseMessage = response.data.message;
					$scope.showSuccess = false;
					$scope.showError = true;
				}
			});
		};
	}
});