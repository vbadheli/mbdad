var cacheName = 'v1'; 
var cacheFiles = [	
	'./',
	'./manifest.json',
	'./dial_offline.html'
];

self.addEventListener('install', function(e) {
    e.waitUntil(
	    caches.open(cacheName).then(function(cache) {
			return cache.addAll(cacheFiles);
	    })
	);
});

self.addEventListener('activate', function(e) {
    e.waitUntil(
		caches.keys().then(function(cacheNames) {
			return Promise.all(cacheNames.map(function(thisCacheName){
				if (thisCacheName !== cacheName) {
					return caches.delete(thisCacheName);
				}
			}));
		})
	);
});

self.addEventListener('fetch', function(e) {
	console.log('[ServiceWorker] Fetch', e.request.url);
	e.respondWith(
		caches.match(e.request).then(function(response) {
		return response || fetch(e.request);
		})
	);
});